import { BaseService } from '@baseapp/base.service';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { UploaderService } from '@baseapp/upload-attachment.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActionItem } from '@baseapp/widgets/action-bar/action-bar.component';
import { AppGlobalService } from '@baseapp/app-global.service';
import { SlaughterReportDetailBaseComponent } from '@baseapp/slaughter-report/slaughter-report/slaughter-report-detail/slaughter-report-detail.base.component';
import { AppConstants } from '@app/app-constants';
import { BreadcrumbService } from '@baseapp/widgets/breadcrumb/breadcrumb.service';
import { environment } from '@env/environment';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { ConfirmationPopupComponent } from '@baseapp/widgets/confirmation/confirmation-popup.component';
import { BaseAppConstants } from '@baseapp/app-constants.base';
import { allowedValuesValidator } from '@baseapp/widgets/validators/allowedValuesValidator';
import { ActivatedRoute, Router } from '@angular/router';
import { OnInit, SimpleChanges, OnChanges, Component } from '@angular/core';
import { SlaughterReportService } from '@baseapp/slaughter-report/slaughter-report/slaughter-report.service';
import { DomSanitizer } from '@angular/platform-browser';
import { dateValidator } from '@baseapp/widgets/validators/dateValidator';
import { DialogService } from 'primeng/dynamicdialog';
import { debounceTime, fromEvent, distinctUntilChanged, pairwise, of, Observer, Subscription, map, Observable, Subject, startWith } from 'rxjs';
import { ConfirmationService, MessageService } from 'primeng/api';
import { WorkflowSimulatorComponent } from '@baseapp/widgets/workflow-simulator/workflow-simulator.component';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component';
import { WorkflowHistoryComponent } from '@baseapp/widgets/workflow-history/workflow-history.component';
import { AppBaseService } from '@baseapp/app.base.service';


@Component({
  selector: 'app-slaughter-report-detail',
  templateUrl: '../../../../../base/app/slaughter-report/slaughter-report/slaughter-report-detail/slaughter-report-detail.component.html',
  styleUrls: ['./slaughter-report-detail.scss']
})
export class SlaughterReportDetailComponent extends SlaughterReportDetailBaseComponent implements OnInit, OnChanges {

  constructor(public override slaughterReportService: SlaughterReportService, public override appUtilBaseService: AppUtilBaseService, public override translateService: TranslateService, public override messageService: MessageService, public override confirmationService: ConfirmationService, public override dialogService: DialogService, public override domSanitizer: DomSanitizer, public override bsModalService: BsModalService, public override activatedRoute: ActivatedRoute, public override breadcrumbService: BreadcrumbService, public override appBaseService: AppBaseService, public override router: Router, public override appGlobalService: AppGlobalService, public override uploaderService: UploaderService, public override baseService: BaseService, public override location: Location) {
    super(slaughterReportService, appUtilBaseService, translateService, messageService, confirmationService, dialogService, domSanitizer, bsModalService, activatedRoute, breadcrumbService, appBaseService, router, appGlobalService, uploaderService, baseService, location);
  }

  /* Method to set the initial values of the form before it is saved */
  setInitialValues() {
    this.detailFormControls.patchValue({
      maxrejectslaughterhouse: 0.5,
      maxdead: 0.2,
      status: 'SUPPLY_AND_FLOCK_DATA'
    });
  }

  calculateAge(data: any) {
    let age: any = new Date(data.slaughterDate).getTime() - new Date(data.installDate).getTime();
    age = age / (1000 * 60 * 60 * 24);
    if (age < 0 || isNaN(age)) { age = 0; }
    this.detailFormControls.get('age')?.patchValue(age);
  }

  calculateWeighingBridgeDiscount(data: any) {
    this.detailFormControls.get('weighingBridgeDiscount')?.patchValue(data.weighingExternally == 'YES' ? 1 : 0);
  }

  calculateFeedDiscount(data: any) {
    let feedDiscount = 0;
    if (data.voerkroppen > 6) {
      feedDiscount = 2;
    } else if (data.voerkroppen > 3) {
      feedDiscount = 1
    } else {
      feedDiscount = 0
    }

    this.detailFormControls.get('feedDiscount')?.patchValue(feedDiscount);
  }

  calculatePriceBaseValue(data: any) {
    let priceBaseValue;
    let avgWt = this.detailFormControls.getRawValue()?.averageWeight;
    if (avgWt >= 2401 && avgWt <= 9999) {
      priceBaseValue = 1.5;
    } else if (avgWt >= 1801 && avgWt <= 2400) {
      priceBaseValue = 1.2;
    } else if (avgWt >= 0 && avgWt <= 1800) {
      priceBaseValue = 1;
    } else {
      priceBaseValue = 1;
    }

    this.detailFormControls.get('pricebasevalue')?.patchValue(priceBaseValue);

  }

  calculatePricesrmvalue(data: any) {
    let srmDev = this.detailFormControls.getRawValue()?.srmDeviation;
    let srmVal;
    if (srmDev < 0) {
      srmVal = -0.05;
    } else if (srmDev >= 1 && srmDev <= 50) {
      srmVal = 0;
    } else if (srmDev >= 51 && srmDev <= 99999) {
      srmVal = 0.05;
    }

    this.detailFormControls.get('pricesrmvalue')?.patchValue(srmVal);
  }

  calculatePriceSoberValue(data: any) {
    let vkp = data.voerkroppen;
    let soberValue;
    if (vkp <= 1) {
      soberValue = 0.05;
    } else if (vkp >= 1.1 && vkp <= 5) {
      soberValue = 0;
    } else if (vkp >= 5.1) {
      soberValue = -0.05;
    }
    this.detailFormControls.get('pricesobervalue')?.patchValue(soberValue);
  }

  calculatePriceEnrichmentValue(data: any) {
    let ppE = data.enrichment;
    let enrichmentValue;
    if (ppE) {
      enrichmentValue = 0.02;
    } else {
      enrichmentValue = 0;
    }
    this.detailFormControls.get('priceenrichmentvalue')?.patchValue(enrichmentValue);
  }

  calculatePriceConceptPremiumValue(data: any) {
    let concept = data.concept;
    let premiumValue;
    switch (concept?.toLowerCase()) {
      case 'bio':
        premiumValue = 0.05;
        break;
      case 'blk1_ast':
        premiumValue = 0.04;
        break;
      case 'gnk':
        premiumValue = 0.03;
        break;
      case 'regular':
        premiumValue = 0.02;
        break;
      default:
        break;
    }

    this.detailFormControls.get('priceconceptpremiumvalue')?.patchValue(premiumValue);
  }

  calculatePriceLoadingCostValue(data: any) {
    const loadingCostVal = this.detailFormControls.getRawValue()?.certificateId ? 0.01 : 0;
    this.detailFormControls.get('priceloadingcostvalue')?.patchValue(loadingCostVal);
  }

  /* Method to validate the values of losses fields */
  validateLosses(currentData: any, prevData: any) {
    let errors = [];
    let errorFields = [];
    if ((currentData.losses1stWeek && currentData.lossesTotal) && currentData.losses1stWeek > currentData.lossesTotal) {
      errors.push("Losses 1st week cannot be greater than Losses Total");
      errorFields.push('losses1stWeek');
      errorFields.push('lossesTotal');
    }
    if ((currentData.lossesTotal && currentData.quantityInstalledInStable) && currentData.lossesTotal > currentData.quantityInstalledInStable) {
      errors.push("Losses Total cannot be greater Quantity Installed In Stable")
      errorFields.push('lossesTotal');
      errorFields.push('quantityInstalledInStable');
    }

    if ((currentData.selected1stWeek && currentData.losses1stWeek) && currentData.selected1stWeek > currentData.losses1stWeek) {
      errors.push("Selected 1st Week cannot be greater Losses 1st Week")
      errorFields.push('selected1stWeek');
      errorFields.push('losses1stWeek');
    }

    if ((currentData.selectedTotal && currentData.lossesTotal) && currentData.selectedTotal > currentData.lossesTotal) {
      errors.push("Selected Total cannot be greater Losses Total")
      errorFields.push('selectedTotal');
      errorFields.push('lossesTotal');
    }

    if ((currentData.legSelectionTotal && currentData.lossesTotal) && currentData.legSelectionTotal > currentData.lossesTotal) {
      errors.push("Let Selection Total cannot be greater Losses Total")
      errorFields.push('legSelectionTotal');
      errorFields.push('lossesTotal');
    }
    if (errors.length) {
      this.showMessage({ severity: 'error', summary: '', detail: errors.join(", ") });
      /* let valToPatch: any = {};
      errorFields.map((ef: any) => {
        valToPatch[ef] = prevData[ef];
        this.detailFormControls.get(ef)?.patchValue(prevData[ef] || '');
      }); */
    }
  }

  /* Method to do custom calculations on the fields.
  *  This is needed when formula editor is not sufficient
  */
  initializeCalculations() {
    this.detailFormControls.valueChanges
      .pipe(startWith(this.detailFormControls.value), pairwise())
      .subscribe(([prevData, currentData]) => {
        if ((currentData.slaughterDate && currentData.installDate)
          && (currentData.slaughterDate != prevData.slaughterDate)
          || (currentData.installDate != prevData.installDate)) {
          this.calculateAge(currentData);
        }

        if (currentData.weighingExternally != prevData.weighingExternally) {
          this.calculateWeighingBridgeDiscount(currentData);
        }

        if (currentData.voerkroppen != prevData.voerkroppen) {
          this.calculateFeedDiscount(currentData);
        }

        if (currentData.averageWeight != prevData.averageWeight || currentData.netWeight != prevData.netWeight || currentData.quantitySupplied != prevData.quantitySupplied) {
          this.calculatePriceBaseValue(currentData);
        }

        if (currentData.srmDeviation != prevData.srmDeviation
          || currentData.averageWeight != prevData.averageWeight
          || currentData.srmNotification != prevData.srmNotification) {
          this.calculatePricesrmvalue(currentData);
        }

        if (currentData.voerkroppen != prevData.voerkroppen) {
          this.calculatePriceSoberValue(currentData);
        }

        if (currentData.enrichment != prevData.enrichment) {
          this.calculatePriceEnrichmentValue(currentData);
        }

        if (currentData.concept != prevData.concept) {
          this.calculatePriceConceptPremiumValue(currentData);
        }

        if ((currentData.losses1stWeek && (currentData.losses1stWeek != prevData.losses1stWeek))
          || (currentData.lossesTotal && (currentData.lossesTotal != prevData.lossesTotal))
          || (currentData.quantityInstalledInStable && (currentData.quantityInstalledInStable != prevData.quantityInstalledInStable))
          || (currentData.selected1stWeek && (currentData.selected1stWeek != prevData.selected1stWeek))
          || (currentData.selectedTotal && (currentData.selectedTotal != prevData.selectedTotal))
          || (currentData.legSelectionTotal && (currentData.legSelectionTotal != prevData.legSelectionTotal))) {
          this.validateLosses(currentData, prevData);
        }

        if (currentData.certificateId != prevData.certificateId) {
          this.calculatePriceLoadingCostValue(currentData);
        }

        this.highlightChangedFields(prevData, currentData);

      });
  }

  /* Method to get all the fields that are changed */
  getChangedFields(prevData: any, currentData: any) {
    const changedFields: any = [];
    Object.keys(currentData).map((key: any) => {
      if (prevData.hasOwnProperty(key) && prevData[key] != currentData[key]) {
        changedFields.push(key);
      }
    })
    return changedFields;
  }

  /* Method to highlight the calculated fields when the change */
  highlightChangedFields(prevData: any, currentData: any) {
    const changedFields: any = this.getChangedFields(prevData, currentData);
    changedFields.map((field: any) => {
      console.log(field, ' : ', this.detailFormControls.get(field)?.touched);
      if (this.detailFormControls.get(field)?.touched == false && this.detailFormControls.get(field)?.dirty == false) {
        //console.log("highlightField", field);
        console.log($(`[formcontrolname = ${field}]`))

        $(`[formcontrolname = ${field}]`).addClass('highlight');
        setTimeout(() => {
          $(`[formcontrolname = ${field}]`).removeClass('highlight');
        }, 500);
      }
    })
  }

  /* Method to manually set the user types in the initial form.
  *  Ideally this should be handled by the workflow logic.
  *  It will be added in the future releases
  */
  getUserTypes(currentUser: any = {}) {
    const uTypes = [];
    if (currentUser.agriApprover) { uTypes.push('agriApprover') }
    if (currentUser.centralAdministratorDe) { uTypes.push('centralAdministratorDe') }
    if (currentUser.centralAdministratorNl) { uTypes.push('centralAdministratorNl') }
    if (currentUser.localAdministrator) { uTypes.push('localAdministrator') }
    if (currentUser.devAdmin) { uTypes.push('localAdministrator') }
    return uTypes;
  }

  setReportId() {
    this.detailFormControls.patchValue({
      "reportId": Math.round(Math.random() * 100000)
    });
  }

  setInitialWorkflow() {
    if (this.activatedRoute?.snapshot?.queryParams?.id) {
      return;
    }

    this.setReportId();
    const currentUser = this.appGlobalService.get("currentUser")?.[0] || {}
    const userTypes = this.getUserTypes(currentUser);

    this.data = {
      ...this.data,
      status: 'SUPPLY_AND_FLOCK_DATA',
      workflowInfo: {
        [this.workflowType]: {
          userTypes: userTypes,
          step: this.workFlowInitialState.toLowerCase()
        }
      }
    }
    const params = {
      workflowType: this.workflowType
    }
    this.appBaseService.getWorkFlowConfig(params).subscribe((res: any) => {
      this.securityJson = res.config;
      this.configureFormOnWorkflow();
    })
  }

  hideSectionsBasedOnWorkflow() {
    if (!this.data || Object.keys(this.data).length == 0) {
      setTimeout(() => {
        this.hideSectionsBasedOnWorkflow();
      }, 500);
      return;
    }
    let sectionsToHide: any = [];
    switch (this.data.status?.toLowerCase()) {
      case "supply_and_flock_data":
        sectionsToHide = [...sectionsToHide, ["#sectionPURCHASE_PRICING", "#sectionCERTIFICATES", "#sectionTOTAL"]]
        break;
      case "slaughter_details":
        sectionsToHide = [...sectionsToHide, ["#sectionPRICING"]];
        break;
      case "to_be_approved":
        sectionsToHide = [...sectionsToHide, ["#sectionCERTIFICATES"]];
        break;
      case "finalized":
      case "cancelled": break;
      case "purchase_pricing":
        sectionsToHide = [...sectionsToHide, ["#sectionCERTIFICATES"]];
        break;
      default: break;
    }

    //TODO: Hide sections based on workflow
    $(sectionsToHide.join(",")).attr("hidden", "true");
  }

  ngAfterViewInit(): void {
    this.onAfterViewInit();
    this.setInitialValues();
    this.setInitialWorkflow();
    this.hideSectionsBasedOnWorkflow()
    this.initializeCalculations();
  }

  ngOnInit(): void {
    super.onInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

}
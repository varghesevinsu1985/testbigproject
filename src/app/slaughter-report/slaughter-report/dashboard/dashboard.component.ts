import { Component, OnInit } from '@angular/core';
import { DashboardBaseComponent } from '@baseapp/slaughter-report/slaughter-report/dashboard/dashboard.base.component';
import { SlaughterReportService } from '@baseapp/slaughter-report/slaughter-report/slaughter-report.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.scss']
})
export class DashboardComponent extends DashboardBaseComponent implements OnInit {
  
  pageViewTitle: string = 'Dashboard';
  
	
  ngAfterViewInit(): void {
    this.onAfterViewInit()
  }

  ngOnInit(): void {
    super.onInit();
  }
 
}

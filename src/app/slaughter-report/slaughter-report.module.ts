import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SlaughterReportRoutingModule } from './slaughter-report-routing.module';
import { SlaughterReportBaseModule } from '@baseapp/slaughter-report/slaughter-report.base.module';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    SlaughterReportBaseModule,
    SlaughterReportRoutingModule
    
  ],
  exports: [
      SlaughterReportBaseModule,
  ]

})
export class SlaughterReportModule  { }
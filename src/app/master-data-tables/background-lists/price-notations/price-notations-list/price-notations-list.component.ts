import { allowedValuesValidator } from '@baseapp/widgets/validators/allowedValuesValidator';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OnInit, ElementRef, Renderer2, ViewChild, Component } from '@angular/core';
import { AppGlobalService } from '@baseapp/app-global.service';
import { DomSanitizer } from '@angular/platform-browser';
import { DialogService } from 'primeng/dynamicdialog';
import { fromEvent, Subscription, map } from 'rxjs';
import { Filter } from '@baseapp/vs-models/filter.model';
import { AppConstants } from '@app/app-constants';
import { ConfirmationService, MessageService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { PriceNotationsListBaseComponent } from '@baseapp/master-data-tables/background-lists/price-notations/price-notations-list/price-notations-list.base.component';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component';
import { environment } from '@env/environment';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { PriceNotationsService } from '@baseapp/master-data-tables/background-lists/price-notations/price-notations.service';
import { BaseAppConstants } from '@baseapp/app-constants.base';


@Component({
  selector: 'app-price-notations-list',
  templateUrl: '../../../../../../base/app/master-data-tables/background-lists/price-notations/price-notations-list/price-notations-list.component.html',
  styleUrls: ['./price-notations-list.scss']
})
export class PriceNotationsListComponent extends PriceNotationsListBaseComponent implements OnInit {
 
  constructor(public override priceNotationsService: PriceNotationsService, public override appUtilBaseService: AppUtilBaseService, public override translateService: TranslateService, public override messageService: MessageService, public override confirmationService: ConfirmationService, public override dialogService: DialogService, public override domSanitizer: DomSanitizer, public override bsModalService: BsModalService, public override activatedRoute: ActivatedRoute, public override renderer2: Renderer2, public override router: Router, public override appGlobalService: AppGlobalService) {
    super(priceNotationsService, appUtilBaseService, translateService, messageService, confirmationService, dialogService, domSanitizer, bsModalService, activatedRoute, renderer2, router, appGlobalService);
  }
	
  ngAfterViewInit(): void {
    this.onAfterViewInit()
  }

  ngOnInit(): void {
    super.onInit();
  }
 
}
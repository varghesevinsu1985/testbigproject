import { allowedValuesValidator } from '@baseapp/widgets/validators/allowedValuesValidator';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OnInit, ElementRef, Renderer2, ViewChild, Component } from '@angular/core';
import { AppGlobalService } from '@baseapp/app-global.service';
import { DomSanitizer } from '@angular/platform-browser';
import { DialogService } from 'primeng/dynamicdialog';
import { fromEvent, Subscription, map } from 'rxjs';
import { Filter } from '@baseapp/vs-models/filter.model';
import { AppConstants } from '@app/app-constants';
import { BreedListBaseComponent } from '@baseapp/master-data-tables/background-lists/breed/breed-list/breed-list.base.component';
import { ConfirmationService, MessageService } from 'primeng/api';
import { BreedService } from '@baseapp/master-data-tables/background-lists/breed/breed.service';
import { TranslateService } from '@ngx-translate/core';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component';
import { environment } from '@env/environment';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { BaseAppConstants } from '@baseapp/app-constants.base';


@Component({
  selector: 'app-breed-list',
  templateUrl: '../../../../../../base/app/master-data-tables/background-lists/breed/breed-list/breed-list.component.html',
  styleUrls: ['./breed-list.scss']
})
export class BreedListComponent extends BreedListBaseComponent implements OnInit {
 
  constructor(public override breedService: BreedService, public override appUtilBaseService: AppUtilBaseService, public override translateService: TranslateService, public override messageService: MessageService, public override confirmationService: ConfirmationService, public override dialogService: DialogService, public override domSanitizer: DomSanitizer, public override bsModalService: BsModalService, public override activatedRoute: ActivatedRoute, public override renderer2: Renderer2, public override router: Router, public override appGlobalService: AppGlobalService) {
    super(breedService, appUtilBaseService, translateService, messageService, confirmationService, dialogService, domSanitizer, bsModalService, activatedRoute, renderer2, router, appGlobalService);
  }
	
  ngAfterViewInit(): void {
    this.onAfterViewInit()
  }

  ngOnInit(): void {
    super.onInit();
  }
 
}
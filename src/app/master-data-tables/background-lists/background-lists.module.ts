import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BackgroundListsRoutingModule } from './background-lists-routing.module';
import { BackgroundListsBaseModule } from '@baseapp/master-data-tables/background-lists/background-lists.base.module';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    BackgroundListsBaseModule,
    BackgroundListsRoutingModule
    
  ],
  exports: [
      BackgroundListsBaseModule,
  ]

})
export class BackgroundListsModule  { }
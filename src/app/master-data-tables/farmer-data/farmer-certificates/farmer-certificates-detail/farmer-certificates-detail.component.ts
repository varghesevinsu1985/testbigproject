import { BaseService } from '@baseapp/base.service';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { UploaderService } from '@baseapp/upload-attachment.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OnInit, Component } from '@angular/core';
import { FarmerCertificatesDetailBaseComponent } from '@baseapp/master-data-tables/farmer-data/farmer-certificates/farmer-certificates-detail/farmer-certificates-detail.base.component';
import { ActionItem } from '@baseapp/widgets/action-bar/action-bar.component';
import { AppGlobalService } from '@baseapp/app-global.service';
import { AppConstants } from '@app/app-constants';
import { BreadcrumbService } from '@baseapp/widgets/breadcrumb/breadcrumb.service';
import { FarmerCertificatesService } from '@baseapp/master-data-tables/farmer-data/farmer-certificates/farmer-certificates.service';
import { environment } from '@env/environment';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { ConfirmationPopupComponent } from '@baseapp/widgets/confirmation/confirmation-popup.component';
import { BaseAppConstants } from '@baseapp/app-constants.base';
import { allowedValuesValidator } from '@baseapp/widgets/validators/allowedValuesValidator';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { dateValidator } from '@baseapp/widgets/validators/dateValidator';
import { DialogService } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { WorkflowSimulatorComponent } from '@baseapp/widgets/workflow-simulator/workflow-simulator.component';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component';
import { WorkflowHistoryComponent } from '@baseapp/widgets/workflow-history/workflow-history.component';
import { debounceTime, fromEvent, distinctUntilChanged, of, Observer, Subscription, map, Observable, Subject } from 'rxjs';
import { AppBaseService } from '@baseapp/app.base.service';


@Component({
  selector: 'app-farmer-certificates-detail',
  templateUrl: '../../../../../../base/app/master-data-tables/farmer-data/farmer-certificates/farmer-certificates-detail/farmer-certificates-detail.component.html',
  styleUrls: ['./farmer-certificates-detail.scss']
})
export class FarmerCertificatesDetailComponent extends FarmerCertificatesDetailBaseComponent implements OnInit {
 
  constructor(public override farmerCertificatesService: FarmerCertificatesService, public override appUtilBaseService: AppUtilBaseService, public override translateService: TranslateService, public override messageService: MessageService, public override confirmationService: ConfirmationService, public override dialogService: DialogService, public override domSanitizer: DomSanitizer, public override bsModalService: BsModalService, public override activatedRoute: ActivatedRoute, public override breadcrumbService: BreadcrumbService, public override appBaseService: AppBaseService, public override router: Router, public override appGlobalService: AppGlobalService, public override uploaderService: UploaderService, public override baseService: BaseService, public override location: Location) {
    super(farmerCertificatesService, appUtilBaseService, translateService, messageService, confirmationService, dialogService, domSanitizer, bsModalService, activatedRoute, breadcrumbService, appBaseService, router, appGlobalService, uploaderService, baseService, location);
  }
	
  ngAfterViewInit(): void {
    this.onAfterViewInit()
  }

  ngOnInit(): void {
    super.onInit();
  }
 
}
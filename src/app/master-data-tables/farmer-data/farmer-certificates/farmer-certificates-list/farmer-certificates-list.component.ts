import { BaseService } from '@baseapp/base.service';
import { allowedValuesValidator } from '@baseapp/widgets/validators/allowedValuesValidator';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OnInit, ElementRef, Renderer2, ViewChild, Component } from '@angular/core';
import { AppGlobalService } from '@baseapp/app-global.service';
import { FarmerCertificatesListBaseComponent } from '@baseapp/master-data-tables/farmer-data/farmer-certificates/farmer-certificates-list/farmer-certificates-list.base.component';
import { DomSanitizer } from '@angular/platform-browser';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConstants } from '@app/app-constants';
import { fromEvent, Subscription, map } from 'rxjs';
import { Filter } from '@baseapp/vs-models/filter.model';
import { ConfirmationService, MessageService } from 'primeng/api';
import { FarmerCertificatesService } from '@baseapp/master-data-tables/farmer-data/farmer-certificates/farmer-certificates.service';
import { TranslateService } from '@ngx-translate/core';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component';
import { environment } from '@env/environment';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { BaseAppConstants } from '@baseapp/app-constants.base';


@Component({
  selector: 'app-farmer-certificates-list',
  templateUrl: '../../../../../../base/app/master-data-tables/farmer-data/farmer-certificates/farmer-certificates-list/farmer-certificates-list.component.html',
  styleUrls: ['./farmer-certificates-list.scss']
})
export class FarmerCertificatesListComponent extends FarmerCertificatesListBaseComponent implements OnInit {
 
  constructor(public override farmerCertificatesService: FarmerCertificatesService, public override appUtilBaseService: AppUtilBaseService, public override translateService: TranslateService, public override messageService: MessageService, public override confirmationService: ConfirmationService, public override dialogService: DialogService, public override domSanitizer: DomSanitizer, public override bsModalService: BsModalService, public override activatedRoute: ActivatedRoute, public override renderer2: Renderer2, public override router: Router, public override appGlobalService: AppGlobalService, public override baseService: BaseService) {
    super(farmerCertificatesService, appUtilBaseService, translateService, messageService, confirmationService, dialogService, domSanitizer, bsModalService, activatedRoute, renderer2, router, appGlobalService, baseService);
  }
	
  ngAfterViewInit(): void {
    this.onAfterViewInit()
  }

  ngOnInit(): void {
    super.onInit();
  }
 
}
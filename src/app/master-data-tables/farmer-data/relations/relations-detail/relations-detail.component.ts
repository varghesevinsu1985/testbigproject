import { BaseService } from '@baseapp/base.service';
import { RelationsDetailBaseComponent } from '@baseapp/master-data-tables/farmer-data/relations/relations-detail/relations-detail.base.component';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { UploaderService } from '@baseapp/upload-attachment.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { OnInit, Component } from '@angular/core';
import { ActionItem } from '@baseapp/widgets/action-bar/action-bar.component';
import { AppGlobalService } from '@baseapp/app-global.service';
import { AppConstants } from '@app/app-constants';
import { BreadcrumbService } from '@baseapp/widgets/breadcrumb/breadcrumb.service';
import { environment } from '@env/environment';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { ConfirmationPopupComponent } from '@baseapp/widgets/confirmation/confirmation-popup.component';
import { BaseAppConstants } from '@baseapp/app-constants.base';
import { allowedValuesValidator } from '@baseapp/widgets/validators/allowedValuesValidator';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { dateValidator } from '@baseapp/widgets/validators/dateValidator';
import { DialogService } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';
import { WorkflowSimulatorComponent } from '@baseapp/widgets/workflow-simulator/workflow-simulator.component';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component';
import { WorkflowHistoryComponent } from '@baseapp/widgets/workflow-history/workflow-history.component';
import { debounceTime, fromEvent, distinctUntilChanged, of, Observer, Subscription, map, Observable, Subject } from 'rxjs';
import { AppBaseService } from '@baseapp/app.base.service';
import { RelationsService } from '@baseapp/master-data-tables/farmer-data/relations/relations.service';


@Component({
  selector: 'app-relations-detail',
  templateUrl: '../../../../../../base/app/master-data-tables/farmer-data/relations/relations-detail/relations-detail.component.html',
  styleUrls: ['./relations-detail.scss']
})
export class RelationsDetailComponent extends RelationsDetailBaseComponent implements OnInit {
 
  constructor(public override relationsService: RelationsService, public override appUtilBaseService: AppUtilBaseService, public override translateService: TranslateService, public override messageService: MessageService, public override confirmationService: ConfirmationService, public override dialogService: DialogService, public override domSanitizer: DomSanitizer, public override bsModalService: BsModalService, public override activatedRoute: ActivatedRoute, public override breadcrumbService: BreadcrumbService, public override appBaseService: AppBaseService, public override router: Router, public override appGlobalService: AppGlobalService, public override uploaderService: UploaderService, public override baseService: BaseService, public override location: Location) {
    super(relationsService, appUtilBaseService, translateService, messageService, confirmationService, dialogService, domSanitizer, bsModalService, activatedRoute, breadcrumbService, appBaseService, router, appGlobalService, uploaderService, baseService, location);
  }
	
  ngAfterViewInit(): void {
    this.onAfterViewInit()
  }

  ngOnInit(): void {
    super.onInit();
  }
 
}
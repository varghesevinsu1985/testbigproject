import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';


@Pipe({
  name: 'relationsPipe'
})
export class RelationsPipe implements PipeTransform { 

  constructor(public _sanitizer: DomSanitizer, public translateService : TranslateService) { } 
  
  transform(data:any,columConfig:any,page:string, rowData:any, fieldName:any): SafeHtml {
    return this._sanitizer.bypassSecurityTrustHtml(data);
  }
}
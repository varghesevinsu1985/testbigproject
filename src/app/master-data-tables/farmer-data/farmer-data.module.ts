import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FarmerDataRoutingModule } from './farmer-data-routing.module';
import { FarmerDataBaseModule } from '@baseapp/master-data-tables/farmer-data/farmer-data.base.module';


@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    FarmerDataBaseModule,
    FarmerDataRoutingModule
    
  ],
  exports: [
      FarmerDataBaseModule,
  ]

})
export class FarmerDataModule  { }
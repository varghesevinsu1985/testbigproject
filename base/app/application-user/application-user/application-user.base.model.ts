export interface ApplicationUserBase {
	modifiedDate: Date;
	createdDate: Date;
	lastName: string;
	userRoles: string;
	createdBy: string;
	sid: string;
	localAdministrator: Boolean;
	centralAdministratorNl: Boolean;
	firstName: string;
	centralAdministratorDe: Boolean;
	modifiedBy: string;
	email: string;
	agriApprover: Boolean;
}
import { SlaughterReportDetailComponent } from '@app/slaughter-report/slaughter-report/slaughter-report-detail/slaughter-report-detail.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SlaughterReportPipe } from '@app/slaughter-report/slaughter-report/slaughter-report.pipe.component';
import { SlaughterReportListComponent } from '@app/slaughter-report/slaughter-report/slaughter-report-list/slaughter-report-list.component';
import { DashboardComponent } from '@app/slaughter-report/slaughter-report/dashboard/dashboard.component';
import { WidgetsBaseModule } from '@baseapp/widgets/widgets.base.module';
import { CanDeactivateGuard } from '@baseapp/auth.can-deactivate-guard.service';

@NgModule({
  declarations: [
SlaughterReportDetailComponent,
SlaughterReportPipe,
SlaughterReportListComponent,
DashboardComponent
],
imports: [
SharedModule,
WidgetsBaseModule
],

exports: [
SharedModule,
WidgetsBaseModule,
SlaughterReportDetailComponent,
SlaughterReportListComponent,
DashboardComponent
],

providers: [
BsModalService,
CanDeactivateGuard
],
  
})
export class SlaughterReportBaseModule { }
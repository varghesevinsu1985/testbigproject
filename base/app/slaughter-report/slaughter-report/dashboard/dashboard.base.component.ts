import { SlaughterReportService } from '../slaughter-report.service';
import { SlaughterReportBase} from '../slaughter-report.base.model';
import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component'

@Directive(
{
	providers:[MessageService, ConfirmationService, DialogService]
}
)
export class DashboardBaseComponent{
	
	bsModalRef?: BsModalRef;
	isChildPage:boolean = false;

	
	


	calculateFormula(){
	
}

    onInit() {
    }
	
     onDestroy() {
    }
     onAfterViewInit() {
    }

}

import { SlaughterReportService } from '../slaughter-report.service';
import { SlaughterReportBase} from '../slaughter-report.base.model';
import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component'
import { BreedApiConstants } from '@baseapp/master-data-tables/background-lists/breed/breed.api-constants';
import { StablesApiConstants } from '@baseapp/master-data-tables/farmer-data/stables/stables.api-constants';
import { CompaniesApiConstants } from '@baseapp/master-data-tables/farmer-data/companies/companies.api-constants';
import { CountriesApiConstants } from '@baseapp/master-data-tables/background-lists/countries/countries.api-constants';
import { RelationTypeApiConstants } from '@baseapp/master-data-tables/background-lists/relation-type/relation-type.api-constants';
import { CertificatesListApiConstants } from '@baseapp/master-data-tables/background-lists/certificates-list/certificates-list.api-constants';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationPopupComponent } from '@baseapp/widgets/confirmation/confirmation-popup.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { debounceTime, fromEvent, distinctUntilChanged, of, Observer, Subscription, map, Observable, Subject } from 'rxjs';
import { environment } from '@env/environment';
import { allowedValuesValidator } from '@baseapp/widgets/validators/allowedValuesValidator';
import { dateValidator } from '@baseapp/widgets/validators/dateValidator';
import { ActionItem } from '@baseapp/widgets/action-bar/action-bar.component';
import { AppConstants } from '@app/app-constants';
import { AppGlobalService } from '@baseapp/app-global.service';
import { AppBaseService } from '@baseapp/app.base.service';
import { WorkflowSimulatorComponent } from '@baseapp/widgets/workflow-simulator/workflow-simulator.component';
import { BreadcrumbService } from '@baseapp/widgets/breadcrumb/breadcrumb.service';
import { UploaderService } from '@baseapp/upload-attachment.service';
import { BaseAppConstants } from '@baseapp/app-constants.base';
import { WorkflowHistoryComponent } from '@baseapp/widgets/workflow-history/workflow-history.component';
import { BaseService } from '@baseapp/base.service';

@Directive(
{
	providers:[MessageService, ConfirmationService, DialogService]
}
)
export class SlaughterReportDetailBaseComponent{
	
	
	isSearchFocused:boolean = false;
showBreadcrumb = AppConstants.showBreadcrumb;
	
filteredItemsdetailFormfromCountry:any = [];
isAutoSuggestCallFireddetailFormfromCountry: boolean = false;
	
filteredItemsdetailFormfeedSupplier:any = [];
isAutoSuggestCallFireddetailFormfeedSupplier: boolean = false;
	
filteredItemsdetailFormweighingrelationtypedefault:any = [];
isAutoSuggestCallFireddetailFormweighingrelationtypedefault: boolean = false;
	
filteredItemsdetailFormhatchery:any = [];
isAutoSuggestCallFireddetailFormhatchery: boolean = false;
	
filteredItemsdetailFormcertificateId:any = [];
isAutoSuggestCallFireddetailFormcertificateId: boolean = false;
	id: any;
pid:any;
isMobile: boolean = AppConstants.isMobile;
errorfields: any = {};           
backupData:any = {};
hiddenFields:any = {};
data:any = {};
formErrors:any = {};
inValidFields:any = {};
formFieldConfig:any = {};
securityJson:any = {
}
formConfig = {};     
actionButtons:ActionItem[] = [];  
wizardItems:any = [];
currentUserData = this.appGlobalService.get('currentUser');
selectedItems:any ={};
workflowType = "slaughterreportsworkflow";
workFlowEnabled = true;
workFlowInitialState = "SUPPLY_AND_FLOCK_DATA";
workFlowField = "status";
workflowActions:any ={
    disableActions:[],
    enableActions:[],
    hideActions:[]
  };
  isFormValueChanged: boolean = false;
  mandatoryFields:any ={};
  validatorsRetained:any ={};
  isSaveResponseReceived:boolean = false;
  formSecurityConfig:any = {};
  enableReadOnly = AppConstants.enableReadOnly;
 showScrollSpy = AppConstants.showScrollSpy;
isRowSelected:boolean = true; 
isPrototype = environment.prototype;
isList = false;
detailPageIgnoreFields:any=['pricesobersum','slaughterPercentage','netWeight','rejectedSlaughterhouse','deadSlaughterhouse','netCount','priceconceptpremiumsum','pricebasesum','density','deadOnArrivalPercentage','totalLesionVzl','totalLesionScoreVzl','rejectedFarmer','priceenrichmentsum','qtySlaughtered','dailyGrowth','deadFarmer','quantityRejectedWholeKg','paidWeight','averageWeight','pricesrmsum','priceloadingcostsum','allRejected','srmDeviation','deadTotal'];
autoSuggestPageNo:number = 0;
complexAutoSuggestPageNo:number = 0
  fieldEditMode: any = {};
conditionalActions:any ={
  disableActions:[],
  hideActions:[]
}
actionBarConfig:any =[];
dateFormat: string = AppConstants.calDateFormat;
subscriptions: Subscription[] = [];
activeTabIndexes: any = {};
 showWorkflowSimulator:boolean = false;
roles:any ={};
	comments: string ='';
confirmationReference:any;
	
filteredItemsdetailFormstableId:any = [];
isAutoSuggestCallFireddetailFormstableId: boolean = false;
	
filteredItemsdetailFormbreed:any = [];
isAutoSuggestCallFireddetailFormbreed: boolean = false;
	
filteredItemsdetailFormtoCountry:any = [];
isAutoSuggestCallFireddetailFormtoCountry: boolean = false;
	bsModalRef?: BsModalRef;
	
filteredItemsdetailFormcatchingrelationtypedefault:any = [];
isAutoSuggestCallFireddetailFormcatchingrelationtypedefault: boolean = false;
	
filteredItemsdetailFormlaboratoryrelationtypedefault:any = [];
isAutoSuggestCallFireddetailFormlaboratoryrelationtypedefault: boolean = false;
	
filteredItemsdetailFormbuyer:any = [];
isAutoSuggestCallFireddetailFormbuyer: boolean = false;
	
filteredItemsdetailFormweighingBridge:any = [];
isAutoSuggestCallFireddetailFormweighingBridge: boolean = false;
	
filteredItemsdetailFormorigin:any = [];
isAutoSuggestCallFireddetailFormorigin: boolean = false;
	
filteredItemsdetailFormveterinarian:any = [];
isAutoSuggestCallFireddetailFormveterinarian: boolean = false;
	
filteredItemsdetailFormlaboratory:any = [];
isAutoSuggestCallFireddetailFormlaboratory: boolean = false;
	
filteredItemsdetailFormbeneficiaryrelationtypedefault:any = [];
isAutoSuggestCallFireddetailFormbeneficiaryrelationtypedefault: boolean = false;
	
filteredItemsdetailFormsellerrelationtypedefault:any = [];
isAutoSuggestCallFireddetailFormsellerrelationtypedefault: boolean = false;
	
filteredItemsdetailFormfeedrelationtypedefault:any = [];
isAutoSuggestCallFireddetailFormfeedrelationtypedefault: boolean = false;
	
filteredItemsdetailFormcatchingCrew:any = [];
isAutoSuggestCallFireddetailFormcatchingCrew: boolean = false;
	isChildPage:boolean = false;
	
filteredItemsdetailFormfarmer:any = [];
isAutoSuggestCallFireddetailFormfarmer: boolean = false;
	
filteredItemsdetailFormbeneficiary:any = [];
isAutoSuggestCallFireddetailFormbeneficiary: boolean = false;

	
	leftActionBarConfig : any = {
  "children" : [ {
    "outline" : true,
    "buttonType" : "icon_on_left",
    "visibility" : "show",
    "showOn" : "both",
    "buttonStyle" : "curved",
    "enableOnlyIfRecordSelected" : false,
    "buttonEnabled" : "yes",
    "action" : "back",
    "label" : "BACK",
    "type" : "button"
  }, {
    "outline" : true,
    "label" : "MAIN_BTNS",
    "type" : "buttonGroup",
    "children" : [ {
      "outline" : true,
      "buttonType" : "icon_on_left",
      "visibility" : "show",
      "showOn" : "both",
      "buttonStyle" : "curved",
      "enableOnlyIfRecordSelected" : false,
      "buttonEnabled" : "yes",
      "action" : "save",
      "label" : "SAVE",
      "type" : "button"
    }, {
      "outline" : true,
      "buttonType" : "icon_on_left",
      "visibility" : "show",
      "showOn" : "both",
      "buttonStyle" : "curved",
      "enableOnlyIfRecordSelected" : false,
      "buttonEnabled" : "yes",
      "action" : "cancel",
      "label" : "CANCEL",
      "type" : "button"
    } ],
    "buttonStyle" : "curved"
  } ]
}
	workflowActionBarConfig : any = {
  "children" : [ {
    "outline" : true,
    "children" : [ {
      "visibility" : "show",
      "buttonStyle" : "curved",
      "currentNode" : "_toSlaughterDetails",
      "label" : "TO_SLAUGHTER_DETAILS",
      "type" : "button",
      "outline" : false,
      "buttonType" : "icon_on_left",
      "service" : {
        "name" : "slaughterReportsWorkflowToSlaughterDetails",
        "tableId" : "f524c64f-f41b-4461-845f-5e127e9c59dc",
        "sid" : "8c1c53ba-0314-45c2-8f46-3404c675d7c3",
        "tableName" : "Slaughter report"
      },
      "valueChange" : true,
      "showOn" : "both",
      "enableOnlyIfRecordSelected" : false,
      "buttonEnabled" : "yes",
      "action" : "call_a_backend_webservice",
      "wfAction" : "toSlaughterDetails",
      "font" : {
        "fontSize" : "25px"
      }
    }, {
      "visibility" : "show",
      "buttonStyle" : "curved",
      "currentNode" : "_toPurchasePricing",
      "label" : "TO_PURCHASE_PRICING",
      "type" : "button",
      "outline" : true,
      "buttonType" : "icon_on_left",
      "service" : {
        "name" : "slaughterReportsWorkflowToPurchasePricing",
        "sid" : "f03ae58d-2c96-44ce-bd78-9396c56676cd",
        "tableName" : "Slaughter report",
        "tableId" : "f524c64f-f41b-4461-845f-5e127e9c59dc"
      },
      "valueChange" : true,
      "showOn" : "both",
      "enableOnlyIfRecordSelected" : false,
      "buttonEnabled" : "yes",
      "action" : "call_a_backend_webservice",
      "wfAction" : "toPurchasePricing"
    }, {
      "outline" : true,
      "buttonType" : "icon_on_left",
      "visibility" : "show",
      "service" : {
        "name" : "slaughterReportsWorkflowPricingDone",
        "tableId" : "f524c64f-f41b-4461-845f-5e127e9c59dc",
        "sid" : "ab7464aa-ead2-40a5-bd10-cfcd92d51085",
        "tableName" : "Slaughter report"
      },
      "showOn" : "both",
      "buttonStyle" : "curved",
      "buttonEnabled" : "yes",
      "action" : "call_a_backend_webservice",
      "label" : "PRICING_DONE",
      "type" : "button",
      "wfAction" : "pricingDone"
    }, {
      "outline" : true,
      "buttonType" : "icon_on_left",
      "visibility" : "show",
      "service" : {
        "name" : "slaughterReportsWorkflowBackToSupplyAndFlockData",
        "tableId" : "f524c64f-f41b-4461-845f-5e127e9c59dc",
        "sid" : "e21c6061-a7a4-4d2a-9801-4727e67a586b",
        "tableName" : "Slaughter report"
      },
      "showOn" : "both",
      "buttonStyle" : "curved",
      "buttonEnabled" : "yes",
      "action" : "call_a_backend_webservice",
      "label" : "BACK_TO_SUPPLY_AND_FLOCK_DATA",
      "type" : "button",
      "wfAction" : "backToSupplyAndFlockData"
    }, {
      "outline" : true,
      "buttonType" : "icon_on_left",
      "visibility" : "show",
      "service" : {
        "name" : "slaughterReportsWorkflowBackToSlaughterDetails",
        "tableId" : "f524c64f-f41b-4461-845f-5e127e9c59dc",
        "sid" : "7b86196a-5587-44c6-b2b8-ded9440f6eba",
        "tableName" : "Slaughter report"
      },
      "showOn" : "both",
      "buttonStyle" : "curved",
      "buttonEnabled" : "yes",
      "action" : "call_a_backend_webservice",
      "label" : "BACK_TO_SLAUGHTER_DETAILS",
      "type" : "button",
      "wfAction" : "backToSlaughterDetails"
    }, {
      "outline" : true,
      "buttonType" : "icon_on_left",
      "visibility" : "show",
      "service" : {
        "name" : "slaughterReportsWorkflowApprove",
        "tableId" : "f524c64f-f41b-4461-845f-5e127e9c59dc",
        "sid" : "f0553f19-ef6b-4220-997f-ca05ca4046cd",
        "tableName" : "Slaughter report"
      },
      "showOn" : "both",
      "buttonStyle" : "curved",
      "buttonEnabled" : "yes",
      "action" : "call_a_backend_webservice",
      "label" : "APPROVE",
      "type" : "button",
      "wfAction" : "approve"
    }, {
      "outline" : true,
      "buttonType" : "icon_on_left",
      "visibility" : "show",
      "service" : {
        "name" : "slaughterReportsWorkflowCancelBatch",
        "tableId" : "f524c64f-f41b-4461-845f-5e127e9c59dc",
        "sid" : "82172546-fe26-46c1-8543-0b7125795bf0",
        "tableName" : "Slaughter report"
      },
      "showOn" : "both",
      "buttonStyle" : "curved",
      "buttonEnabled" : "yes",
      "action" : "call_a_backend_webservice",
      "label" : "CANCEL_BATCH",
      "type" : "button",
      "wfAction" : "cancelBatch"
    }, {
      "outline" : true,
      "buttonType" : "icon_on_left",
      "visibility" : "show",
      "service" : {
        "name" : "slaughterReportsWorkflowNotApproved",
        "tableId" : "f524c64f-f41b-4461-845f-5e127e9c59dc",
        "sid" : "d8a94abf-63d1-497b-ae22-af9b1ccec9de",
        "tableName" : "Slaughter report"
      },
      "showOn" : "both",
      "buttonStyle" : "curved",
      "buttonEnabled" : "yes",
      "action" : "call_a_backend_webservice",
      "label" : "NOT_APPROVED",
      "type" : "button",
      "wfAction" : "notApproved"
    }, {
      "outline" : true,
      "buttonType" : "icon_on_left",
      "visibility" : "show",
      "service" : {
        "name" : "slaughterReportsWorkflowNoaction",
        "tableId" : "f524c64f-f41b-4461-845f-5e127e9c59dc",
        "sid" : "67264bfb-f498-433b-a018-4eeb5e34a3f1",
        "tableName" : "Slaughter report"
      },
      "showOn" : "both",
      "buttonStyle" : "curved",
      "buttonEnabled" : "yes",
      "action" : "call_a_backend_webservice",
      "label" : "NOACTION",
      "type" : "button",
      "wfAction" : "noaction"
    } ],
    "valueChange" : true,
    "buttonStyle" : "curved",
    "displayCount" : 1,
    "currentNode" : "_14",
    "label" : "BUTTON_GROUP",
    "type" : "buttonGroup"
  } ],
  "label" : "Workflow Action Bar",
  "type" : "workflowActionBar"
}
	detailCaptionBarConfig : any = {
  "children" : [ {
    "showOn" : "both",
    "data" : "Slaughter Date",
    "field" : "slaughterDate",
    "fieldType" : "Date",
    "fieldId" : "slaughterDate",
    "timeOnly" : false,
    "uiType" : "date",
    "name" : "slaughterDate",
    "type" : "captionItem",
    "label" : "SLAUGHTER_DATE",
    "fieldName" : "slaughterDate"
  }, {
    "showOn" : "both",
    "data" : "Status",
    "field" : "status",
    "fieldType" : "string",
    "multipleValues" : false,
    "uiType" : "select",
    "allowedValues" : {
      "values" : [ {
        "label" : "SUPPLY_AND_FLOCK_DATA",
        "value" : "SUPPLY_AND_FLOCK_DATA"
      }, {
        "label" : "SLAUGHTER_DETAILS",
        "value" : "SLAUGHTER_DETAILS"
      }, {
        "label" : "PURCHASE_PRICING",
        "value" : "PURCHASE_PRICING"
      }, {
        "label" : "TO_BE_APPROVED",
        "value" : "TO_BE_APPROVED"
      }, {
        "label" : "FINALIZED",
        "value" : "FINALIZED"
      }, {
        "label" : "CANCELLED",
        "value" : "CANCELLED"
      } ],
      "conditions" : {
        "conditionType" : "None",
        "conditions" : [ ]
      }
    },
    "fieldId" : "status",
    "timeOnly" : false,
    "name" : "status",
    "type" : "captionItem",
    "label" : "STATUS",
    "fieldName" : "status"
  } ]
}
	detailFormStructureConfig : any = {
  "children" : [ {
    "type" : "formSection",
    "children" : [ ],
    "label" : "HEADER"
  }, {
    "currentNode" : "0d4631bf-9d16-4987-8531-686feddb78f0",
    "label" : "REMARKS",
    "type" : "formSection",
    "children" : [ ],
    "columns" : "4",
    "valueChange" : true
  }, {
    "currentNode" : "6b1488b9-c51f-4867-a79d-3992001ee072",
    "label" : "SUPPLY_AND_FLOCK_DATA",
    "type" : "formSection",
    "children" : [ {
      "type" : "formSection",
      "children" : [ {
        "type" : "formSection",
        "children" : [ ],
        "label" : "LOSSES"
      } ],
      "label" : "FLOCK_DETAILS"
    }, {
      "type" : "formSection",
      "children" : [ ],
      "label" : "BATCH_DETAILS"
    }, {
      "type" : "formSection",
      "children" : [ ],
      "label" : "RELATIONS"
    } ],
    "columns" : "4",
    "valueChange" : true
  }, {
    "currentNode" : "792db02b-5632-487d-ad89-63b82e557479",
    "label" : "SLAUGHTER_DETAILS",
    "type" : "formSection",
    "children" : [ {
      "type" : "formSection",
      "children" : [ ],
      "label" : "RESULT"
    }, {
      "type" : "formSection",
      "children" : [ ],
      "label" : "QUALITY_PARAMETERS"
    }, {
      "type" : "formSection",
      "children" : [ ],
      "label" : "SLAUGHTER_DETAILS"
    }, {
      "currentNode" : "ce154e02-5407-4d9f-8249-c128504e60d6",
      "label" : "CERTIFICATES",
      "type" : "formSection",
      "children" : [ ],
      "columns" : "2",
      "valueChange" : true
    } ],
    "columns" : "4",
    "valueChange" : true
  }, {
    "currentNode" : "0f7f217c-2607-4e9b-8f03-f2e0d1e17bc4",
    "label" : "PRICING",
    "type" : "formSection",
    "children" : [ {
      "type" : "formSection",
      "children" : [ ],
      "label" : "PURCHASE_PRICING"
    }, {
      "type" : "formSection",
      "children" : [ ],
      "label" : "PRICE_PARAMETERS"
    }, {
      "label" : "TOTAL",
      "type" : "formSection",
      "children" : [ ],
      "columns" : "1"
    } ],
    "columns" : "3",
    "valueChange" : true
  } ]
}
	detailFormConfig : any = {
  "children" : [ {
    "label" : "SLAUGHTER_REPORT_STATUS",
    "field" : "status",
    "data" : "Status",
    "currentNode" : "eeae8111-12fa-454d-9cb2-b9804a68cc81",
    "valueChange" : true,
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldType" : "string",
    "uiType" : "select",
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "allowedValues" : {
      "values" : [ {
        "label" : "SUPPLY_AND_FLOCK_DATA",
        "value" : "SUPPLY_AND_FLOCK_DATA"
      }, {
        "label" : "SLAUGHTER_DETAILS",
        "value" : "SLAUGHTER_DETAILS"
      }, {
        "label" : "PURCHASE_PRICING",
        "value" : "PURCHASE_PRICING"
      }, {
        "label" : "TO_BE_APPROVED",
        "value" : "TO_BE_APPROVED"
      }, {
        "label" : "FINALIZED",
        "value" : "FINALIZED"
      }, {
        "label" : "CANCELLED",
        "value" : "CANCELLED"
      } ],
      "conditions" : {
        "conditionType" : "None",
        "conditions" : [ ]
      }
    },
    "fieldId" : "status",
    "defaultVal" : "",
    "placeHolder" : "",
    "helpText" : "",
    "infoBubble" : "",
    "timeOnly" : false,
    "name" : "status",
    "type" : "formField",
    "fieldName" : "status"
  }, {
    "data" : "",
    "field" : "reportId",
    "label" : "REPORT_ID",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "reportId",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "reportId",
    "type" : "formField",
    "fieldName" : "reportId"
  }, {
    "data" : "",
    "field" : "sequenceNr",
    "label" : "SEQUENCE_NR",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "sequenceNr",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "sequenceNr",
    "type" : "formField",
    "fieldName" : "sequenceNr"
  }, {
    "data" : "",
    "field" : "remarkGrower",
    "label" : "REMARK_GROWER",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "remarkGrower",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "textarea",
    "name" : "remarkGrower",
    "type" : "formField",
    "fieldName" : "remarkGrower"
  }, {
    "data" : "",
    "field" : "remarkSeller",
    "label" : "REMARK_SELLER",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "remarkSeller",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "textarea",
    "name" : "remarkSeller",
    "type" : "formField",
    "fieldName" : "remarkSeller"
  }, {
    "data" : "",
    "field" : "remarkSlaughterhouse",
    "label" : "REMARK_SLAUGHTERHOUSE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "remarkSlaughterhouse",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "textarea",
    "name" : "remarkSlaughterhouse",
    "type" : "formField",
    "fieldName" : "remarkSlaughterhouse"
  }, {
    "data" : "",
    "field" : "puchaseNote",
    "label" : "PUCHASE_NOTE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "puchaseNote",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "textarea",
    "name" : "puchaseNote",
    "type" : "formField",
    "fieldName" : "puchaseNote"
  }, {
    "data" : "",
    "field" : "flockNumber",
    "label" : "FLOCK_NUMBER",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "flockNumber",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "flockNumber",
    "type" : "formField",
    "fieldName" : "flockNumber"
  }, {
    "data" : "",
    "field" : "quantityInstalledInStable",
    "label" : "QUANTITY_INSTALLED_IN_STABLE",
    "mandatory" : "yes",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "quantityInstalledInStable",
    "minVal" : 1000.0,
    "maxVal" : 150000.0,
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "quantityInstalledInStable",
    "type" : "formField",
    "fieldName" : "quantityInstalledInStable"
  }, {
    "data" : "",
    "field" : "installDate",
    "label" : "INSTALL_DATE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "installDate",
    "timeOnly" : false,
    "fieldType" : "Date",
    "uiType" : "date",
    "name" : "installDate",
    "type" : "formField",
    "fieldName" : "installDate"
  }, {
    "data" : "",
    "field" : "breed",
    "label" : "BREED",
    "mandatory" : "yes",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "breedCode", "breedName" ],
    "functionalPrimaryKey" : [ "breedCode" ],
    "displayField" : "breedCode",
    "lookupUrl" : "breeds/autosuggest",
    "fieldId" : "breed",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "breed",
    "type" : "formField",
    "fieldName" : "breed"
  }, {
    "data" : "",
    "field" : "thinningYesno",
    "label" : "THINNING_YESNO",
    "mandatory" : "yes",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "thinningYesno",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "select",
    "name" : "thinningYesno",
    "type" : "formField",
    "fieldName" : "thinningYesno"
  }, {
    "data" : "",
    "field" : "roundNr",
    "label" : "ROUND_NR",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "roundNr",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "roundNr",
    "type" : "formField",
    "fieldName" : "roundNr"
  }, {
    "data" : "",
    "field" : "age",
    "label" : "AGE",
    "mandatory" : "yes",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "age",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "age",
    "type" : "formField",
    "fieldName" : "age"
  }, {
    "data" : "",
    "field" : "stableId",
    "label" : "STABLE_ID",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "stableId", "stableM2" ],
    "functionalPrimaryKey" : [ "stableId" ],
    "displayField" : "stableId",
    "filterOutputMapping" : [ {
      "lookupField" : "stableId",
      "tableField" : ""
    }, {
      "lookupField" : "stableM2",
      "tableField" : "stableM2"
    }, {
      "lookupField" : "",
      "tableField" : ""
    }, {
      "lookupField" : "",
      "tableField" : ""
    } ],
    "lookupUrl" : "stables/autosuggest",
    "fieldId" : "stableId",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "stableId",
    "type" : "formField",
    "fieldName" : "stableId"
  }, {
    "data" : "",
    "field" : "stableM2",
    "label" : "STABLE_M2",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "stableM2",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "stableM2",
    "type" : "formField",
    "fieldName" : "stableM2"
  }, {
    "data" : "",
    "field" : "density",
    "label" : "DENSITY",
    "formulaMethod" : "formulaDensity",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "density",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "density",
    "type" : "formField",
    "fieldName" : "density"
  }, {
    "data" : "",
    "field" : "dailyGrowth",
    "label" : "DAILY_GROWTH",
    "formulaMethod" : "formulaDailyGrowth",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "dailyGrowth",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "dailyGrowth",
    "type" : "formField",
    "fieldName" : "dailyGrowth"
  }, {
    "data" : "",
    "field" : "losses1stWeek",
    "label" : "LOSSES_1ST_WEEK",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "losses1stWeek",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "losses1stWeek",
    "type" : "formField",
    "fieldName" : "losses1stWeek"
  }, {
    "data" : "",
    "field" : "lossesTotal",
    "label" : "LOSSES_TOTAL",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "lossesTotal",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "lossesTotal",
    "type" : "formField",
    "fieldName" : "lossesTotal"
  }, {
    "data" : "",
    "field" : "selected1stWeek",
    "label" : "SELECTED_1ST_WEEK",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "selected1stWeek",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "selected1stWeek",
    "type" : "formField",
    "fieldName" : "selected1stWeek"
  }, {
    "data" : "",
    "field" : "selectedTotal",
    "label" : "SELECTED_TOTAL",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "selectedTotal",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "selectedTotal",
    "type" : "formField",
    "fieldName" : "selectedTotal"
  }, {
    "data" : "",
    "field" : "legSelectionTotal",
    "label" : "LEG_SELECTION_TOTAL",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "legSelectionTotal",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "legSelectionTotal",
    "type" : "formField",
    "fieldName" : "legSelectionTotal"
  }, {
    "data" : "",
    "field" : "slaughterDate",
    "label" : "SLAUGHTER_DATE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "slaughterDate",
    "timeOnly" : false,
    "fieldType" : "Date",
    "uiType" : "date",
    "name" : "slaughterDate",
    "type" : "formField",
    "fieldName" : "slaughterDate"
  }, {
    "data" : "",
    "field" : "slaughterBatchId",
    "label" : "SLAUGHTER_BATCH_ID",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "slaughterBatchId",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "slaughterBatchId",
    "type" : "formField",
    "fieldName" : "slaughterBatchId"
  }, {
    "data" : "",
    "field" : "farmer",
    "label" : "FARMER",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "farmerName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "farmer",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "farmer",
    "type" : "formField",
    "fieldName" : "farmer"
  }, {
    "data" : "",
    "field" : "farmerName",
    "label" : "FARMER_NAME",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "farmerName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "farmerName",
    "type" : "formField",
    "fieldName" : "farmerName"
  }, {
    "data" : "",
    "field" : "invoiceDate",
    "label" : "INVOICE_DATE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "invoiceDate",
    "timeOnly" : false,
    "fieldType" : "Date",
    "uiType" : "date",
    "name" : "invoiceDate",
    "type" : "formField",
    "fieldName" : "invoiceDate"
  }, {
    "data" : "",
    "field" : "fromCountry",
    "label" : "FROM_COUNTRY",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "countryName", "countryCode" ],
    "functionalPrimaryKey" : [ "countryCode" ],
    "displayField" : "countryCode",
    "mandatoryFilters" : [ "countryCode" ],
    "lookupUrl" : "countries/autosuggest",
    "fieldId" : "fromCountry",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "fromCountry",
    "type" : "formField",
    "fieldName" : "fromCountry"
  }, {
    "data" : "",
    "field" : "toCountry",
    "label" : "TO_COUNTRY",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "countryName", "countryCode" ],
    "functionalPrimaryKey" : [ "countryCode" ],
    "displayField" : "countryCode",
    "mandatoryFilters" : [ "countryCode" ],
    "lookupUrl" : "countries/autosuggest",
    "fieldId" : "toCountry",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "toCountry",
    "type" : "formField",
    "fieldName" : "toCountry"
  }, {
    "data" : "",
    "field" : "origin",
    "label" : "ORIGIN",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "countryName", "countryCode" ],
    "functionalPrimaryKey" : [ "countryCode" ],
    "displayField" : "countryCode",
    "mandatoryFilters" : [ "countryCode" ],
    "lookupUrl" : "countries/autosuggest",
    "fieldId" : "origin",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "origin",
    "type" : "formField",
    "fieldName" : "origin"
  }, {
    "field" : "weighingExternally",
    "data" : "Weighing externally",
    "mandatory" : "yes",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "weighingExternally",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "select",
    "name" : "weighingExternally",
    "type" : "formField",
    "label" : "WEIGHING_EXTERNALLY",
    "fieldName" : "weighingExternally"
  }, {
    "label" : "SELLERRELATIONTYPEDEFAULT",
    "field" : "sellerrelationtypedefault",
    "data" : "SellerRelationTypeDefault",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationTypeCode", "relationTypeName" ],
    "functionalPrimaryKey" : [ "relationTypeCode" ],
    "displayField" : "relationTypeCode",
    "lookupUrl" : "relationtypes/autosuggest",
    "fieldId" : "sellerrelationtypedefault",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "sellerrelationtypedefault",
    "type" : "formField",
    "fieldName" : "sellerrelationtypedefault"
  }, {
    "data" : "",
    "field" : "buyer",
    "label" : "BUYER",
    "mandatory" : "yes",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "buyerName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "buyer",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "buyer",
    "type" : "formField",
    "fieldName" : "buyer"
  }, {
    "data" : "",
    "field" : "buyerName",
    "label" : "BUYER_NAME",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "buyerName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "buyerName",
    "type" : "formField",
    "fieldName" : "buyerName"
  }, {
    "label" : "BENEFICIARYRELATIONTYPEDEFAULT",
    "field" : "beneficiaryrelationtypedefault",
    "data" : "BeneficiaryRelationTypeDefault",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationTypeCode", "relationTypeName" ],
    "functionalPrimaryKey" : [ "relationTypeCode" ],
    "displayField" : "relationTypeCode",
    "lookupUrl" : "relationtypes/autosuggest",
    "fieldId" : "beneficiaryrelationtypedefault",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "beneficiaryrelationtypedefault",
    "type" : "formField",
    "fieldName" : "beneficiaryrelationtypedefault"
  }, {
    "data" : "",
    "field" : "beneficiary",
    "label" : "BENEFICIARY",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "beneficiaryName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "filterInputMapping" : [ {
      "lookupField" : "relationType_value_relationTypeCode",
      "tableField" : "sellerrelationtypedefault.relationTypeCode"
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "beneficiary",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "beneficiary",
    "type" : "formField",
    "fieldName" : "beneficiary"
  }, {
    "data" : "",
    "field" : "beneficiaryName",
    "label" : "BENEFICIARY_NAME",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "beneficiaryName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "beneficiaryName",
    "type" : "formField",
    "fieldName" : "beneficiaryName"
  }, {
    "label" : "FEEDRELATIONTYPEDEFAULT",
    "field" : "feedrelationtypedefault",
    "data" : "FeedRelationTypeDefault",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationTypeCode", "relationTypeName" ],
    "functionalPrimaryKey" : [ "relationTypeCode" ],
    "displayField" : "relationTypeCode",
    "lookupUrl" : "relationtypes/autosuggest",
    "fieldId" : "feedrelationtypedefault",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "feedrelationtypedefault",
    "type" : "formField",
    "fieldName" : "feedrelationtypedefault"
  }, {
    "data" : "",
    "field" : "feedSupplier",
    "label" : "FEED_SUPPLIER",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "feedSupplierName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "feedSupplier",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "feedSupplier",
    "type" : "formField",
    "fieldName" : "feedSupplier"
  }, {
    "data" : "",
    "field" : "feedSupplierName",
    "label" : "FEED_SUPPLIER_NAME",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "feedSupplierName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "feedSupplierName",
    "type" : "formField",
    "fieldName" : "feedSupplierName"
  }, {
    "label" : "HATCHERYRALATIONTYPEDEFAULT",
    "field" : "hatcheryralationtypedefault",
    "data" : "HatcheryRalationTypeDefault",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "fieldId" : "hatcheryralationtypedefault",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "hatcheryralationtypedefault",
    "type" : "formField",
    "fieldName" : "hatcheryralationtypedefault"
  }, {
    "data" : "",
    "field" : "hatchery",
    "label" : "HATCHERY",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "hatcheryName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "hatchery",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "hatchery",
    "type" : "formField",
    "fieldName" : "hatchery"
  }, {
    "data" : "",
    "field" : "hatcheryName",
    "label" : "HATCHERY_NAME",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "fieldId" : "hatcheryName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "hatcheryName",
    "type" : "formField",
    "fieldName" : "hatcheryName"
  }, {
    "label" : "CATCHINGRELATIONTYPEDEFAULT",
    "field" : "catchingrelationtypedefault",
    "data" : "CatchingRelationTypeDefault",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationTypeCode", "relationTypeName" ],
    "functionalPrimaryKey" : [ "relationTypeCode" ],
    "displayField" : "relationTypeCode",
    "lookupUrl" : "relationtypes/autosuggest",
    "fieldId" : "catchingrelationtypedefault",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "catchingrelationtypedefault",
    "type" : "formField",
    "fieldName" : "catchingrelationtypedefault"
  }, {
    "data" : "",
    "field" : "catchingCrew",
    "label" : "CATCHING_CREW",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "catchingCrewName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "catchingCrew",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "catchingCrew",
    "type" : "formField",
    "fieldName" : "catchingCrew"
  }, {
    "data" : "",
    "field" : "catchingCrewName",
    "label" : "CATCHING_CREW_NAME",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "fieldId" : "catchingCrewName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "catchingCrewName",
    "type" : "formField",
    "fieldName" : "catchingCrewName"
  }, {
    "label" : "LABORATORYRELATIONTYPEDEFAULT",
    "field" : "laboratoryrelationtypedefault",
    "data" : "LaboratoryRelationTypeDefault",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationTypeCode", "relationTypeName" ],
    "functionalPrimaryKey" : [ "relationTypeCode" ],
    "displayField" : "relationTypeCode",
    "lookupUrl" : "relationtypes/autosuggest",
    "fieldId" : "laboratoryrelationtypedefault",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "laboratoryrelationtypedefault",
    "type" : "formField",
    "fieldName" : "laboratoryrelationtypedefault"
  }, {
    "data" : "",
    "field" : "laboratory",
    "label" : "LABORATORY",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "laboratoryName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "laboratory",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "laboratory",
    "type" : "formField",
    "fieldName" : "laboratory"
  }, {
    "data" : "",
    "field" : "laboratoryName",
    "label" : "LABORATORY_NAME",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "laboratoryName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "laboratoryName",
    "type" : "formField",
    "fieldName" : "laboratoryName"
  }, {
    "label" : "VETERINARIANRELATIONTYPEDEFAULT",
    "field" : "veterinarianrelationtypedefault",
    "data" : "VeterinarianRelationTypeDefault",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "fieldId" : "veterinarianrelationtypedefault",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "veterinarianrelationtypedefault",
    "type" : "formField",
    "fieldName" : "veterinarianrelationtypedefault"
  }, {
    "data" : "",
    "field" : "veterinarian",
    "label" : "VETERINARIAN",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : true,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "veterinarianName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "veterinarian",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "veterinarian",
    "type" : "formField",
    "fieldName" : "veterinarian"
  }, {
    "data" : "",
    "field" : "veterinarianName",
    "label" : "VETERINARIAN_NAME",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "veterinarianName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "veterinarianName",
    "type" : "formField",
    "fieldName" : "veterinarianName"
  }, {
    "label" : "WEIGHINGRELATIONTYPEDEFAULT",
    "field" : "weighingrelationtypedefault",
    "data" : "WeighingRelationTypeDefault",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationTypeCode", "relationTypeName" ],
    "functionalPrimaryKey" : [ "relationTypeCode" ],
    "displayField" : "relationTypeCode",
    "lookupUrl" : "relationtypes/autosuggest",
    "fieldId" : "weighingrelationtypedefault",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "weighingrelationtypedefault",
    "type" : "formField",
    "fieldName" : "weighingrelationtypedefault"
  }, {
    "data" : "",
    "field" : "weighingBridge",
    "label" : "WEIGHING_BRIDGE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "weighingBridgeName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "weighingBridge",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "weighingBridge",
    "type" : "formField",
    "fieldName" : "weighingBridge"
  }, {
    "data" : "",
    "field" : "weighingBridgeName",
    "label" : "WEIGHING_BRIDGE_NAME",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "weighingBridgeName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "weighingBridgeName",
    "type" : "formField",
    "fieldName" : "weighingBridgeName"
  }, {
    "data" : "",
    "field" : "grillerAmount",
    "label" : "GRILLER_AMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "grillerAmount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "grillerAmount",
    "type" : "formField",
    "fieldName" : "grillerAmount"
  }, {
    "data" : "",
    "field" : "grillerWeight",
    "label" : "GRILLER_WEIGHT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "grillerWeight",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "grillerWeight",
    "type" : "formField",
    "fieldName" : "grillerWeight"
  }, {
    "data" : "",
    "field" : "slaughterPercentage",
    "label" : "SLAUGHTER_PERCENTAGE",
    "formulaMethod" : "formulaSlaughterPercentage",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "slaughterPercentage",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "slaughterPercentage",
    "type" : "formField",
    "fieldName" : "slaughterPercentage"
  }, {
    "data" : "",
    "field" : "qtySlaughtered",
    "label" : "QTY_SLAUGHTERED",
    "formulaMethod" : "formulaQtySlaughtered",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "qtySlaughtered",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "qtySlaughtered",
    "type" : "formField",
    "fieldName" : "qtySlaughtered"
  }, {
    "data" : "",
    "field" : "netCount",
    "label" : "NET_COUNT",
    "formulaMethod" : "formulaNetCount",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "netCount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "netCount",
    "type" : "formField",
    "fieldName" : "netCount"
  }, {
    "data" : "",
    "field" : "averageWeight",
    "label" : "AVERAGE_WEIGHT",
    "formulaMethod" : "formulaAverageWeight",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "averageWeight",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "averageWeight",
    "type" : "formField",
    "fieldName" : "averageWeight"
  }, {
    "data" : "",
    "field" : "srmDeviation",
    "label" : "SRM_DEVIATION",
    "formulaMethod" : "formulaSrmDeviation",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "srmDeviation",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "srmDeviation",
    "type" : "formField",
    "fieldName" : "srmDeviation"
  }, {
    "data" : "",
    "field" : "netWeight",
    "label" : "NET_WEIGHT",
    "formulaMethod" : "formulaNetWeight",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "netWeight",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "netWeight",
    "type" : "formField",
    "fieldName" : "netWeight"
  }, {
    "data" : "",
    "field" : "allRejected",
    "label" : "ALL_REJECTED",
    "formulaMethod" : "formulaAllRejected",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "allRejected",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "allRejected",
    "type" : "formField",
    "fieldName" : "allRejected"
  }, {
    "data" : "",
    "field" : "rejectedSlaughterhouse",
    "label" : "REJECTED_SLAUGHTERHOUSE",
    "formulaMethod" : "formulaRejectedSlaughterhouse",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "rejectedSlaughterhouse",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "rejectedSlaughterhouse",
    "type" : "formField",
    "fieldName" : "rejectedSlaughterhouse"
  }, {
    "data" : "",
    "field" : "rejectedFarmer",
    "label" : "REJECTED_FARMER",
    "formulaMethod" : "formulaRejectedFarmer",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "rejectedFarmer",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "rejectedFarmer",
    "type" : "formField",
    "fieldName" : "rejectedFarmer"
  }, {
    "data" : "",
    "field" : "deadTotal",
    "label" : "DEAD_TOTAL",
    "formulaMethod" : "formulaDeadTotal",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "deadTotal",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "deadTotal",
    "type" : "formField",
    "fieldName" : "deadTotal"
  }, {
    "data" : "",
    "field" : "deadSlaughterhouse",
    "label" : "DEAD_SLAUGHTERHOUSE",
    "formulaMethod" : "formulaDeadSlaughterhouse",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "deadSlaughterhouse",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "deadSlaughterhouse",
    "type" : "formField",
    "fieldName" : "deadSlaughterhouse"
  }, {
    "data" : "",
    "field" : "deadFarmer",
    "label" : "DEAD_FARMER",
    "formulaMethod" : "formulaDeadFarmer",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "deadFarmer",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "deadFarmer",
    "type" : "formField",
    "fieldName" : "deadFarmer"
  }, {
    "data" : "",
    "field" : "paidWeight",
    "label" : "PAID_WEIGHT",
    "formulaMethod" : "formulaPaidWeight",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "paidWeight",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "paidWeight",
    "type" : "formField",
    "fieldName" : "paidWeight"
  }, {
    "data" : "",
    "field" : "occupationKgM2",
    "label" : "OCCUPATION_KG_M2",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "fieldId" : "occupationKgM2",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "occupationKgM2",
    "type" : "formField",
    "fieldName" : "occupationKgM2"
  }, {
    "data" : "",
    "field" : "deviation",
    "label" : "DEVIATION",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "fieldId" : "deviation",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "select",
    "name" : "deviation",
    "type" : "formField",
    "fieldName" : "deviation"
  }, {
    "data" : "",
    "field" : "netWeight2",
    "label" : "NET_WEIGHT2",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "multipleValues" : false,
    "fieldId" : "netWeight2",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "netWeight2",
    "type" : "formField",
    "fieldName" : "netWeight2"
  }, {
    "data" : "",
    "field" : "rejectDb",
    "label" : "REJECT_DB",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "no",
    "fieldId" : "rejectDb",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "rejectDb",
    "type" : "formField",
    "fieldName" : "rejectDb"
  }, {
    "data" : "",
    "field" : "voerkroppen",
    "label" : "VOERKROPPEN",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "voerkroppen",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "voerkroppen",
    "type" : "formField",
    "fieldName" : "voerkroppen"
  }, {
    "data" : "",
    "field" : "chestBruise",
    "label" : "CHEST_BRUISE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "chestBruise",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "chestBruise",
    "type" : "formField",
    "fieldName" : "chestBruise"
  }, {
    "data" : "",
    "field" : "legBruise",
    "label" : "LEG_BRUISE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "legBruise",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "legBruise",
    "type" : "formField",
    "fieldName" : "legBruise"
  }, {
    "data" : "",
    "field" : "wingBruise",
    "label" : "WING_BRUISE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "wingBruise",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "wingBruise",
    "type" : "formField",
    "fieldName" : "wingBruise"
  }, {
    "data" : "",
    "field" : "hakdermatitis",
    "label" : "HAKDERMATITIS",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "hakdermatitis",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "hakdermatitis",
    "type" : "formField",
    "fieldName" : "hakdermatitis"
  }, {
    "data" : "",
    "field" : "discolouredBreast",
    "label" : "DISCOLOURED_BREAST",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "discolouredBreast",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "discolouredBreast",
    "type" : "formField",
    "fieldName" : "discolouredBreast"
  }, {
    "data" : "",
    "field" : "hipInjuries",
    "label" : "HIP_INJURIES",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "hipInjuries",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "hipInjuries",
    "type" : "formField",
    "fieldName" : "hipInjuries"
  }, {
    "data" : "",
    "field" : "brokenWing",
    "label" : "BROKEN_WING",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "brokenWing",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "brokenWing",
    "type" : "formField",
    "fieldName" : "brokenWing"
  }, {
    "data" : "",
    "field" : "brokenLeg",
    "label" : "BROKEN_LEG",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "brokenLeg",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "brokenLeg",
    "type" : "formField",
    "fieldName" : "brokenLeg"
  }, {
    "data" : "",
    "field" : "uniformity",
    "label" : "UNIFORMITY",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "uniformity",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "uniformity",
    "type" : "formField",
    "fieldName" : "uniformity"
  }, {
    "data" : "",
    "field" : "noLesionVzl",
    "label" : "NO_LESION_VZL",
    "mandatory" : "conditional",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "conditionalMandatory" : {
      "sid" : "2697c267-c549-4b2b-b8c0-07fe8317aeb6",
      "query" : {
        "condition" : "and",
        "rules" : [ {
          "lhsTableName" : "slaughterreport",
          "value1" : "no",
          "label" : "thinningYesno",
          "type" : "value",
          "operator" : "is equal to"
        } ]
      }
    },
    "fieldId" : "noLesionVzl",
    "maxVal" : 100.0,
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "noLesionVzl",
    "type" : "formField",
    "fieldName" : "noLesionVzl"
  }, {
    "data" : "",
    "field" : "mildLesionVzl",
    "label" : "MILD_LESION_VZL",
    "mandatory" : "conditional",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "conditionalMandatory" : {
      "sid" : "2697c267-c549-4b2b-b8c0-07fe8317aeb6",
      "query" : {
        "condition" : "and",
        "rules" : [ {
          "lhsTableName" : "slaughterreport",
          "value1" : "no",
          "label" : "thinningYesno",
          "type" : "value",
          "operator" : "is equal to"
        } ]
      }
    },
    "fieldId" : "mildLesionVzl",
    "maxVal" : 100.0,
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "mildLesionVzl",
    "type" : "formField",
    "fieldName" : "mildLesionVzl"
  }, {
    "data" : "",
    "field" : "severeLesionVzl",
    "label" : "SEVERE_LESION_VZL",
    "mandatory" : "conditional",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "conditionalMandatory" : {
      "sid" : "2697c267-c549-4b2b-b8c0-07fe8317aeb6",
      "query" : {
        "condition" : "and",
        "rules" : [ {
          "lhsTableName" : "slaughterreport",
          "value1" : "no",
          "label" : "thinningYesno",
          "type" : "value",
          "operator" : "is equal to"
        } ]
      }
    },
    "fieldId" : "severeLesionVzl",
    "maxVal" : 100.0,
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "severeLesionVzl",
    "type" : "formField",
    "fieldName" : "severeLesionVzl"
  }, {
    "label" : "TOTAL_LESION_VZL",
    "field" : "totalLesionVzl",
    "data" : "Total lesion VZL",
    "formulaMethod" : "formulaTotalLesionVzl",
    "mandatory" : "conditional",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "conditionalMandatory" : {
      "sid" : "2697c267-c549-4b2b-b8c0-07fe8317aeb6",
      "query" : {
        "condition" : "and",
        "rules" : [ {
          "lhsTableName" : "slaughterreport",
          "value1" : "no",
          "label" : "thinningYesno",
          "type" : "value",
          "operator" : "is equal to"
        } ]
      }
    },
    "fieldId" : "totalLesionVzl",
    "maxVal" : 100.0,
    "infoBubble" : "Maximum value cannot be more than 100",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "totalLesionVzl",
    "type" : "formField",
    "fieldName" : "totalLesionVzl"
  }, {
    "label" : "TOTAL_LESION_SCORE_VZL",
    "field" : "totalLesionScoreVzl",
    "data" : "Total lesion score VZL",
    "formulaMethod" : "formulaTotalLesionScoreVzl",
    "mandatory" : "conditional",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "conditionalMandatory" : {
      "sid" : "2697c267-c549-4b2b-b8c0-07fe8317aeb6",
      "query" : {
        "condition" : "and",
        "rules" : [ {
          "lhsTableName" : "slaughterreport",
          "value1" : "no",
          "label" : "thinningYesno",
          "type" : "value",
          "operator" : "is equal to"
        } ]
      }
    },
    "fieldId" : "totalLesionScoreVzl",
    "maxVal" : 200.0,
    "infoBubble" : "Maximum value cannot be more than 200",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "totalLesionScoreVzl",
    "type" : "formField",
    "fieldName" : "totalLesionScoreVzl"
  }, {
    "data" : "",
    "field" : "quantitySupplied",
    "label" : "QUANTITY_SUPPLIED",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "quantitySupplied",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "quantitySupplied",
    "type" : "formField",
    "fieldName" : "quantitySupplied"
  }, {
    "data" : "",
    "field" : "quantityPlanned",
    "label" : "QUANTITY_PLANNED",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "quantityPlanned",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "quantityPlanned",
    "type" : "formField",
    "fieldName" : "quantityPlanned"
  }, {
    "data" : "",
    "field" : "deadOnArrival",
    "label" : "DEAD_ON_ARRIVAL",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "deadOnArrival",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "deadOnArrival",
    "type" : "formField",
    "fieldName" : "deadOnArrival"
  }, {
    "data" : "",
    "field" : "deadOnArrivalPercentage",
    "label" : "DEAD_ON_ARRIVAL_PERCENTAGE",
    "formulaMethod" : "formulaDeadOnArrivalPercentage",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "deadOnArrivalPercentage",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "deadOnArrivalPercentage",
    "type" : "formField",
    "fieldName" : "deadOnArrivalPercentage"
  }, {
    "data" : "",
    "field" : "grossWeight",
    "label" : "GROSS_WEIGHT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "grossWeight",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "grossWeight",
    "type" : "formField",
    "fieldName" : "grossWeight"
  }, {
    "data" : "",
    "field" : "weighingBridgeDiscount",
    "label" : "WEIGHING_BRIDGE_DISCOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "weighingBridgeDiscount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "weighingBridgeDiscount",
    "type" : "formField",
    "fieldName" : "weighingBridgeDiscount"
  }, {
    "data" : "",
    "field" : "feedDiscount",
    "label" : "FEED_DISCOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "feedDiscount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "feedDiscount",
    "type" : "formField",
    "fieldName" : "feedDiscount"
  }, {
    "data" : "",
    "field" : "srmNotification",
    "label" : "SRM_NOTIFICATION",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "srmNotification",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "srmNotification",
    "type" : "formField",
    "fieldName" : "srmNotification"
  }, {
    "data" : "",
    "field" : "maxdead",
    "label" : "MAXDEAD",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "maxdead",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "maxdead",
    "type" : "formField",
    "fieldName" : "maxdead"
  }, {
    "data" : "",
    "field" : "quantityRejectedWhole",
    "label" : "QUANTITY_REJECTED_WHOLE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "quantityRejectedWhole",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "quantityRejectedWhole",
    "type" : "formField",
    "fieldName" : "quantityRejectedWhole"
  }, {
    "data" : "",
    "field" : "quantityRejectedWholeKg",
    "label" : "QUANTITY_REJECTED_WHOLE_KG",
    "formulaMethod" : "formulaQuantityRejectedWholeKg",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "quantityRejectedWholeKg",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "quantityRejectedWholeKg",
    "type" : "formField",
    "fieldName" : "quantityRejectedWholeKg"
  }, {
    "data" : "",
    "field" : "quantityRejectedParts",
    "label" : "QUANTITY_REJECTED_PARTS",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "quantityRejectedParts",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "quantityRejectedParts",
    "type" : "formField",
    "fieldName" : "quantityRejectedParts"
  }, {
    "data" : "",
    "field" : "maxrejectslaughterhouse",
    "label" : "MAXREJECTSLAUGHTERHOUSE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "maxrejectslaughterhouse",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "maxrejectslaughterhouse",
    "type" : "formField",
    "fieldName" : "maxrejectslaughterhouse"
  }, {
    "data" : "",
    "field" : "ort",
    "label" : "ORT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "ort",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "ort",
    "type" : "formField",
    "fieldName" : "ort"
  }, {
    "data" : "",
    "field" : "organsReject",
    "label" : "ORGANS_REJECT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "organsReject",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "organsReject",
    "type" : "formField",
    "fieldName" : "organsReject"
  }, {
    "data" : "",
    "field" : "dayLoading",
    "label" : "DAY_LOADING",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "dayLoading",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "dayLoading",
    "type" : "formField",
    "fieldName" : "dayLoading"
  }, {
    "label" : "CERTIFICATE_ID",
    "field" : "certificateId",
    "data" : "Certificate ID",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "lookupFields" : [ "certificateName", "validFrom", "validForCountry", "validTo", "certificateId", "stable" ],
    "functionalPrimaryKey" : [ "certificateId" ],
    "displayField" : "certificateId",
    "filterOutputMapping" : [ {
      "lookupField" : "certificateName",
      "tableField" : "certificateName"
    }, {
      "lookupField" : "validFrom",
      "tableField" : ""
    }, {
      "lookupField" : "validForCountry",
      "tableField" : ""
    }, {
      "lookupField" : "validTo",
      "tableField" : ""
    }, {
      "lookupField" : "certificateId",
      "tableField" : ""
    }, {
      "lookupField" : "stable",
      "tableField" : ""
    } ],
    "lookupUrl" : "certificateslists/autosuggest",
    "fieldId" : "certificateId",
    "timeOnly" : false,
    "fieldType" : "any",
    "uiType" : "autosuggest",
    "name" : "certificateId",
    "type" : "formField",
    "fieldName" : "certificateId"
  }, {
    "label" : "CERTIFICATE_NAME",
    "field" : "certificateName",
    "data" : "Certificate Name",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "certificateName",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "certificateName",
    "type" : "formField",
    "fieldName" : "certificateName"
  }, {
    "data" : "",
    "field" : "pricebaseamount",
    "label" : "PRICEBASEAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricebaseamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricebaseamount",
    "type" : "formField",
    "fieldName" : "pricebaseamount"
  }, {
    "data" : "",
    "field" : "pricebasevalue",
    "label" : "PRICEBASEVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricebasevalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricebasevalue",
    "type" : "formField",
    "fieldName" : "pricebasevalue"
  }, {
    "label" : "PRICEBASESUM",
    "field" : "pricebasesum",
    "data" : "PriceBaseSum",
    "formulaMethod" : "formulaPricebasesum",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "pricebasesum",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricebasesum",
    "type" : "formField",
    "fieldName" : "pricebasesum"
  }, {
    "data" : "",
    "field" : "priceflexamount",
    "label" : "PRICEFLEXAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceflexamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceflexamount",
    "type" : "formField",
    "fieldName" : "priceflexamount"
  }, {
    "data" : "",
    "field" : "priceflexvalue",
    "label" : "PRICEFLEXVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceflexvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceflexvalue",
    "type" : "formField",
    "fieldName" : "priceflexvalue"
  }, {
    "data" : "",
    "field" : "pricedayloadamount",
    "label" : "PRICEDAYLOADAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricedayloadamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricedayloadamount",
    "type" : "formField",
    "fieldName" : "pricedayloadamount"
  }, {
    "data" : "",
    "field" : "pricedayloadvalue",
    "label" : "PRICEDAYLOADVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricedayloadvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricedayloadvalue",
    "type" : "formField",
    "fieldName" : "pricedayloadvalue"
  }, {
    "data" : "",
    "field" : "pricesrmamount",
    "label" : "PRICESRMAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricesrmamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricesrmamount",
    "type" : "formField",
    "fieldName" : "pricesrmamount"
  }, {
    "data" : "",
    "field" : "pricesrmvalue",
    "label" : "PRICESRMVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricesrmvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricesrmvalue",
    "type" : "formField",
    "fieldName" : "pricesrmvalue"
  }, {
    "label" : "PRICESRMSUM",
    "field" : "pricesrmsum",
    "data" : "PriceSRMSum",
    "formulaMethod" : "formulaPricesrmsum",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "pricesrmsum",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricesrmsum",
    "type" : "formField",
    "fieldName" : "pricesrmsum"
  }, {
    "data" : "",
    "field" : "pricebreedamount",
    "label" : "PRICEBREEDAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricebreedamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricebreedamount",
    "type" : "formField",
    "fieldName" : "pricebreedamount"
  }, {
    "data" : "",
    "field" : "pricebreedvalue",
    "label" : "PRICEBREEDVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricebreedvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricebreedvalue",
    "type" : "formField",
    "fieldName" : "pricebreedvalue"
  }, {
    "data" : "",
    "field" : "pricehakdermatitisamount",
    "label" : "PRICEHAKDERMATITISAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricehakdermatitisamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricehakdermatitisamount",
    "type" : "formField",
    "fieldName" : "pricehakdermatitisamount"
  }, {
    "data" : "",
    "field" : "pricehakdermatitisvalue",
    "label" : "PRICEHAKDERMATITISVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricehakdermatitisvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricehakdermatitisvalue",
    "type" : "formField",
    "fieldName" : "pricehakdermatitisvalue"
  }, {
    "data" : "",
    "field" : "priceoccupationamount",
    "label" : "PRICEOCCUPATIONAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceoccupationamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceoccupationamount",
    "type" : "formField",
    "fieldName" : "priceoccupationamount"
  }, {
    "data" : "",
    "field" : "priceoccupationvalue",
    "label" : "PRICEOCCUPATIONVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceoccupationvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceoccupationvalue",
    "type" : "formField",
    "fieldName" : "priceoccupationvalue"
  }, {
    "data" : "",
    "field" : "pricesoberamount",
    "label" : "PRICESOBERAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricesoberamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricesoberamount",
    "type" : "formField",
    "fieldName" : "pricesoberamount"
  }, {
    "data" : "",
    "field" : "pricesobervalue",
    "label" : "PRICESOBERVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricesobervalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricesobervalue",
    "type" : "formField",
    "fieldName" : "pricesobervalue"
  }, {
    "label" : "PRICESOBERSUM",
    "field" : "pricesobersum",
    "data" : "PriceSoberSum",
    "formulaMethod" : "formulaPricesobersum",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "pricesobersum",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricesobersum",
    "type" : "formField",
    "fieldName" : "pricesobersum"
  }, {
    "data" : "",
    "field" : "priceenrichmentamount",
    "label" : "PRICEENRICHMENTAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceenrichmentamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceenrichmentamount",
    "type" : "formField",
    "fieldName" : "priceenrichmentamount"
  }, {
    "data" : "",
    "field" : "priceenrichmentvalue",
    "label" : "PRICEENRICHMENTVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceenrichmentvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceenrichmentvalue",
    "type" : "formField",
    "fieldName" : "priceenrichmentvalue"
  }, {
    "label" : "PRICEENRICHMENTSUM",
    "field" : "priceenrichmentsum",
    "data" : "PriceEnrichmentSum",
    "formulaMethod" : "formulaPriceenrichmentsum",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "priceenrichmentsum",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceenrichmentsum",
    "type" : "formField",
    "fieldName" : "priceenrichmentsum"
  }, {
    "data" : "",
    "field" : "priceikbamount",
    "label" : "PRICEIKBAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceikbamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceikbamount",
    "type" : "formField",
    "fieldName" : "priceikbamount"
  }, {
    "data" : "",
    "field" : "priceikbvalue",
    "label" : "PRICEIKBVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceikbvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceikbvalue",
    "type" : "formField",
    "fieldName" : "priceikbvalue"
  }, {
    "data" : "",
    "field" : "pricebelplumeamount",
    "label" : "PRICEBELPLUMEAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricebelplumeamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricebelplumeamount",
    "type" : "formField",
    "fieldName" : "pricebelplumeamount"
  }, {
    "data" : "",
    "field" : "pricebelplumevalue",
    "label" : "PRICEBELPLUMEVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricebelplumevalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricebelplumevalue",
    "type" : "formField",
    "fieldName" : "pricebelplumevalue"
  }, {
    "data" : "",
    "field" : "priceplantBasedamount",
    "label" : "PRICEPLANT_BASEDAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceplantBasedamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceplantBasedamount",
    "type" : "formField",
    "fieldName" : "priceplantBasedamount"
  }, {
    "data" : "",
    "field" : "priceplantBasedvalue",
    "label" : "PRICEPLANT_BASEDVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceplantBasedvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceplantBasedvalue",
    "type" : "formField",
    "fieldName" : "priceplantBasedvalue"
  }, {
    "data" : "",
    "field" : "pricedelhaizeamount",
    "label" : "PRICEDELHAIZEAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricedelhaizeamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricedelhaizeamount",
    "type" : "formField",
    "fieldName" : "pricedelhaizeamount"
  }, {
    "data" : "",
    "field" : "pricedelhaizevalue",
    "label" : "PRICEDELHAIZEVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricedelhaizevalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricedelhaizevalue",
    "type" : "formField",
    "fieldName" : "pricedelhaizevalue"
  }, {
    "data" : "",
    "field" : "pricecolruytamount",
    "label" : "PRICECOLRUYTAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricecolruytamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricecolruytamount",
    "type" : "formField",
    "fieldName" : "pricecolruytamount"
  }, {
    "data" : "",
    "field" : "pricecolruytvalue",
    "label" : "PRICECOLRUYTVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricecolruytvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricecolruytvalue",
    "type" : "formField",
    "fieldName" : "pricecolruytvalue"
  }, {
    "data" : "",
    "field" : "pricedayGrowthamount",
    "label" : "PRICEDAY_GROWTHAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricedayGrowthamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricedayGrowthamount",
    "type" : "formField",
    "fieldName" : "pricedayGrowthamount"
  }, {
    "data" : "",
    "field" : "pricedayGrowthvalue",
    "label" : "PRICEDAY_GROWTHVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricedayGrowthvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricedayGrowthvalue",
    "type" : "formField",
    "fieldName" : "pricedayGrowthvalue"
  }, {
    "data" : "",
    "field" : "pricekmChargeamount",
    "label" : "PRICEKM_CHARGEAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricekmChargeamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricekmChargeamount",
    "type" : "formField",
    "fieldName" : "pricekmChargeamount"
  }, {
    "data" : "",
    "field" : "pricekmChargevalue",
    "label" : "PRICEKM_CHARGEVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricekmChargevalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricekmChargevalue",
    "type" : "formField",
    "fieldName" : "pricekmChargevalue"
  }, {
    "data" : "",
    "field" : "priceslidingamount",
    "label" : "PRICESLIDINGAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceslidingamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceslidingamount",
    "type" : "formField",
    "fieldName" : "priceslidingamount"
  }, {
    "data" : "",
    "field" : "priceslidingvalue",
    "label" : "PRICESLIDINGVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceslidingvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceslidingvalue",
    "type" : "formField",
    "fieldName" : "priceslidingvalue"
  }, {
    "data" : "",
    "field" : "pricecompensationoccupationamount",
    "label" : "PRICECOMPENSATIONOCCUPATIONAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricecompensationoccupationamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricecompensationoccupationamount",
    "type" : "formField",
    "fieldName" : "pricecompensationoccupationamount"
  }, {
    "data" : "",
    "field" : "pricecompensationoccupationvalue",
    "label" : "PRICECOMPENSATIONOCCUPATIONVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricecompensationoccupationvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricecompensationoccupationvalue",
    "type" : "formField",
    "fieldName" : "pricecompensationoccupationvalue"
  }, {
    "data" : "",
    "field" : "pricebweOtheramount",
    "label" : "PRICEBWE_OTHERAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricebweOtheramount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricebweOtheramount",
    "type" : "formField",
    "fieldName" : "pricebweOtheramount"
  }, {
    "data" : "",
    "field" : "pricebweOthervalue",
    "label" : "PRICEBWE_OTHERVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricebweOthervalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricebweOthervalue",
    "type" : "formField",
    "fieldName" : "pricebweOthervalue"
  }, {
    "data" : "",
    "field" : "priceotheramount",
    "label" : "PRICEOTHERAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceotheramount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceotheramount",
    "type" : "formField",
    "fieldName" : "priceotheramount"
  }, {
    "data" : "",
    "field" : "priceothervalue",
    "label" : "PRICEOTHERVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceothervalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceothervalue",
    "type" : "formField",
    "fieldName" : "priceothervalue"
  }, {
    "data" : "",
    "field" : "priceconceptpremiumamount",
    "label" : "PRICECONCEPTPREMIUMAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceconceptpremiumamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceconceptpremiumamount",
    "type" : "formField",
    "fieldName" : "priceconceptpremiumamount"
  }, {
    "data" : "",
    "field" : "priceconceptpremiumvalue",
    "label" : "PRICECONCEPTPREMIUMVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceconceptpremiumvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceconceptpremiumvalue",
    "type" : "formField",
    "fieldName" : "priceconceptpremiumvalue"
  }, {
    "label" : "PRICECONCEPTPREMIUMSUM",
    "field" : "priceconceptpremiumsum",
    "data" : "PriceConceptPremiumSum",
    "formulaMethod" : "formulaPriceconceptpremiumsum",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "priceconceptpremiumsum",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceconceptpremiumsum",
    "type" : "formField",
    "fieldName" : "priceconceptpremiumsum"
  }, {
    "data" : "",
    "field" : "priceabFreeamount",
    "label" : "PRICEAB_FREEAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceabFreeamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceabFreeamount",
    "type" : "formField",
    "fieldName" : "priceabFreeamount"
  }, {
    "data" : "",
    "field" : "priceabFreevalue",
    "label" : "PRICEAB_FREEVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "priceabFreevalue",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "priceabFreevalue",
    "type" : "formField",
    "fieldName" : "priceabFreevalue"
  }, {
    "data" : "",
    "field" : "pricegmoamount",
    "label" : "PRICEGMOAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricegmoamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricegmoamount",
    "type" : "formField",
    "fieldName" : "pricegmoamount"
  }, {
    "data" : "",
    "field" : "pricegmovalue",
    "label" : "PRICEGMOVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricegmovalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricegmovalue",
    "type" : "formField",
    "fieldName" : "pricegmovalue"
  }, {
    "data" : "",
    "field" : "pricenongmoamount",
    "label" : "PRICENONGMOAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricenongmoamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricenongmoamount",
    "type" : "formField",
    "fieldName" : "pricenongmoamount"
  }, {
    "data" : "",
    "field" : "pricenongmovalue",
    "label" : "PRICENONGMOVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "pricenongmovalue",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "pricenongmovalue",
    "type" : "formField",
    "fieldName" : "pricenongmovalue"
  }, {
    "data" : "",
    "field" : "pricevoerkroppenamount",
    "label" : "PRICEVOERKROPPENAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricevoerkroppenamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricevoerkroppenamount",
    "type" : "formField",
    "fieldName" : "pricevoerkroppenamount"
  }, {
    "data" : "",
    "field" : "pricevoerkroppenvalue",
    "label" : "PRICEVOERKROPPENVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "pricevoerkroppenvalue",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "pricevoerkroppenvalue",
    "type" : "formField",
    "fieldName" : "pricevoerkroppenvalue"
  }, {
    "data" : "",
    "field" : "pricediscolouredBreastamount",
    "label" : "PRICEDISCOLOURED_BREASTAMOUNT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricediscolouredBreastamount",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricediscolouredBreastamount",
    "type" : "formField",
    "fieldName" : "pricediscolouredBreastamount"
  }, {
    "data" : "",
    "field" : "pricediscolouredBreastvalue",
    "label" : "PRICEDISCOLOURED_BREASTVALUE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldId" : "pricediscolouredBreastvalue",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "pricediscolouredBreastvalue",
    "type" : "formField",
    "fieldName" : "pricediscolouredBreastvalue"
  }, {
    "data" : "",
    "field" : "pricenewBuildingPremium",
    "label" : "PRICENEW_BUILDING_PREMIUM",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricenewBuildingPremium",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricenewBuildingPremium",
    "type" : "formField",
    "fieldName" : "pricenewBuildingPremium"
  }, {
    "data" : "",
    "field" : "priceloadingCosts",
    "label" : "PRICELOADING_COSTS",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceloadingCosts",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceloadingCosts",
    "type" : "formField",
    "fieldName" : "priceloadingCosts"
  }, {
    "label" : "PRICELOADINGCOSTVALUE",
    "field" : "priceloadingcostvalue",
    "data" : "PriceLoadingCostValue",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceloadingcostvalue",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceloadingcostvalue",
    "type" : "formField",
    "fieldName" : "priceloadingcostvalue"
  }, {
    "label" : "PRICELOADINGCOSTSUM",
    "field" : "priceloadingcostsum",
    "data" : "PriceLoadingCostSum",
    "formulaMethod" : "formulaPriceloadingcostsum",
    "mandatory" : "no",
    "allowEditing" : "no",
    "allowViewing" : "yes",
    "fieldId" : "priceloadingcostsum",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceloadingcostsum",
    "type" : "formField",
    "fieldName" : "priceloadingcostsum"
  }, {
    "data" : "",
    "field" : "pricefairmastFee",
    "label" : "PRICEFAIRMAST_FEE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "pricefairmastFee",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "pricefairmastFee",
    "type" : "formField",
    "fieldName" : "pricefairmastFee"
  }, {
    "data" : "",
    "field" : "priceBeterVoorGuaranteeArrangement",
    "label" : "PRICE_BETER_VOOR_GUARANTEE_ARRANGEMENT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "priceBeterVoorGuaranteeArrangement",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "priceBeterVoorGuaranteeArrangement",
    "type" : "formField",
    "fieldName" : "priceBeterVoorGuaranteeArrangement"
  }, {
    "data" : "",
    "field" : "abFree",
    "label" : "AB_FREE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "abFree",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "abFree",
    "type" : "formField",
    "fieldName" : "abFree"
  }, {
    "data" : "",
    "field" : "kgExcluded",
    "label" : "KG_EXCLUDED",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "kgExcluded",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "kgExcluded",
    "type" : "formField",
    "fieldName" : "kgExcluded"
  }, {
    "data" : "",
    "field" : "enrichment",
    "label" : "ENRICHMENT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "enrichment",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "enrichment",
    "type" : "formField",
    "fieldName" : "enrichment"
  }, {
    "field" : "concept",
    "data" : "Concept",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "multipleValues" : false,
    "fieldType" : "string",
    "uiType" : "select",
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "allowedValues" : {
      "values" : [ {
        "label" : "BIO",
        "value" : "BIO"
      }, {
        "label" : "BLK1_AST",
        "value" : "BLK1_AST"
      }, {
        "label" : "GNK",
        "value" : "GNK"
      }, {
        "label" : "REGULAR",
        "value" : "REGULAR"
      } ],
      "conditions" : {
        "conditionType" : "None",
        "conditions" : [ ]
      }
    },
    "fieldId" : "concept",
    "timeOnly" : false,
    "name" : "concept",
    "type" : "formField",
    "label" : "CONCEPT",
    "fieldName" : "concept"
  }, {
    "data" : "",
    "field" : "gmo",
    "label" : "GMO",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "gmo",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "gmo",
    "type" : "formField",
    "fieldName" : "gmo"
  }, {
    "data" : "",
    "field" : "tierwohl",
    "label" : "TIERWOHL",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "tierwohl",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "tierwohl",
    "type" : "formField",
    "fieldName" : "tierwohl"
  }, {
    "data" : "",
    "field" : "gnk",
    "label" : "GNK",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "gnk",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "gnk",
    "type" : "formField",
    "fieldName" : "gnk"
  }, {
    "data" : "",
    "field" : "nonGmo",
    "label" : "NON_GMO",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "nonGmo",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "nonGmo",
    "type" : "formField",
    "fieldName" : "nonGmo"
  }, {
    "data" : "",
    "field" : "plantBased",
    "label" : "PLANT_BASED",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "plantBased",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "plantBased",
    "type" : "formField",
    "fieldName" : "plantBased"
  }, {
    "data" : "",
    "field" : "vegetarian",
    "label" : "VEGETARIAN",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "vegetarian",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "vegetarian",
    "type" : "formField",
    "fieldName" : "vegetarian"
  }, {
    "data" : "",
    "field" : "scharrel",
    "label" : "SCHARREL",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "scharrel",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "scharrel",
    "type" : "formField",
    "fieldName" : "scharrel"
  }, {
    "data" : "",
    "field" : "fairmast",
    "label" : "FAIRMAST",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "fairmast",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "fairmast",
    "type" : "formField",
    "fieldName" : "fairmast"
  }, {
    "data" : "",
    "field" : "colruyt",
    "label" : "COLRUYT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "colruyt",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "colruyt",
    "type" : "formField",
    "fieldName" : "colruyt"
  }, {
    "data" : "",
    "field" : "plukonwaardig",
    "label" : "PLUKONWAARDIG",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "plukonwaardig",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "plukonwaardig",
    "type" : "formField",
    "fieldName" : "plukonwaardig"
  }, {
    "data" : "",
    "field" : "delhaize",
    "label" : "DELHAIZE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "delhaize",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "delhaize",
    "type" : "formField",
    "fieldName" : "delhaize"
  }, {
    "data" : "",
    "field" : "vw",
    "label" : "VW",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "vw",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "vw",
    "type" : "formField",
    "fieldName" : "vw"
  }, {
    "data" : "",
    "field" : "vtp",
    "label" : "VTP",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "vtp",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "vtp",
    "type" : "formField",
    "fieldName" : "vtp"
  }, {
    "data" : "",
    "field" : "bbl",
    "label" : "BBL",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "bbl",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "bbl",
    "type" : "formField",
    "fieldName" : "bbl"
  }, {
    "data" : "",
    "field" : "geenConcept",
    "label" : "GEEN_CONCEPT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "geenConcept",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "geenConcept",
    "type" : "formField",
    "fieldName" : "geenConcept"
  }, {
    "data" : "",
    "field" : "stSe",
    "label" : "ST_SE",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "stSe",
    "timeOnly" : false,
    "fieldType" : "Boolean",
    "uiType" : "checkbox",
    "name" : "stSe",
    "type" : "formField",
    "fieldName" : "stSe"
  }, {
    "data" : "",
    "field" : "specialBooking",
    "label" : "SPECIAL_BOOKING",
    "multipleValuesMin" : 0,
    "multipleValuesMax" : 10,
    "fieldId" : "specialBooking",
    "timeOnly" : false,
    "fieldType" : "string",
    "uiType" : "text",
    "name" : "specialBooking",
    "type" : "formField",
    "fieldName" : "specialBooking"
  }, {
    "data" : "",
    "field" : "total",
    "label" : "TOTAL",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "total",
    "currencySymbol" : "EUR",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "currency",
    "name" : "total",
    "type" : "formField",
    "fieldName" : "total"
  }, {
    "data" : "",
    "field" : "vat",
    "label" : "VAT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "vat",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "number",
    "name" : "vat",
    "type" : "formField",
    "fieldName" : "vat"
  }, {
    "data" : "",
    "field" : "totalInclVat",
    "label" : "TOTAL_INCL_VAT",
    "mandatory" : "no",
    "allowEditing" : "yes",
    "allowViewing" : "yes",
    "fieldId" : "totalInclVat",
    "currencySymbol" : "EUR",
    "timeOnly" : false,
    "fieldType" : "number",
    "uiType" : "currency",
    "name" : "totalInclVat",
    "type" : "formField",
    "fieldName" : "totalInclVat"
  } ],
  "columns" : "1",
  "type" : "form"
}
	pageViewTitle: string = 'SLAUGHTER_REPORT_DETAIL';
	
		detailFormControls : FormGroup = new FormGroup({
	pricesrmsum: new FormControl(null,[]),
	gmo: new FormControl('',[]),
	gnk: new FormControl('',[]),
	feedDiscount: new FormControl(null,[]),
	averageWeight: new FormControl(null,[]),
	pricehakdermatitisamount: new FormControl(null,[]),
	pricediscolouredBreastvalue: new FormControl('',[]),
	netWeight: new FormControl(null,[]),
	priceoccupationamount: new FormControl(null,[]),
	priceothervalue: new FormControl(null,[]),
	enrichment: new FormControl('',[]),
	pricedayGrowthvalue: new FormControl(null,[]),
	hatcheryName: new FormControl('',[]),
	catchingCrewName: new FormControl('',[]),
	priceikbvalue: new FormControl(null,[]),
	pricesobervalue: new FormControl(null,[]),
	slaughterBatchId: new FormControl(null,[]),
	uniformity: new FormControl(null,[]),
	vw: new FormControl('',[]),
	priceenrichmentsum: new FormControl(null,[]),
	remarkSeller: new FormControl('',[]),
	deadOnArrival: new FormControl(null,[]),
	reportId: new FormControl(null,[]),
	invoiceDate: new FormControl('',[]),
	pricedayGrowthamount: new FormControl(null,[]),
	occupationKgM2: new FormControl(null,[]),
	pricesrmvalue: new FormControl(null,[]),
	catchingCrew: new FormControl('',[]),
	priceslidingamount: new FormControl(null,[]),
	rejectedFarmer: new FormControl(null,[]),
	pricefairmastFee: new FormControl(null,[]),
	losses1stWeek: new FormControl(null,[]),
	pricebasevalue: new FormControl(null,[]),
	thinningYesno: new FormControl('',[Validators.required]),
	age: new FormControl(null,[Validators.required]),
	priceoccupationvalue: new FormControl(null,[]),
	farmer: new FormControl('',[]),
	pricesobersum: new FormControl(null,[]),
	deadOnArrivalPercentage: new FormControl(null,[]),
	priceconceptpremiumvalue: new FormControl(null,[]),
	priceconceptpremiumamount: new FormControl(null,[]),
	pricekmChargeamount: new FormControl(null,[]),
	pricedelhaizeamount: new FormControl(null,[]),
	totalLesionVzl: new FormControl(null,[Validators.max(100.0)]),
	farmerName: new FormControl('',[]),
	chestBruise: new FormControl(null,[]),
	quantityPlanned: new FormControl(null,[]),
	pricedayloadamount: new FormControl(null,[]),
	density: new FormControl(null,[]),
	noLesionVzl: new FormControl(null,[Validators.max(100.0)]),
	pricebweOtheramount: new FormControl(null,[]),
	slaughterDate: new FormControl('',[]),
	wingBruise: new FormControl(null,[]),
	sequenceNr: new FormControl(null,[]),
	priceBeterVoorGuaranteeArrangement: new FormControl(null,[]),
	netCount: new FormControl(null,[]),
	priceflexvalue: new FormControl(null,[]),
	priceotheramount: new FormControl(null,[]),
	hatchery: new FormControl('',[]),
	pricebelplumeamount: new FormControl(null,[]),
	feedrelationtypedefault: new FormControl('',[]),
	grillerAmount: new FormControl(null,[]),
	selected1stWeek: new FormControl(null,[]),
	pricevoerkroppenvalue: new FormControl('',[]),
	pricebweOthervalue: new FormControl(null,[]),
	plantBased: new FormControl('',[]),
	priceflexamount: new FormControl(null,[]),
	stableM2: new FormControl(null,[]),
	lossesTotal: new FormControl(null,[]),
	beneficiaryName: new FormControl('',[]),
	deadTotal: new FormControl(null,[]),
	pricenongmovalue: new FormControl('',[]),
	allRejected: new FormControl(null,[]),
	remarkGrower: new FormControl('',[]),
	pricesrmamount: new FormControl(null,[]),
	laboratory: new FormControl('',[]),
	vtp: new FormControl('',[]),
	netWeight2: new FormControl('',[]),
	rejectDb: new FormControl(null,[]),
	pricecompensationoccupationamount: new FormControl(null,[]),
	mildLesionVzl: new FormControl(null,[Validators.max(100.0)]),
	stSe: new FormControl('',[]),
	catchingrelationtypedefault: new FormControl('',[]),
	pricebreedamount: new FormControl(null,[]),
	srmDeviation: new FormControl(null,[]),
	hatcheryralationtypedefault: new FormControl('',[]),
	weighingBridge: new FormControl('',[]),
	pricesoberamount: new FormControl(null,[]),
	total: new FormControl(null,[]),
	priceconceptpremiumsum: new FormControl(null,[]),
	bbl: new FormControl('',[]),
	installDate: new FormControl('',[]),
	breed: new FormControl('',[Validators.required]),
	totalLesionScoreVzl: new FormControl(null,[Validators.max(200.0)]),
	kgExcluded: new FormControl('',[]),
	roundNr: new FormControl(null,[]),
	certificateId: new FormControl('',[]),
	pricebreedvalue: new FormControl(null,[]),
	priceplantBasedamount: new FormControl(null,[]),
	beneficiary: new FormControl('',[]),
	priceloadingcostsum: new FormControl(null,[]),
	delhaize: new FormControl('',[]),
	quantityRejectedWhole: new FormControl(null,[]),
	discolouredBreast: new FormControl(null,[]),
	slaughterPercentage: new FormControl(null,[]),
	pricedelhaizevalue: new FormControl(null,[]),
	pricenewBuildingPremium: new FormControl(null,[]),
	pricecolruytamount: new FormControl(null,[]),
	plukonwaardig: new FormControl('',[]),
	priceloadingCosts: new FormControl(null,[]),
	pricebaseamount: new FormControl(null,[]),
	pricevoerkroppenamount: new FormControl(null,[]),
	grillerWeight: new FormControl(null,[]),
	flockNumber: new FormControl(null,[]),
	voerkroppen: new FormControl(null,[]),
	priceslidingvalue: new FormControl(null,[]),
	pricecompensationoccupationvalue: new FormControl(null,[]),
	pricedayloadvalue: new FormControl(null,[]),
	quantityRejectedParts: new FormControl(null,[]),
	srmNotification: new FormControl(null,[]),
	brokenLeg: new FormControl(null,[]),
	concept: new FormControl('',[]),
	sellerrelationtypedefault: new FormControl('',[]),
	veterinarian: new FormControl([],[]),
	pricekmChargevalue: new FormControl(null,[]),
	deadFarmer: new FormControl(null,[]),
	feedSupplierName: new FormControl('',[]),
	priceabFreevalue: new FormControl('',[]),
	vat: new FormControl(null,[]),
	dailyGrowth: new FormControl(null,[]),
	feedSupplier: new FormControl('',[]),
	priceloadingcostvalue: new FormControl(null,[]),
	pricegmoamount: new FormControl(null,[]),
	pricediscolouredBreastamount: new FormControl(null,[]),
	quantityRejectedWholeKg: new FormControl(null,[]),
	quantityInstalledInStable: new FormControl(null,[Validators.required, Validators.min(1000.0), Validators.max(150000.0)]),
	pricenongmoamount: new FormControl(null,[]),
	scharrel: new FormControl('',[]),
	dayLoading: new FormControl(null,[]),
	abFree: new FormControl('',[]),
	grossWeight: new FormControl(null,[]),
	remarkSlaughterhouse: new FormControl('',[]),
	buyerName: new FormControl('',[]),
	hipInjuries: new FormControl(null,[]),
	totalInclVat: new FormControl(null,[]),
	deviation: new FormControl('',[]),
	fairmast: new FormControl('',[]),
	tierwohl: new FormControl('',[]),
	certificateName: new FormControl('',[]),
	veterinarianrelationtypedefault: new FormControl('',[]),
	nonGmo: new FormControl('',[]),
	maxrejectslaughterhouse: new FormControl(null,[]),
	weighingBridgeDiscount: new FormControl(null,[]),
	vegetarian: new FormControl('',[]),
	selectedTotal: new FormControl(null,[]),
	hakdermatitis: new FormControl(null,[]),
	qtySlaughtered: new FormControl(null,[]),
	priceabFreeamount: new FormControl(null,[]),
	priceenrichmentamount: new FormControl(null,[]),
	pricebelplumevalue: new FormControl(null,[]),
	quantitySupplied: new FormControl(null,[]),
	stableId: new FormControl('',[]),
	pricecolruytvalue: new FormControl(null,[]),
	priceikbamount: new FormControl(null,[]),
	weighingBridgeName: new FormControl('',[]),
	rejectedSlaughterhouse: new FormControl(null,[]),
	pricebasesum: new FormControl(null,[]),
	maxdead: new FormControl(null,[]),
	priceplantBasedvalue: new FormControl(null,[]),
	colruyt: new FormControl('',[]),
	legBruise: new FormControl(null,[]),
	paidWeight: new FormControl(null,[]),
	pricegmovalue: new FormControl(null,[]),
	deadSlaughterhouse: new FormControl(null,[]),
	pricehakdermatitisvalue: new FormControl(null,[]),
	origin: new FormControl('',[]),
	beneficiaryrelationtypedefault: new FormControl('',[]),
	ort: new FormControl(null,[]),
	severeLesionVzl: new FormControl(null,[Validators.max(100.0)]),
	geenConcept: new FormControl('',[]),
	toCountry: new FormControl('',[]),
	fromCountry: new FormControl('',[]),
	specialBooking: new FormControl('',[]),
	veterinarianName: new FormControl('',[]),
	laboratoryName: new FormControl('',[]),
	legSelectionTotal: new FormControl(null,[]),
	laboratoryrelationtypedefault: new FormControl('',[]),
	weighingExternally: new FormControl('',[Validators.required]),
	puchaseNote: new FormControl('',[]),
	priceenrichmentvalue: new FormControl(null,[]),
	weighingrelationtypedefault: new FormControl('',[]),
	status: new FormControl('',[]),
	organsReject: new FormControl(null,[]),
	buyer: new FormControl('',[Validators.required]),
	brokenWing: new FormControl(null,[]),
});


	constructor(public slaughterReportService: SlaughterReportService, public appUtilBaseService: AppUtilBaseService, public translateService: TranslateService, public messageService: MessageService, public confirmationService: ConfirmationService, public dialogService: DialogService, public domSanitizer: DomSanitizer, public bsModalService: BsModalService, public activatedRoute: ActivatedRoute, public breadcrumbService: BreadcrumbService, public appBaseService: AppBaseService, public router: Router, public appGlobalService: AppGlobalService, public uploaderService: UploaderService, public baseService: BaseService, public location: Location, ...args: any) {
    
 	 }

	
	formulaDensity(data:any){
	const result = data.netWeight / data.stableM2; // formula
	this.detailFormControls?.get('density')?.patchValue(result); // patch the value to form
}
	autoSuggestSearchdetailFormbeneficiary(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormbeneficiary){
      this.isAutoSuggestCallFireddetailFormbeneficiary = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormbeneficiary = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormbeneficiary, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormbeneficiary = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormbeneficiary = this.formatFilteredData(this.filteredItemsdetailFormbeneficiary, 'beneficiary');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormbeneficiary = false;
    })
this.subscriptions.push(sub);}
 }
	autoSuggestSearchdetailFormbuyer(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormbuyer){
      this.isAutoSuggestCallFireddetailFormbuyer = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormbuyer = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormbuyer, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormbuyer = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormbuyer = this.formatFilteredData(this.filteredItemsdetailFormbuyer, 'buyer');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormbuyer = false;
    })
this.subscriptions.push(sub);}
 }
	attachInfiniteScrollForAutoCompletedetailFormsellerrelationtypedefault(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormsellerrelationtypedefault = false;
          if(this.filteredItemsdetailFormsellerrelationtypedefault.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormsellerrelationtypedefault`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formulaSlaughterPercentage(data:any){
	const result = data.grillerWeight / data.grillerAmount * 100; // formula
	this.detailFormControls?.get('slaughterPercentage')?.patchValue(result); // patch the value to form
}
	getTabIndex(tabName:string){
    return 0;
  }
	attachInfiniteScrollForAutoCompletedetailFormtoCountry(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormtoCountry = false;
          if(this.filteredItemsdetailFormtoCountry.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormtoCountry`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formatFormDataBeforeSave() {
    let data = this.detailFormControls.getRawValue();
    for (const ele in this.formFieldConfig) {
      const ec = this.formFieldConfig[ele];
      if (ec.columns && !ec.multipleValues) {
        data[ec.name] = this.configureRequestData(data[ec.name], ec.columns, ec.name)
      }
      else if (ec.columns && ec.multipleValues && data[ec.name]) {
        data[ec.name]?.map((o: any,index:number) => {
          const tempData = this.configureRequestData(data[ec.name][index], ec.columns, ec.name,index);
          data[ec.name][index] = tempData;
        })
      }
    }
    data = this.configureRequestData(data,this.detailFormConfig.children);
    return data;
  }
	formatRawData() {
    this.denormalize(this.data);
        // since this.data cannot be directly used for patch value, doing a deep copy
        // Deep copy converts date into string, so copy is performed before formatting
    let patchData = JSON.parse(JSON.stringify(this.data));
    this.selectedItems = [];
    for (const e in this.formFieldConfig) {
      const ec = this.formFieldConfig[e];
      if (ec.columns && !ec.multipleValues && this.data[ec.name]) {
        for (const e1 in this.data[ec.name]) {
           const ec1 = ec[e1];
          if(this.data[ec.name]){
          this.data[ec.name][ec1.name] = this.bindData(this.data[ec.name][ec1.name], ec1,ec.name);
          patchData[ec.name][ec1.name] = this.bindData(patchData[ec.name][ec1.name], ec1,ec.name);
          }
        }
      }
      else if (ec.columns && ec.multipleValues && this.data[ec.name]) {
        this.data[ec.name]?.map((o: any, index: number) => {
          for (const e1 in o) {
            const ec1 = ec[e1];
            if (this.data[ec.name]) {
              this.data[ec.name][index][ec1.name] = this.bindData(this.data[ec.name][index][ec1.name], ec1,ec.name,index);
              patchData[ec.name][index][ec1.name] = this.bindData(patchData[ec.name][index][ec1.name], ec1,ec.name,index);
            }
          }
        })
      }
      else {
       if(!ec.columns && this.data[ec.name]){
          this.data[ec.name] = this.bindData(this.data[ec.name], ec);
                  patchData[ec.name] = this.bindData(patchData[ec.name], ec);
        }        
      }
    }
   
   
// const patchData = JSON.parse(JSON.stringify(this.data));
this.detailFormControls.patchValue(patchData,{ emitEvent: true});
this.backupData = this.appUtilBaseService.deepClone(this.detailFormControls.getRawValue());

  }
	unSelect(event:any,field:string,multiple:boolean,rowIndex?:number){
    field = field.replace('?.','_');
if(rowIndex!= undefined && rowIndex>=0)
    field = `${field}_${rowIndex}`;
 if(multiple){
    this.selectedItems[field]?.forEach((item:any,index:number)=>{
        if(item.id === event.sid){
            this.selectedItems[field].splice(index,1);
        }
    })
}else{
 this.selectedItems[field] =[];
}
  }
	configureFormOnWorkflow() {
    const actions: any = this.workflowActionBarConfig?.children;
    const workflowInformation = {
      actors: this.data?.workflowInfo && this.data?.workflowInfo[this.workflowType]?.userTypes || [],
      step: this.data?.workflowInfo && this.data?.workflowInfo[this.workflowType]?.step || ''
    }
    this.formSecurityConfig = this.appUtilBaseService.getFormSecurityConfigFromSecurityJSON(this.securityJson, this.detailFormControls, actions, workflowInformation);
    console.log(this.formSecurityConfig)
    this.hiddenFields = this.formSecurityConfig.hidefields;

    for (const control in this.detailFormControls.getRawValue()) {
      if (this.formSecurityConfig.disableonlyfields.indexOf(control) > -1)
      // && !(this.formSecurityConfig.enableonlyfields.indexOf(control) > -1)) 
      {
        this.detailFormControls.controls[control].disable({ emitEvent: false });
      }
      if (this.formSecurityConfig.enableonlyfields.indexOf(control) > -1) {
        this.detailFormControls.controls[control].enable({ emitEvent: false });
      }
      if (this.formSecurityConfig.hidefields.indexOf(control) > -1 )
      // && !(this.formSecurityConfig.showfields.indexOf(control) > -1)) 
      {
        this.hiddenFields[control] = true;
      }
      if (this.formSecurityConfig.showfields.indexOf(control) > -1) {
        this.hiddenFields[control] = false;
      }
    }
    this.workflowActions = {
      disableActions: [...this.formSecurityConfig.disableonlyactions],
      enableActions: [...this.formSecurityConfig.enableonlyactions],
      hideActions: [...this.formSecurityConfig.hideactions],
      showActions: [...this.formSecurityConfig.showactions]
    }
  }
	attachInfiniteScrollForAutoCompletedetailFormbuyer(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormbuyer = false;
          if(this.filteredItemsdetailFormbuyer.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormbuyer`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formatAutoComplete(item:any,displayField:string,formControlName:string){
     return ((item && item[displayField]) ? item[displayField] : '');
  }
	formulaAllRejected(data:any){
	const result = data.quantityRejectedWholeKg + data.quantityRejectedParts; // formula
	this.detailFormControls?.get('allRejected')?.patchValue(result); // patch the value to form
}
	onwfBackToSupplyAndFlockData(){
	const params = {id:this.id,
      comments:this.comments}
	this.slaughterReportService.slaughterReportsWorkflowBackToSupplyAndFlockData(params).subscribe((res:any)=>{
		this.showMessage({ severity: 'success', summary: '', detail: 'Record Updated Successfully' });
		if(Object.keys(this.mandatoryFields).length > 0){
			this.clearValidations(this.mandatoryFields);
		}
		this.onInit();
	},error=>{
		if(Object.keys(this.mandatoryFields).length > 0)
			this.clearValidations(this.mandatoryFields);
	})
}
	onSave(isToastNotNeeded ?: boolean) {
  if (this.appUtilBaseService.isEqualIgnoreCase(this.detailFormControls.getRawValue(), this.backupData, this.appUtilBaseService.getIgnorableFields(this.detailFormControls.getRawValue(), this.backupData, this.detailPageIgnoreFields), true)) {
    this.showMessage({ severity: 'info', summary: '', detail: this.translateService.instant('NO_CHANGES_AVAILABLE_TO_SAVE') });
    return;
  }
  if (this.checkValidations()) {
    const complexData = this.checkUnsavedComplexFields();
    if (complexData.hasUnSavedChanges) {
    
      complexData.fields.map((o: any) => $($('.'+o+'_editSave-btn')).trigger('click'));
    }
    let preFormattedData = this.formatFormDataBeforeSave();
    let data = this.normalize(preFormattedData)
    const method = this.id ? 'update' : 'create';
    data = { ...this.backupData, ...data };
    if (this.pid) {
      data.pid = this.pid;
    }
    if (this.id) {
      data.sid = this.id
    }

  const requestedObj = this.generateRequestObj(data);
      this.messageService.clear();
      const attachmentTypes = ['imageCarousel', 'attachments'];
      const attachmentFields = this.detailFormConfig?.children
        .filter((ele: any) => attachmentTypes.includes(ele.uiType))
        .map((item: any) => item.fieldName);
      const splittedData = this.appUtilBaseService.splitFileAndData(data, attachmentFields);

      if (Object.keys(splittedData.files).length > 0) {
      
      const saveSubscription = this.uploadAttachmentsandSaveData(requestedObj, splittedData).subscribe(res => {
          this.onAfterSave(res, data, method, isToastNotNeeded);
        }, err => { this.isSaveResponseReceived = true })
        this.subscriptions.push(saveSubscription);
      }
      else {
        const saveSubscription = this.slaughterReportService[method](requestedObj).subscribe((res:SlaughterReportBase) => {
          this.onAfterSave(res, data, method, isToastNotNeeded);
        })
        this.subscriptions.push(saveSubscription);
      }
    }

  }
     
       onAfterSave(res: any, data: any, method: string, isToastNotNeeded?: boolean) {
    this.data = { ...data, ...res };
    this.formatRawData();
    this.isSaveResponseReceived = true;
    this.isFormValueChanged = false;
    this.id = res.sid;
    if (method === 'create') {
      this.router.navigate(
        [],
        {
          queryParams: { id: this.id },
          relativeTo: this.activatedRoute,
          queryParamsHandling: 'merge',
          replaceUrl: true
        }).then(() => { this.onInit() });
      this.getId();
    }
    if (!isToastNotNeeded) {
      this.showMessage({ severity: 'success', summary: '', detail: this.translateService.instant('RECORD_SAVED_SUCCESSFULLY') });
    }
  }

uploadAttachmentsandSaveData(data: any, splittedData: any): Observable<any> {
    const subject$ = new Subject();

    const completeReq = (resData: any,) => {
      resData ? subject$.next(resData) : subject$.error(resData);
      subject$.complete();
    }
    if (!this.id) {
       const saveSubscription =  this.slaughterReportService.create(splittedData.data).subscribe(
        createdData => {
          const data = { ...splittedData.data, ...createdData };
          splittedData.data = data;
          this.id = data.sid;
          if (splittedData.files) {
            this.updateData(splittedData).subscribe(
              updatedData => completeReq(updatedData),
              err => completeReq(null)
            );
          }
        },
        err => completeReq(null))
        this.subscriptions.push(saveSubscription);
    } else {
      const saveSubscription =  this.updateData(splittedData).subscribe(
        updatedOrg => completeReq(updatedOrg),
        err => completeReq(null)
      );
      this.subscriptions.push(saveSubscription);
    }

    return subject$.asObservable()
  }

updateData(splittedData:any){
const subject$ = new Subject();
const updateSubscription =  this.saveFiles(splittedData).subscribe((dataToUpdate:any)=>{
  dataToUpdate.data.sid = this.id;
  this.slaughterReportService.update(dataToUpdate.data).subscribe(
   res => {
     subject$.next(res);
     subject$.complete();
   },
   err =>{
     subject$.error(null);
     subject$.complete();
   }
 )
});
this.subscriptions.push(updateSubscription);
return subject$.asObservable()
}

saveFiles(splittedData: any) {
    if (splittedData.files && Object.values(splittedData.files).filter(item => item).length) {
      return new Observable(observer => {
        this.uploaderService.saveAddedFiles(splittedData, this.id, this.detailFormControls).subscribe((res: any) => {
          let fData: any = {};
          for (const key in res.dataToResend) {
            if (res.dataToResend[key] instanceof Array) {
              const tempArr = res.dataToResend[key].flat();
              fData[key] = (tempArr.filter((n: any, i: any) => tempArr.indexOf(n) === i)).filter(Boolean);
            } else {
              fData[key] = [res.dataToResend[key]];
            }
            fData[key]= this.appUtilBaseService.removeImagePreviewProperties(fData[key]);
          }
          
          const finalData = { data: { ...splittedData.data, ...fData } };
          if (res.error) {
            const errorArr: any = [];
            Object.keys(res.error).forEach((key) => {
              errorArr.push("Failed to upload " + key);
            })
            if(errorArr.length > 0)
            this.showMessage({ severity: 'error', summary: 'Error', detail: this.appUtilBaseService.createNotificationList(errorArr) });
          }
          observer.next(finalData);
          observer.complete();
        }, (err: any) => {
          observer.error(err);
        });
      });
    } else {
      return of(splittedData)
    }
  }
	attachInfiniteScrollForAutoCompletedetailFormlaboratory(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormlaboratory = false;
          if(this.filteredItemsdetailFormlaboratory.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormlaboratory`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	attachInfiniteScrollForAutoCompletedetailFormcatchingrelationtypedefault(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormcatchingrelationtypedefault = false;
          if(this.filteredItemsdetailFormcatchingrelationtypedefault.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormcatchingrelationtypedefault`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formulaPricesrmsum(data:any){
	const result = data.pricesrmamount * data.pricesrmvalue; // formula
	this.detailFormControls?.get('pricesrmsum')?.patchValue(result); // patch the value to form
}
	formatFilteredData(data:any,fieldName:string){
    return data;
  }
	attachInfiniteScrollForAutoCompletedetailFormorigin(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormorigin = false;
          if(this.filteredItemsdetailFormorigin.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormorigin`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	loadActionbar(){
    
}
	autoSuggestSearchdetailFormlaboratory(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormlaboratory){
      this.isAutoSuggestCallFireddetailFormlaboratory = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormlaboratory = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormlaboratory, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormlaboratory = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormlaboratory = this.formatFilteredData(this.filteredItemsdetailFormlaboratory, 'laboratory');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormlaboratory = false;
    })
this.subscriptions.push(sub);}
 }
	autoSuggestSearchdetailFormbeneficiaryrelationtypedefault(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormbeneficiaryrelationtypedefault){
      this.isAutoSuggestCallFireddetailFormbeneficiaryrelationtypedefault = true;
    let apiObj = Object.assign({}, RelationTypeApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormbeneficiaryrelationtypedefault = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormbeneficiaryrelationtypedefault, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormbeneficiaryrelationtypedefault = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormbeneficiaryrelationtypedefault = this.formatFilteredData(this.filteredItemsdetailFormbeneficiaryrelationtypedefault, 'beneficiaryrelationtypedefault');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormbeneficiaryrelationtypedefault = false;
    })
this.subscriptions.push(sub);}
 }
	formulaPaidWeight(data:any){
	const result = data.netWeight - data.rejectedFarmer - data.deadFarmer; // formula
	this.detailFormControls?.get('paidWeight')?.patchValue(result); // patch the value to form
}
	attachInfiniteScrollForAutoCompletedetailFormcertificateId(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormcertificateId = false;
          if(this.filteredItemsdetailFormcertificateId.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormcertificateId`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formulaPricebasesum(data:any){
	const result = data.pricebaseamount * data.pricebasevalue; // formula
	this.detailFormControls?.get('pricebasesum')?.patchValue(result); // patch the value to form
}
	formulaQuantityRejectedWholeKg(data:any){
	const result = data.quantityRejectedWhole * data.averageWeight / 1000; // formula
	this.detailFormControls?.get('quantityRejectedWholeKg')?.patchValue(result); // patch the value to form
}
	attachInfiniteScrollForAutoCompletedetailFormlaboratoryrelationtypedefault(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormlaboratoryrelationtypedefault = false;
          if(this.filteredItemsdetailFormlaboratoryrelationtypedefault.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormlaboratoryrelationtypedefault`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formulaPriceenrichmentsum(data:any){
	const result = data.priceenrichmentamount * data.priceenrichmentvalue; // formula
	this.detailFormControls?.get('priceenrichmentsum')?.patchValue(result); // patch the value to form
}
	bindLookupFields(): void {
    const lookupArray: any = []
    for (const ele in this.formFieldConfig) {
      if (this.formFieldConfig[ele].uiType === 'autosuggest' && this.formFieldConfig[ele].filterOutputMapping) {
        this.detailFormControls.get(this.formFieldConfig[ele].fieldName)?.valueChanges.subscribe((obj) => {
          this.formFieldConfig[ele].filterOutputMapping.map((filter: any) => {
            if (this.formFieldConfig[ele].multipleValues && obj) {
              obj?.map((o: any) => {
                const filterValue = o.value? o.value[filter.lookupField]:o[filter.lookupField];
                lookupArray.push(filterValue);
              })
              this.detailFormControls?.get(filter.tableField)?.patchValue(lookupArray.toString());
            }
           else{
              if(obj){
                const filterValue = obj.value? obj.value[filter.lookupField]:obj[filter.lookupField];
                this.detailFormControls?.get(filter.tableField)?.patchValue(filterValue);
              }
              else{
                this.detailFormControls?.get(filter.tableField)?.reset();
              }
            }
          })
        })
      }
    }
  }
	formulaDeadFarmer(data:any){
	const result = data.deadTotal - data.deadSlaughterhouse; // formula
	this.detailFormControls?.get('deadFarmer')?.patchValue(result); // patch the value to form
}
	waitForResponse() {
    if(environment.prototype){
      this.getWorkflowConfig();
    }
    else{
      setTimeout(() => {
        if (this.id && !environment.prototype) {
          if (!this.data?.workflowInfo) {
            this.waitForResponse();
          }
          else {
            this.getWorkflowConfig();
          }
        }
      }, 3000);
    }
  
  }
	calculateFormula(){
		this.detailFormControls?.get('quantityRejectedWholeKg')?.valueChanges.subscribe(()=>{
		this.formulaAllRejected(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('mildLesionVzl')?.valueChanges.subscribe(()=>{
		this.formulaTotalLesionVzl(this.detailFormControls.getRawValue());
		this.formulaTotalLesionScoreVzl(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('deadTotal')?.valueChanges.subscribe(()=>{
		this.formulaDeadFarmer(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('pricebasevalue')?.valueChanges.subscribe(()=>{
		this.formulaPricebasesum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('quantitySupplied')?.valueChanges.subscribe(()=>{
		this.formulaQtySlaughtered(this.detailFormControls.getRawValue());
		this.formulaNetCount(this.detailFormControls.getRawValue());
		this.formulaAverageWeight(this.detailFormControls.getRawValue());
		this.formulaDeadOnArrivalPercentage(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('weighingBridgeDiscount')?.valueChanges.subscribe(()=>{
		this.formulaNetWeight(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('rejectedSlaughterhouse')?.valueChanges.subscribe(()=>{
		this.formulaRejectedFarmer(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('priceloadingcostvalue')?.valueChanges.subscribe(()=>{
		this.formulaPriceloadingcostsum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('priceenrichmentamount')?.valueChanges.subscribe(()=>{
		this.formulaPriceenrichmentsum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('priceconceptpremiumamount')?.valueChanges.subscribe(()=>{
		this.formulaPriceconceptpremiumsum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('stableM2')?.valueChanges.subscribe(()=>{
		this.formulaDensity(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('deadFarmer')?.valueChanges.subscribe(()=>{
		this.formulaPaidWeight(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('srmNotification')?.valueChanges.subscribe(()=>{
		this.formulaSrmDeviation(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('pricebaseamount')?.valueChanges.subscribe(()=>{
		this.formulaPricebasesum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('maxdead')?.valueChanges.subscribe(()=>{
		this.formulaDeadSlaughterhouse(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('maxrejectslaughterhouse')?.valueChanges.subscribe(()=>{
		this.formulaRejectedSlaughterhouse(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('feedDiscount')?.valueChanges.subscribe(()=>{
		this.formulaNetWeight(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('priceenrichmentvalue')?.valueChanges.subscribe(()=>{
		this.formulaPriceenrichmentsum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('priceloadingCosts')?.valueChanges.subscribe(()=>{
		this.formulaPriceloadingcostsum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('deadOnArrival')?.valueChanges.subscribe(()=>{
		this.formulaQtySlaughtered(this.detailFormControls.getRawValue());
		this.formulaNetCount(this.detailFormControls.getRawValue());
		this.formulaDeadTotal(this.detailFormControls.getRawValue());
		this.formulaDeadOnArrivalPercentage(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('allRejected')?.valueChanges.subscribe(()=>{
		this.formulaRejectedFarmer(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('severeLesionVzl')?.valueChanges.subscribe(()=>{
		this.formulaTotalLesionVzl(this.detailFormControls.getRawValue());
		this.formulaTotalLesionScoreVzl(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('priceconceptpremiumvalue')?.valueChanges.subscribe(()=>{
		this.formulaPriceconceptpremiumsum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('grillerWeight')?.valueChanges.subscribe(()=>{
		this.formulaSlaughterPercentage(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('rejectedFarmer')?.valueChanges.subscribe(()=>{
		this.formulaPaidWeight(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('pricesrmvalue')?.valueChanges.subscribe(()=>{
		this.formulaPricesrmsum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('netWeight')?.valueChanges.subscribe(()=>{
		this.formulaDensity(this.detailFormControls.getRawValue());
		this.formulaAverageWeight(this.detailFormControls.getRawValue());
		this.formulaRejectedSlaughterhouse(this.detailFormControls.getRawValue());
		this.formulaDeadSlaughterhouse(this.detailFormControls.getRawValue());
		this.formulaPaidWeight(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('grossWeight')?.valueChanges.subscribe(()=>{
		this.formulaNetWeight(this.detailFormControls.getRawValue());
		this.formulaNetWeight(this.detailFormControls.getRawValue());
		this.formulaNetWeight(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('pricesrmamount')?.valueChanges.subscribe(()=>{
		this.formulaPricesrmsum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('pricesobervalue')?.valueChanges.subscribe(()=>{
		this.formulaPricesobersum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('grillerAmount')?.valueChanges.subscribe(()=>{
		this.formulaSlaughterPercentage(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('noLesionVzl')?.valueChanges.subscribe(()=>{
		this.formulaTotalLesionVzl(this.detailFormControls.getRawValue());
		this.formulaTotalLesionScoreVzl(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('quantityRejectedParts')?.valueChanges.subscribe(()=>{
		this.formulaAllRejected(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('quantityRejectedWhole')?.valueChanges.subscribe(()=>{
		this.formulaQuantityRejectedWholeKg(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('averageWeight')?.valueChanges.subscribe(()=>{
		this.formulaDailyGrowth(this.detailFormControls.getRawValue());
		this.formulaSrmDeviation(this.detailFormControls.getRawValue());
		this.formulaDeadTotal(this.detailFormControls.getRawValue());
		this.formulaQuantityRejectedWholeKg(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('deadSlaughterhouse')?.valueChanges.subscribe(()=>{
		this.formulaDeadFarmer(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('pricesoberamount')?.valueChanges.subscribe(()=>{
		this.formulaPricesobersum(this.detailFormControls.getRawValue());
	});
	this.detailFormControls?.get('age')?.valueChanges.subscribe(()=>{
		this.formulaDailyGrowth(this.detailFormControls.getRawValue());
	});

}
	updateActions(actionConfig:any){
    if(actionConfig.visibility === 'conditional' && actionConfig.conditionForButtonVisiblity){
      this.restrictEditandView(actionConfig.conditionForButtonVisiblity,'view',actionConfig.action,true);
    }
    if(actionConfig.buttonEnabled === 'conditional' && actionConfig.conditionForButtonEnable){
      this.restrictEditandView(actionConfig.conditionForButtonEnable,'edit',actionConfig.action,true);
    }
  }
	onUrlClick(event: any) {
      window.open(event.value, '_blank');
  }
	formulaTotalLesionVzl(data:any){
	const result = data.noLesionVzl + data.mildLesionVzl + data.severeLesionVzl; // formula
	this.detailFormControls?.get('totalLesionVzl')?.patchValue(result); // patch the value to form
}
	attachInfiniteScrollForAutoCompletedetailFormfeedSupplier(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormfeedSupplier = false;
          if(this.filteredItemsdetailFormfeedSupplier.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormfeedSupplier`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	restrictEditandView(ele: any, action: string, fieldName: string, fromActionBar?: boolean) {
    const conResult = this.appUtilBaseService.evaluvateCondition(ele?.query?.rules, ele?.query?.condition,{...this.data,...this.detailFormControls.getRawValue()}, this.formFieldConfig);
    if (fromActionBar) {
      this.allocateActions(fieldName, conResult, action)
    } else {
      if (action == 'view') {
        this.hiddenFields[fieldName] = conResult ? false : true;
      }
      else if (action == 'edit') {
        conResult ? this.detailFormControls.get(fieldName)?.enable({ emitEvent: false }) :
          this.detailFormControls.get(fieldName)?.disable({ emitEvent: false });
      }
    }
  }
	getUserRoles(){
      this.appBaseService.getRoles().subscribe((res:any)=>this.roles = res ||{});
   } 

editFormula(config:any,fieldname:any){
     const fieldConfig = config[fieldname]
     if(fieldConfig.formulaMethod){
       let methodName: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = fieldConfig.formulaMethod
         if(typeof this[methodName] ==="function")
          this[methodName](this.detailFormControls.getRawValue());
     }
   }
 onWorkflowhistory() {
    const workflowHistory = this.dialogService.open(WorkflowHistoryComponent, {
      header: 'Workflow History',
      width: '60%',
      contentStyle: { "max-height": "500px", "overflow": "auto" },
      styleClass: "workflow-history",
      data: {
        id: this.id,
        workflowType: this.workflowType
      }
    });
  }
	formulaPriceloadingcostsum(data:any){
	const result = data.priceloadingCosts * data.priceloadingcostvalue; // formula
	this.detailFormControls?.get('priceloadingcostsum')?.patchValue(result); // patch the value to form
}
	getId(){
      this.activatedRoute.queryParams.subscribe((params: any) => { 
        this.id = params['id'];
        this.pid = params['pid']
      }); 
    }
	formulaTotalLesionScoreVzl(data:any){
	const result = data.noLesionVzl * 0 + data.mildLesionVzl * 1 / 2 + data.severeLesionVzl * 2; // formula
	this.detailFormControls?.get('totalLesionScoreVzl')?.patchValue(result); // patch the value to form
}
	getValue(formControl: FormGroup, ele: string) {
    const parent = ele.split('?.')[0];
    if (formControl.controls[parent] instanceof FormGroup){
      const child = ele.split('?.')[1];
      return formControl.controls[parent].value[child];
    }
    else
      return formControl.controls[parent].value;
  }
	attachInfiniteScrollForAutoCompletedetailFormfeedrelationtypedefault(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormfeedrelationtypedefault = false;
          if(this.filteredItemsdetailFormfeedrelationtypedefault.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormfeedrelationtypedefault`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formulaNetCount(data:any){
	const result = data.quantitySupplied - data.deadOnArrival; // formula
	this.detailFormControls?.get('netCount')?.patchValue(result); // patch the value to form
}
	autoSuggestSearchdetailFormfeedrelationtypedefault(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormfeedrelationtypedefault){
      this.isAutoSuggestCallFireddetailFormfeedrelationtypedefault = true;
    let apiObj = Object.assign({}, RelationTypeApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormfeedrelationtypedefault = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormfeedrelationtypedefault, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormfeedrelationtypedefault = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormfeedrelationtypedefault = this.formatFilteredData(this.filteredItemsdetailFormfeedrelationtypedefault, 'feedrelationtypedefault');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormfeedrelationtypedefault = false;
    })
this.subscriptions.push(sub);}
 }
	formatCaptionItems(config: any, data: any) {
    if (Object.keys(data).length > 0) {
      return (this.appUtilBaseService.formatRawDatatoRedableFormat(config, data[config.field]));
    }
    else {
      return '';
    }
  }
	autoSuggestSearchdetailFormlaboratoryrelationtypedefault(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormlaboratoryrelationtypedefault){
      this.isAutoSuggestCallFireddetailFormlaboratoryrelationtypedefault = true;
    let apiObj = Object.assign({}, RelationTypeApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormlaboratoryrelationtypedefault = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormlaboratoryrelationtypedefault, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormlaboratoryrelationtypedefault = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormlaboratoryrelationtypedefault = this.formatFilteredData(this.filteredItemsdetailFormlaboratoryrelationtypedefault, 'laboratoryrelationtypedefault');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormlaboratoryrelationtypedefault = false;
    })
this.subscriptions.push(sub);}
 }
	confirmationPopup(fields: []) : Observable<boolean>{
    return Observable.create((observer: Observer<boolean>) => {
      this.confirmationService.confirm({
        message: this.translateService.instant('COMPLEX_RECORD_SAVE_CONFIRMATION', { field: fields.toString() }),
        header: 'Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
          fields.map((o) => $($(`.${o}_editSave-btn`)).trigger('click'));
           observer.next(true) 
           observer.complete();       
          },
        reject: () => {
          fields.map((o) => $($(`.${o}_editCancel-btn`)).trigger('click'));
          observer.next(true) 
           observer.complete(); 
        },
      });
    });
  }
	autoSuggestSearchdetailFormcatchingrelationtypedefault(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormcatchingrelationtypedefault){
      this.isAutoSuggestCallFireddetailFormcatchingrelationtypedefault = true;
    let apiObj = Object.assign({}, RelationTypeApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormcatchingrelationtypedefault = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormcatchingrelationtypedefault, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormcatchingrelationtypedefault = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormcatchingrelationtypedefault = this.formatFilteredData(this.filteredItemsdetailFormcatchingrelationtypedefault, 'catchingrelationtypedefault');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormcatchingrelationtypedefault = false;
    })
this.subscriptions.push(sub);}
 }
	onwfNoaction(){
	const params = {id:this.id,
      comments:this.comments}
	this.slaughterReportService.slaughterReportsWorkflowNoaction(params).subscribe((res:any)=>{
		this.showMessage({ severity: 'success', summary: '', detail: 'Record Updated Successfully' });
		if(Object.keys(this.mandatoryFields).length > 0){
			this.clearValidations(this.mandatoryFields);
		}
		this.onInit();
	},error=>{
		if(Object.keys(this.mandatoryFields).length > 0)
			this.clearValidations(this.mandatoryFields);
	})
}
	formulaRejectedFarmer(data:any){
	const result = data.allRejected - data.rejectedSlaughterhouse; // formula
	this.detailFormControls?.get('rejectedFarmer')?.patchValue(result); // patch the value to form
}
	workflowActionBarAction(btn: any) {
    if(!btn.wfAction) {
      if(btn.action)
        this.actionBarAction(btn);
      return;
    }
    const methodName: any = (`onwf` + btn.wfAction.charAt(0).toUpperCase() + btn.wfAction.slice(1));
    let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
    const finalArr: string[] = [];
    this.formErrors = {};
    this.inValidFields = {};
    this.mandatoryFields = this.formSecurityConfig?.mandatoryfields?.hasOwnProperty(btn.wfAction) ? this.formSecurityConfig.mandatoryfields[btn.wfAction] : {}
    if (Object.keys(this.mandatoryFields).length > 0)
      this.addValidations(this.mandatoryFields);

    if (!this.appUtilBaseService.validateNestedForms(this.detailFormControls, this.formErrors, finalArr, this.inValidFields,this.formFieldConfig)) {
      if (finalArr.length) {
        this.showMessage({ severity: 'error', summary: 'Error', detail: this.appUtilBaseService.createNotificationList(finalArr), sticky: true });
        if (Object.keys(this.mandatoryFields).length > 0)
          this.clearValidations(this.mandatoryFields);
      }
    }
  else {
      if (typeof this[action] === "function") {
        this.confirmationReference= this.dialogService.open(ConfirmationPopupComponent, {
          header: 'Confirmation',
          width: '30%',
          contentStyle: { "max-height": "500px", "overflow": "auto" },
          styleClass: "confirm-popup-container",
          data: {
            confirmationMsg: `Do you want to ${btn.wfAction} the record ?`,
            isRequired: this.formSecurityConfig.comments?.hasOwnProperty(btn.wfAction) && this.formSecurityConfig.comments[btn.wfAction],
            action:btn.label
          }
        });

        this.confirmationReference.onClose.subscribe((result: any) => {
          if (Object.keys(this.mandatoryFields).length > 0)
            this.clearValidations(this.mandatoryFields);
          if (result?.accepted) {
            this.comments = result.comments;
           if(!(this.appUtilBaseService.isEqualIgnoreCase(this.detailFormControls.getRawValue(), this.backupData, this.appUtilBaseService.getIgnorableFields(this.detailFormControls.getRawValue(), this.backupData, this.detailPageIgnoreFields), true))) {
              this.isSaveResponseReceived = false;
              const isToastNotNeeded = true;
              this.onSave(isToastNotNeeded);
              let checkResponse = setInterval(() => {
                if (this.isSaveResponseReceived) {
                  this[action]();
                  clearInterval(checkResponse);
                }
              }, 1000);
            }
            else {
              this[action]();
            }
          }
        });
      }
    }
  }
addValidations(mandatoryFields:[]){
    mandatoryFields.forEach((controlName:string)=>{
      if(this.detailFormControls.controls[controlName].hasValidator(Validators.required)){
        if(!(this.validatorsRetained.hasOwnProperty(controlName))){
          this.validatorsRetained[controlName]= {}
        }
        this.validatorsRetained[controlName]['requiredValidator'] = true;
      }
      else{
        this.detailFormControls.controls[controlName].addValidators([Validators.required]);
        this.detailFormControls.controls[controlName].updateValueAndValidity({emitEvent:false});
      }
     })
  }
	setDefaultValues(){
      for (const ele in this.formFieldConfig) {
         if(this.formFieldConfig[ele].defaultVal && (this.formFieldConfig[ele].uiType === 'select' ||this.formFieldConfig[ele].uiType === 'checkboxgroup'|| this.formFieldConfig[ele].uiType === "radio")){
           const defaultValue = (this.formFieldConfig[ele].defaultVal);
           const defaultLabel = (defaultValue.replace(/ /g,"_"))?.toUpperCase();
           if(this.formFieldConfig[ele].multiple || this.formFieldConfig[ele].uiType === 'checkboxgroup'){
            this.detailFormControls.controls[ele].patchValue([defaultLabel],{ emitEvent: false });
            this.backupData[ele]  = [defaultLabel];
           }
           else{
            this.detailFormControls.controls[ele].patchValue(defaultLabel,{ emitEvent: false });
            this.backupData[ele]  = defaultLabel;
           }
         }
         else if(['Boolean'].includes(this.formFieldConfig[ele].fieldType) ){
          const defaultBooleanValues = this.formFieldConfig[ele].defaultVal? this.formFieldConfig[ele].defaultVal:false;
         this.detailFormControls.controls[ele].patchValue(defaultBooleanValues,{ emitEvent: false });
         this.backupData[ele]  = defaultBooleanValues;
        }
      }
    
    }
	autoSuggestSearchdetailFormfeedSupplier(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormfeedSupplier){
      this.isAutoSuggestCallFireddetailFormfeedSupplier = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormfeedSupplier = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormfeedSupplier, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormfeedSupplier = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormfeedSupplier = this.formatFilteredData(this.filteredItemsdetailFormfeedSupplier, 'feedSupplier');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormfeedSupplier = false;
    })
this.subscriptions.push(sub);}
 }
	loadCaptionbarItems(){
    
}
	checkValidations(): boolean {
    const finalArr: string[] = [];
    this.formErrors = {};
    this.inValidFields = {};
    if (!this.appUtilBaseService.validateNestedForms(this.detailFormControls, this.formErrors, finalArr, this.inValidFields,this.formFieldConfig)) {
      if (finalArr.length) {
        this.showMessage({ severity: 'error', summary: 'Error', detail: this.appUtilBaseService.createNotificationList(finalArr), sticky: true });
      }
      return false;
    }
    if(this.detailFormConfig.children) {
      let multComp = this.detailFormConfig.children.filter((ele: any) => ele.multipleValues && ele.columns);
      if(multComp.length) {
        let mulFormErrors: string[] = [];
        multComp.forEach((config: any) => {
          let colnames = config.columns.filter((col: any) => col.name !== 'jid').map((col: any) => col.name);
          let values = this.detailFormControls.getRawValue()[config.field];
          values = values.filter((row: any) => {
            return Object.keys(row) && Object.keys(row).find(key => colnames.includes(key));
          });
          this.detailFormControls.get(config.field)?.patchValue(values);
          mulFormErrors = [...mulFormErrors, ...this.appUtilBaseService.validateMultipleComp(this.detailFormControls.getRawValue(), config)];          
        });
        if(mulFormErrors.length) {
          this.showMessage({ severity: 'error', summary: 'Error', detail: this.appUtilBaseService.createNotificationList(mulFormErrors), sticky: true });
          return false;
        }
      }
    }
    return true;
  }
	onBack(){
    
	this.messageService.clear();
	if (this.appUtilBaseService.isEqualIgnoreCase(this.backupData, this.detailFormControls.getRawValue(), this.appUtilBaseService.getIgnorableFields(this.detailFormControls.getRawValue(),this.backupData,this.detailPageIgnoreFields), true)){			
     this.location.back();
	}else{
		this.confirmationService.confirm({
		    message:this.translateService.instant('DISCARD_UNSAVED_CHANGES'),
			header:'Confirmation',
			icon:'pipi-info-circle',
			accept:()=>{
              this.backupData = this.appUtilBaseService.deepClone(this.detailFormControls.getRawValue());				
              this.location.back();
			},
			reject:()=>{
			},
		});
	}
}
	generateRequestObj(data: any) {
    const props = Object.keys(data);
    let requestedObj: any = {};
    props.forEach((o: any) => {
      if (this.detailFormControls.controls[o] instanceof FormGroup && !this.formFieldConfig[o].multiple) {
        data[o] = this.configureEmptyValuestoNull(data[o])
      }
      else if (this.detailFormControls.controls[o] instanceof FormGroup && this.formFieldConfig[o].multiple) {
        data[o].map((k: any, index: number) => { data[o][index] = this.configureEmptyValuestoNull(data[o][index]) })
      }
    })
    requestedObj = this.configureEmptyValuestoNull(data);
    return requestedObj;
  }
	onCancel(){
  this.messageService.clear();
  if (this.appUtilBaseService.isEqualIgnoreCase(this.backupData, this.detailFormControls.getRawValue(), this.appUtilBaseService.getIgnorableFields(this.detailFormControls.getRawValue(),this.backupData,this.detailPageIgnoreFields), true)) {
    this.showMessage({ severity: 'info', summary: '', detail: this.translateService.instant('NO_CHANGES_AVAILABLE_TO_CANCEL') });
  } else {
    this.confirmationService.confirm({
      message: this.translateService.instant('DISCARD_UNSAVED_CHANGES'),
      header: 'Confirmation',
      icon: 'pi pi-info-circle',
      accept: () => {
        const patchData = this.appUtilBaseService.deepClone(this.backupData)
        this.detailFormControls.patchValue(patchData,{ emitEvent: false });
         this.calculateFormula();
      },
      reject: () => {
      },
    });
  }

}
	onwfApprove(){
	const params = {id:this.id,
      comments:this.comments}
	this.slaughterReportService.slaughterReportsWorkflowApprove(params).subscribe((res:any)=>{
		this.showMessage({ severity: 'success', summary: '', detail: 'Record Updated Successfully' });
		if(Object.keys(this.mandatoryFields).length > 0){
			this.clearValidations(this.mandatoryFields);
		}
		this.onInit();
	},error=>{
		if(Object.keys(this.mandatoryFields).length > 0)
			this.clearValidations(this.mandatoryFields);
	})
}
	updateFields(formFieldConfig: any, ele: string, childEle: string) {
    const fieldConfig = (childEle) ? formFieldConfig[ele][childEle] : formFieldConfig[ele];

    if (fieldConfig.allowViewing === 'no') {
      if (childEle){
        if(!this.hiddenFields[this.formFieldConfig[ele].name])
          this.hiddenFields[this.formFieldConfig[ele].name] ={};
      this.hiddenFields[this.formFieldConfig[ele].name][fieldConfig.name] = true;
      }
      else
        this.hiddenFields[this.formFieldConfig[ele].name] = true;
    }
    else if (fieldConfig.viewConditionally && fieldConfig.allowViewing === 'conditional') {
      this.restrictEditandView(fieldConfig.viewConditionally, 'view', fieldConfig.name)
    }
    if (fieldConfig.allowEditing === 'no') {
      if (childEle)
        this.detailFormControls.get(`${[this.formFieldConfig[ele].name]}.${fieldConfig.name}`)?.disable({ emitEvent: false });
      else
        this.detailFormControls.get(fieldConfig.name)?.disable({ emitEvent: false });
    }
    else if (this.formFieldConfig[ele].editConditionally && this.formFieldConfig[ele].allowEditing === 'conditional') {
      this.restrictEditandView(fieldConfig.editConditionally, 'edit', fieldConfig.name)
    }
    if (this.formFieldConfig[ele].mandatory === 'conditional' && this.formFieldConfig[ele].conditionalMandatory) {
      this.addRequiredValidator(fieldConfig.conditionalMandatory, fieldConfig.name);
    }
  }
	autoSuggestSearchdetailFormfarmer(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormfarmer){
      this.isAutoSuggestCallFireddetailFormfarmer = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormfarmer = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormfarmer, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormfarmer = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormfarmer = this.formatFilteredData(this.filteredItemsdetailFormfarmer, 'farmer');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormfarmer = false;
    })
this.subscriptions.push(sub);}
 }
	attachInfiniteScrollForAutoCompletedetailFormhatchery(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormhatchery = false;
          if(this.filteredItemsdetailFormhatchery.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormhatchery`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	allocateActions(label: string, result: boolean, action: string) {
    if (action === 'view') {
      if (result && this.conditionalActions.hideActions.includes(label))
        this.conditionalActions.hideActions?.splice(this.conditionalActions.hideActions?.indexOf(label), 1)
      else if (!result && !this.conditionalActions.hideActions.includes(label))
        this.conditionalActions.hideActions.push(label);
    }
    else if (action === 'edit') {
      if (result && this.conditionalActions.disableActions.includes(label))
        this.conditionalActions.disableActions.splice(this.conditionalActions.disableActions?.indexOf(label), 1);
      else if (!result && !this.conditionalActions.disableActions.includes(label))
        this.conditionalActions.disableActions.push(label);
    }
  }
	denormalize(backupData: any) {
}
	configureEmptyValuestoNull(data:any){
       const value =  Object.keys(data).reduce((acc:any, key:string) => {acc[key] = data[key] === '' ||(Array.isArray(data[key]) && data[key].length == 0)? null : 
    data[key]; return acc; }, {})
    return value;
  }
	onwfToPurchasePricing(){
	const params = {id:this.id,
      comments:this.comments}
	this.slaughterReportService.slaughterReportsWorkflowToPurchasePricing(params).subscribe((res:any)=>{
		this.showMessage({ severity: 'success', summary: '', detail: 'Record Updated Successfully' });
		if(Object.keys(this.mandatoryFields).length > 0){
			this.clearValidations(this.mandatoryFields);
		}
		this.onInit();
	},error=>{
		if(Object.keys(this.mandatoryFields).length > 0)
			this.clearValidations(this.mandatoryFields);
	})
}
	clearValidations(mandatoryFields: []) {
    mandatoryFields.forEach((controlName: string) => {
      if (!(this.validatorsRetained[controlName] && this.validatorsRetained[controlName]['requiredValidator'])) {
        this.detailFormControls.controls[controlName].removeValidators(Validators.required);
        this.detailFormControls.controls[controlName].updateValueAndValidity({emitEvent:false});
      }

    })
  }
	attachInfiniteScrollForAutoCompletedetailFormcatchingCrew(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormcatchingCrew = false;
          if(this.filteredItemsdetailFormcatchingCrew.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormcatchingCrew`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	autoSuggestSearchdetailFormhatchery(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormhatchery){
      this.isAutoSuggestCallFireddetailFormhatchery = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormhatchery = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormhatchery, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormhatchery = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormhatchery = this.formatFilteredData(this.filteredItemsdetailFormhatchery, 'hatchery');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormhatchery = false;
    })
this.subscriptions.push(sub);}
 }
	onwfCancelBatch(){
	const params = {id:this.id,
      comments:this.comments}
	this.slaughterReportService.slaughterReportsWorkflowCancelBatch(params).subscribe((res:any)=>{
		this.showMessage({ severity: 'success', summary: '', detail: 'Record Updated Successfully' });
		if(Object.keys(this.mandatoryFields).length > 0){
			this.clearValidations(this.mandatoryFields);
		}
		this.onInit();
	},error=>{
		if(Object.keys(this.mandatoryFields).length > 0)
			this.clearValidations(this.mandatoryFields);
	})
}
	actionBarAction(btn: any) {
    const methodName: any = (`on` + btn.action.charAt(0).toUpperCase() + btn.action.slice(1));
    let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
    if (btn.action === 'navigate_to_page' && btn.pageName?.url) {
      this.router.navigateByUrl(btn.pageName.url);
    }
    else if (typeof this[action] === "function") {
      this[action]();
    }
  }
	attachInfiniteScrollForAutoCompletedetailFormstableId(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormstableId = false;
          if(this.filteredItemsdetailFormstableId.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormstableId`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formulaNetWeight(data:any){
	const result = data.grossWeight - data.grossWeight * data.weighingBridgeDiscount / 100 + data.grossWeight * data.feedDiscount / 100; // formula
	this.detailFormControls?.get('netWeight')?.patchValue(result); // patch the value to form
}
	attachInfiniteScrollForAutoCompletedetailFormveterinarian(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormveterinarian = false;
          if(this.filteredItemsdetailFormveterinarian.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormveterinarian`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formulaRejectedSlaughterhouse(data:any){
	const result = data.maxrejectslaughterhouse / 100 * data.netWeight; // formula
	this.detailFormControls?.get('rejectedSlaughterhouse')?.patchValue(result); // patch the value to form
}
	attachInfiniteScrollForAutoCompletedetailFormbeneficiaryrelationtypedefault(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormbeneficiaryrelationtypedefault = false;
          if(this.filteredItemsdetailFormbeneficiaryrelationtypedefault.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormbeneficiaryrelationtypedefault`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	attachInfiniteScrollForAutoCompletedetailFormbeneficiary(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormbeneficiary = false;
          if(this.filteredItemsdetailFormbeneficiary.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormbeneficiary`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	deattachScroll() {
  }
	autoSuggestSearchdetailFormtoCountry(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormtoCountry){
      this.isAutoSuggestCallFireddetailFormtoCountry = true;
    let apiObj = Object.assign({}, CountriesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormtoCountry = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormtoCountry, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormtoCountry = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormtoCountry = this.formatFilteredData(this.filteredItemsdetailFormtoCountry, 'toCountry');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormtoCountry = false;
    })
this.subscriptions.push(sub);}
 }
	canDeactivate(): Observable<boolean> | boolean {
    
     if(this.appUtilBaseService.isEqualIgnoreCase(this.detailFormControls.getRawValue(), this.backupData,this.appUtilBaseService.getIgnorableFields(this.detailFormControls.getRawValue(),this.backupData,this.detailPageIgnoreFields),true)){
       return true;
     }
    else{
      return Observable.create((observer: Observer<boolean>) => {
        this.confirmationService.confirm({
          message: this.translateService.instant('DISCARD_UNSAVED_CHANGES'),
          header: 'Confirmation',
          icon: 'pi pi-info-circle',
          accept: () => {
            observer.next(true);
            observer.complete();
          },
          reject: () => {
            observer.complete();
          },
        });
      });
    }
  }
	bindData(data: any, ele: any,parentEle?:string,index?:number) {
    if (ele.fieldType == 'Date' && data) {
      const formattedDate = new Date(data)
      return formattedDate;
    }
    if (ele.uiType === 'autosuggest' && ele.multipleValues) {
      let arr: any[] = [];
      if (data && Array.isArray(data)) {
        data?.map((k: any) => {
          const eName = parentEle && (index != undefined && index >=0) ? `${parentEle}_${ele.name}_${index}`: parentEle ?`${parentEle}_${ele.name}`:ele.name;
          this.createAutoSuggestFields(eName);
          this.selectedItems[eName] = data
          const lookupData ={...k.value,sid:(k.id || k.sid)}
           arr.push(lookupData);
        })
        if (arr.length > 0) {
          return arr;
        }
      }
    }
    else if (ele.uiType === 'autosuggest' && !ele.multipleValues) {
      if (data && Object.keys(data).length > 0) {
        const eName = parentEle && (index != undefined && index >= 0) ? `${parentEle}_${ele.name}_${index}` : parentEle ? `${parentEle}_${ele.name}` : ele.name;
        this.createAutoSuggestFields(eName);
        this.selectedItems[eName].push(data);
        const lookupData ={...data.value,sid:(data.id|| data.sid)}
        const value = lookupData;
        return value;
      }
    }
else if(['imageCarousel', 'attachments'].includes(ele.uiType)){
      const responseFiles = this.appUtilBaseService.createImagePreviewURL(data);
      return responseFiles;
    }
    else {
      return data;
    }
  }
	getData(){
  if (environment.prototype && this.id) {
    const params = {
      sid: this.id
    };
    this.slaughterReportService.getProtoTypingDataById(params).subscribe((res: any) => {
      this.data = res;
      this.backupData = res;
      this.detailFormControls.patchValue(this.backupData);
    });
  } else if (this.id) {
    const params = {
      sid: this.id
    };
    const dataSubscription = this.slaughterReportService.getById (params).subscribe((res: SlaughterReportBase[]) => {
      this.data = res || {};
      this.formatRawData();
    });
    this.subscriptions.push(dataSubscription);
  }
  else {
    this.backupData = this.appUtilBaseService.deepClone(this.detailFormControls.getRawValue());
  }
}
	initFormValidations(){
    this.detailFormControls.enable({ emitEvent: false });
    this.hiddenFields = {};
    this.updateAllowedActions();
    this.formValueChanges();
    this.disableDependantFields();
  }

showMessage(config:any){
    this.messageService.clear();
    this.messageService.add(config);
}
	tabviewValidation(array: any, tabIndexes: any = {} , type:string) {
    array.children.map((o: any, index: number) => {
      if (o.type === type) {
        tabIndexes[o.tabName] =o;
      }
      if (o.children)
        this.tabviewValidation(o, tabIndexes,type);
    })
    return tabIndexes;
  }
	formulaDeadSlaughterhouse(data:any){
	const result = data.maxdead / 100 * data.netWeight; // formula
	this.detailFormControls?.get('deadSlaughterhouse')?.patchValue(result); // patch the value to form
}
	initForm(){
    this.formFieldConfig= this.appUtilBaseService.getControlsFromFormConfig(this.detailFormConfig);
    this.actionBarConfig = this.appUtilBaseService.getActionsConfig(this.leftActionBarConfig.children);
    this.appUtilBaseService.configureValidators(this.detailFormControls, this.formFieldConfig);
       this.wizardItems = this.appUtilBaseService.getWizardItemFromFormConfig(this.detailFormStructureConfig, this);

 this.bindLookupFields();
    this.getData();
}
	onSelect(event: any, field: string, config: any,index?:number) { 
    field = field.replace('?.','_');
    if(index != undefined && index >=0)
    field = `${field}_${index}`
    let lookupFields: any[] = config?.lookupFields || [];
    let model = {
      id: event.sid,
      value: {}
    }
    if (lookupFields.length > 0) {
      model.value = lookupFields?.reduce((o: any, key: any) => ({ ...o, [key]: event[key] }), {});
    }
    else {
      model.value = event;
    }
    if (!this.selectedItems?.hasOwnProperty(field)) {
      this.selectedItems[field] = [];
    }
    if (config?.multiple === true) {
      this.selectedItems[field]?.push(model);
    }
    else {
      this.selectedItems[field][0] = model;
    }
  }
	attachInfiniteScrollForAutoCompletedetailFormfarmer(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormfarmer = false;
          if(this.filteredItemsdetailFormfarmer.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormfarmer`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	restrictBasedonRoles(roles: any) {
    if (roles?.includes('selected')) {
       if(this.currentUserData){
        const userDataKeys = Object.keys(this.currentUserData[0]);
        return roles?.some((item: any) => userDataKeys.includes(item.toLowerCase()));
      }
      else{
        return false;
      }
    }
    else if (roles?.includes('all'))
      return true;
    else
      return true;
  }
	autoSuggestSearchdetailFormstableId(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormstableId){
      this.isAutoSuggestCallFireddetailFormstableId = true;
    let apiObj = Object.assign({}, StablesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormstableId = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormstableId, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormstableId = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormstableId = this.formatFilteredData(this.filteredItemsdetailFormstableId, 'stableId');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormstableId = false;
    })
this.subscriptions.push(sub);}
 }
	formulaPriceconceptpremiumsum(data:any){
	const result = data.priceconceptpremiumamount * data.priceconceptpremiumvalue; // formula
	this.detailFormControls?.get('priceconceptpremiumsum')?.patchValue(result); // patch the value to form
}
	initWorkflow(){
    if (this.workFlowInitialState && this.workFlowEnabled && this.workFlowField) {
      this.detailFormConfig?.children?.forEach((ele: any) => {
        if (ele?.field === this.workFlowField && ele?.multipleValues) {
          this.detailFormControls.get(this.workFlowField)?.patchValue([this.workFlowInitialState], { emitEvent: false });
          this.backupData[this.workFlowField] = [this.workFlowInitialState];
        }
        else {
          if (ele?.field === this.workFlowField && !ele?.multipleValues) {
            this.detailFormControls.get(this.workFlowField)?.patchValue(this.workFlowInitialState, { emitEvent: false });
            this.backupData[this.workFlowField] = this.workFlowInitialState;
          }
        }
      })
      if (!environment.prototype && !this.id) {
        this.configureFormOnWorkflow();
      }
    }
  }
	autoSuggestSearchdetailFormcertificateId(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormcertificateId){
      this.isAutoSuggestCallFireddetailFormcertificateId = true;
    let apiObj = Object.assign({}, CertificatesListApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormcertificateId = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormcertificateId, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormcertificateId = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormcertificateId = this.formatFilteredData(this.filteredItemsdetailFormcertificateId, 'certificateId');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormcertificateId = false;
    })
this.subscriptions.push(sub);}
 }
	onwfBackToSlaughterDetails(){
	const params = {id:this.id,
      comments:this.comments}
	this.slaughterReportService.slaughterReportsWorkflowBackToSlaughterDetails(params).subscribe((res:any)=>{
		this.showMessage({ severity: 'success', summary: '', detail: 'Record Updated Successfully' });
		if(Object.keys(this.mandatoryFields).length > 0){
			this.clearValidations(this.mandatoryFields);
		}
		this.onInit();
	},error=>{
		if(Object.keys(this.mandatoryFields).length > 0)
			this.clearValidations(this.mandatoryFields);
	})
}
	attachInfiniteScrollForAutoCompletedetailFormbreed(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormbreed = false;
          if(this.filteredItemsdetailFormbreed.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormbreed`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	formulaDailyGrowth(data:any){
	const result = data.averageWeight / data.age; // formula
	this.detailFormControls?.get('dailyGrowth')?.patchValue(result); // patch the value to form
}
	autoSuggestSearchdetailFormsellerrelationtypedefault(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormsellerrelationtypedefault){
      this.isAutoSuggestCallFireddetailFormsellerrelationtypedefault = true;
    let apiObj = Object.assign({}, RelationTypeApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormsellerrelationtypedefault = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormsellerrelationtypedefault, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormsellerrelationtypedefault = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormsellerrelationtypedefault = this.formatFilteredData(this.filteredItemsdetailFormsellerrelationtypedefault, 'sellerrelationtypedefault');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormsellerrelationtypedefault = false;
    })
this.subscriptions.push(sub);}
 }
	normalize(data:any){
    return data;
  }
	autoSuggestSearchdetailFormweighingBridge(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormweighingBridge){
      this.isAutoSuggestCallFireddetailFormweighingBridge = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormweighingBridge = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormweighingBridge, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormweighingBridge = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormweighingBridge = this.formatFilteredData(this.filteredItemsdetailFormweighingBridge, 'weighingBridge');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormweighingBridge = false;
    })
this.subscriptions.push(sub);}
 }
	formulaDeadOnArrivalPercentage(data:any){
	const result = data.deadOnArrival / data.quantitySupplied * 100; // formula
	this.detailFormControls?.get('deadOnArrivalPercentage')?.patchValue(result); // patch the value to form
}
	formulaDeadTotal(data:any){
	const result = data.deadOnArrival * data.averageWeight / 1000; // formula
	this.detailFormControls?.get('deadTotal')?.patchValue(result); // patch the value to form
}
	getWorkflowConfig() {
    const workFlowInfo = this.data?.workflowInfo;
    const params = {
      workflowType: this.workflowType
    }
    if(environment.prototype && this.workFlowEnabled){
      this.appBaseService.getPrototypingworkflow(this.workflowType).subscribe((res:any)=>{
        this.securityJson = res.config;
      })
    }
    else if (workFlowInfo && this.workFlowEnabled){
        this.appBaseService.getWorkFlowConfig(params).subscribe((res: any) => {
          this.securityJson = res.config;
          this.configureFormOnWorkflow();
        })
    }
  }
	createAutoSuggestFields(ele: any) {
    if (!this.selectedItems?.hasOwnProperty(ele)) {
      this.selectedItems[ele] = [];
    }

  }
	formulaPricesobersum(data:any){
	const result = data.pricesoberamount * data.pricesobervalue; // formula
	this.detailFormControls?.get('pricesobersum')?.patchValue(result); // patch the value to form
}
	getDisabled(formControl: FormGroup, ele: string) {
  const parent = ele.split('?.')[0];
  if (formControl.controls[parent] instanceof FormGroup){
    return formControl.get(ele)?.disabled
  }
  else
    return formControl.controls[parent].disabled;
}
	formulaSrmDeviation(data:any){
	const result = data.averageWeight - data.srmNotification; // formula
	this.detailFormControls?.get('srmDeviation')?.patchValue(result); // patch the value to form
}
	getSelectedObject(field:string,options:any){
      const selectedObj = (options.filter((item: { label: any}) => (item.label)?.toUpperCase() === field?.toUpperCase()));
      return selectedObj[0];
  }
	formulaAverageWeight(data:any){
	const result = data.netWeight / data.quantitySupplied * 1000; // formula
	this.detailFormControls?.get('averageWeight')?.patchValue(result); // patch the value to form
}
	formValueChanges() {
    this.detailFormControls.valueChanges.pipe(
      debounceTime(600),
      distinctUntilChanged(),
    )
      .subscribe(() => {
        this.updateAllowedActions();
        this.isFormValueChanged = true;
      })
  }
	checkUnsavedComplexFields():any {
    const newRows:any = []
    let hasUnSavedChanges:boolean = false;
    const data = this.detailFormControls.getRawValue();
    const fields:any =[];
    const complexTable = (Object.keys(this.formFieldConfig)).filter(key=> this.formFieldConfig[key].columns && this.formFieldConfig[key].multipleValues)
       if(complexTable.length> 0){ 
         complexTable.map((field)=>{
          data[field]?.map((e: any) => {
            if(e.isNewRow || this.fieldEditMode[field]){
              hasUnSavedChanges = true;
              fields.push(field);
            }
              newRows.push(e);
          })
         })
       }
       return {
        newRows:newRows,
        fields:[...new Set(fields)],
        hasUnSavedChanges:hasUnSavedChanges
      }
    }
	onwfPricingDone(){
	const params = {id:this.id,
      comments:this.comments}
	this.slaughterReportService.slaughterReportsWorkflowPricingDone(params).subscribe((res:any)=>{
		this.showMessage({ severity: 'success', summary: '', detail: 'Record Updated Successfully' });
		if(Object.keys(this.mandatoryFields).length > 0){
			this.clearValidations(this.mandatoryFields);
		}
		this.onInit();
	},error=>{
		if(Object.keys(this.mandatoryFields).length > 0)
			this.clearValidations(this.mandatoryFields);
	})
}
	autoSuggestSearchdetailFormorigin(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormorigin){
      this.isAutoSuggestCallFireddetailFormorigin = true;
    let apiObj = Object.assign({}, CountriesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormorigin = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormorigin, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormorigin = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormorigin = this.formatFilteredData(this.filteredItemsdetailFormorigin, 'origin');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormorigin = false;
    })
this.subscriptions.push(sub);}
 }
	scrolltoTop() {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('main-content')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));
    const headerHeight: any = $('#header-container').outerHeight(true);
    const titleBarheight: any = $('#title-bar').outerHeight(true);

    windowYOffsetObservable.subscribe((scrollPos: number) => {
      if (scrollPos >= titleBarheight) {
        $('.wizard-container .p-tieredmenu').animate({
          top: headerHeight, marginTop: '10px'
        }, 0);
      }
      else {
        $('.wizard-container .p-tieredmenu').animate({
          top: headerHeight + titleBarheight
        }, 0);
      }
    })
  }
	addRequiredValidator(ele: any, fieldName: string) {
    const conResult = this.appUtilBaseService.evaluvateCondition(ele.query?.rules, ele.query?.condition, this.detailFormControls.getRawValue(), this.formFieldConfig);
    if (conResult) {
      this.detailFormControls.controls[fieldName].addValidators([Validators.required]);
    }
    else {
      this.detailFormControls.controls[fieldName].removeValidators([Validators.required]);
    }
    this.detailFormControls.controls[fieldName].updateValueAndValidity({ emitEvent: false });
    this.formFieldConfig[fieldName].isRequired = (conResult) ? true : false;
  }
	attachInfiniteScrollForAutoCompletedetailFormweighingBridge(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormweighingBridge = false;
          if(this.filteredItemsdetailFormweighingBridge.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormweighingBridge`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	attachInfiniteScrollForAutoCompletedetailFormweighingrelationtypedefault(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormweighingrelationtypedefault = false;
          if(this.filteredItemsdetailFormweighingrelationtypedefault.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormweighingrelationtypedefault`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	updateAllowedActions() {
    for (const ele in this.formFieldConfig) {
      if (this.detailFormControls.controls[ele] instanceof FormGroup) {
        for (const childEle in this.formFieldConfig[ele])
          this.updateFields(this.formFieldConfig, ele, childEle);
      }
      else {
        this.updateFields(this.formFieldConfig, ele, '');
   }
    }

    this.actionBarConfig?.forEach((action:any)=>{
      this.updateActions(action);
    })
  }
	attachInfiniteScrollForAutoCompletedetailFormfromCountry(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFireddetailFormfromCountry = false;
          if(this.filteredItemsdetailFormfromCountry.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchdetailFormfromCountry`
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	reloadForm(workflowInfo: any) {
    if(this.id){
      const dataObsr$: any = this.data ? of(this.data) : this.getData();
      dataObsr$.subscribe((res: any) => {
        this.data.workflowInfo ={
          [this.workflowType]:workflowInfo
        };
       const methodName: any = "configureFormOnWorkflow";
        let action: Exclude<keyof SlaughterReportDetailBaseComponent, ' '> = methodName;
        if (typeof this[action] === "function") {
          this[action]();
          this.updateAllowedActions();
          this.formValueChanges();
        }
       
       
      })
    }
  }
	autoSuggestSearchdetailFormfromCountry(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormfromCountry){
      this.isAutoSuggestCallFireddetailFormfromCountry = true;
    let apiObj = Object.assign({}, CountriesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormfromCountry = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormfromCountry, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormfromCountry = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormfromCountry = this.formatFilteredData(this.filteredItemsdetailFormfromCountry, 'fromCountry');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormfromCountry = false;
    })
this.subscriptions.push(sub);}
 }
	autoSuggestSearchdetailFormveterinarian(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormveterinarian){
      this.isAutoSuggestCallFireddetailFormveterinarian = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormveterinarian = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormveterinarian, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormveterinarian = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormveterinarian = this.formatFilteredData(this.filteredItemsdetailFormveterinarian, 'veterinarian');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormveterinarian = false;
    })
this.subscriptions.push(sub);}
 }
	configureRequestData(data: any, ele: any, parentEle?:string,index?:number) {
if (Array.isArray(ele)) {
    ele.map((ele: any) => {
      if (ele.fieldType == 'Date' && data[ele.name]) {
        const formattedDate = new Date(data[ele.name]).getTime()
        data[ele.name] = formattedDate;
      }
      else if (ele.fieldType === 'Boolean') {
        if (data[ele.name] === null || data[ele.name] === undefined || data[ele.name] === '') {
          data[ele.name] = false;
        }
      }          
      if(ele.uiType == 'autosuggest' && !(Array.isArray(data[ele.name]) || typeof data[ele.name] === 'object')) {
        data[ele.name] = null;
      }
    })
   }
    if (Object.keys(this.selectedItems).length > 0) {
      const keys = Object.keys(this.selectedItems);
      keys?.forEach((k) => {
        const e = parentEle? k.split('_')[1]:k;
       if(parentEle && (index != undefined && index >=0)) {
          const keyarr = k.split('_');
        
          if (data.hasOwnProperty(e) && parentEle === keyarr[0] && index.toString() == keyarr[2]) {
            data[e] = this.selectedItems[k];
          }
        } 
        else if(parentEle){
          if (data.hasOwnProperty(e) && parentEle === k.split('_')[0]) {
            data[e] = this.selectedItems[k];
          }
        }
        else {
          if (data.hasOwnProperty(e) ) {
            data[e] = this.selectedItems[k];
          }
        }
      });
      ele.map((ele: any) => {
      if (ele.uiType == 'autosuggest' && data[ele.name] && !ele.multipleValues) {
          data[ele.name] = (Array.isArray(data[ele.name]) && data[ele.name].length > 0) ? data[ele.name][0] :(Array.isArray(data[ele.name]) && data[ele.name].length <=0)? {}: data[ele.name];
        }
      })
    }
    return data;
  }
	autoSuggestSearchdetailFormcatchingCrew(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormcatchingCrew){
      this.isAutoSuggestCallFireddetailFormcatchingCrew = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormcatchingCrew = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormcatchingCrew, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormcatchingCrew = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormcatchingCrew = this.formatFilteredData(this.filteredItemsdetailFormcatchingCrew, 'catchingCrew');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormcatchingCrew = false;
    })
this.subscriptions.push(sub);}
 }
	autoSuggestSearchdetailFormweighingrelationtypedefault(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormweighingrelationtypedefault){
      this.isAutoSuggestCallFireddetailFormweighingrelationtypedefault = true;
    let apiObj = Object.assign({}, RelationTypeApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormweighingrelationtypedefault = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormweighingrelationtypedefault, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormweighingrelationtypedefault = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormweighingrelationtypedefault = this.formatFilteredData(this.filteredItemsdetailFormweighingrelationtypedefault, 'weighingrelationtypedefault');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormweighingrelationtypedefault = false;
    })
this.subscriptions.push(sub);}
 }
	onwfNotApproved(){
	const params = {id:this.id,
      comments:this.comments}
	this.slaughterReportService.slaughterReportsWorkflowNotApproved(params).subscribe((res:any)=>{
		this.showMessage({ severity: 'success', summary: '', detail: 'Record Updated Successfully' });
		if(Object.keys(this.mandatoryFields).length > 0){
			this.clearValidations(this.mandatoryFields);
		}
		this.onInit();
	},error=>{
		if(Object.keys(this.mandatoryFields).length > 0)
			this.clearValidations(this.mandatoryFields);
	})
}
	formulaQtySlaughtered(data:any){
	const result = data.quantitySupplied - data.deadOnArrival; // formula
	this.detailFormControls?.get('qtySlaughtered')?.patchValue(result); // patch the value to form
}
	disableDependantFields() {
    let hasUnfilledValues:boolean = false;
    const detailFormControls = this.detailFormControls.getRawValue()
    for (const ele in this.formFieldConfig) {
      if (this.formFieldConfig[ele].uiType === 'autosuggest' && this.formFieldConfig[ele].mandatoryFilters) {
        this.formFieldConfig[ele].filterInputMapping?.map((filter: { lookupField: string,tableField:string }) => {
          const fieldName = filter.tableField.split('.')[0];
          if (this.formFieldConfig[ele].mandatoryFilters.includes(filter.lookupField) && (detailFormControls[fieldName] === null || detailFormControls[fieldName]=== undefined || detailFormControls[fieldName] === '')){
            hasUnfilledValues = true;
          }
        })
        hasUnfilledValues?this.detailFormControls.get(this.formFieldConfig[ele].fieldName)?.disable({ emitEvent: false }):
        this.detailFormControls.get(this.formFieldConfig[ele].fieldName)?.enable({ emitEvent: false });
      }
    }
  }
	openWorkflowSimilator() {
    const ref = this.dialogService.open(WorkflowSimulatorComponent, {
      header: 'Workflow Simulator',
      width: '350px',
      data: {
        statusFieldConfig: this.formFieldConfig[this.workFlowField],
        actorFieldConfig: Object.values(this.roles)?.map((actor)=> {return{label:actor,value:actor}}),
        selectedValues: (this.data?.workflowInfo)? this.data?.workflowInfo[this.workflowType]:{userTypes:[],step:''}
      }
    });
    ref.onClose.subscribe((workflowInfo: any) => {
      if (workflowInfo) {
        this.reloadForm(workflowInfo);
      }
    });
  }
	autoSuggestSearchdetailFormbreed(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFireddetailFormbreed){
      this.isAutoSuggestCallFireddetailFormbreed = true;
    let apiObj = Object.assign({}, BreedApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.detailFormControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFireddetailFormbreed = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemsdetailFormbreed, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemsdetailFormbreed = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemsdetailFormbreed = this.formatFilteredData(this.filteredItemsdetailFormbreed, 'breed');
    }, (err: any) => {
      this.isAutoSuggestCallFireddetailFormbreed = false;
    })
this.subscriptions.push(sub);}
 }
	checkColumnProperty(fieldName: string, table: string) {
    const columnConfig = this.formFieldConfig[table]?.columns?.find((item: any) => item.name == fieldName);
    return ((columnConfig.hidden) ? false : true);
  }
	onwfToSlaughterDetails(){
	const params = {id:this.id,
      comments:this.comments}
	this.slaughterReportService.slaughterReportsWorkflowToSlaughterDetails(params).subscribe((res:any)=>{
		this.showMessage({ severity: 'success', summary: '', detail: 'Record Updated Successfully' });
		if(Object.keys(this.mandatoryFields).length > 0){
			this.clearValidations(this.mandatoryFields);
		}
		this.onInit();
	},error=>{
		if(Object.keys(this.mandatoryFields).length > 0)
			this.clearValidations(this.mandatoryFields);
	})
}
	onWizardClick(event: any) {
    this.wizardItems.forEach((item: any) => {
      delete item.styleClass;
    });
    event.item.styleClass = 'wizard-active';
    let el: any = $("#section" + event.item.id);
    if (el?.length) {

      $('.main-content').animate({
        scrollTop: el.offset().top
      }, 100);
    }
  }

    onInit() {
		
		this.waitForResponse();
this.initWorkflow();
		
this.getId();   
this.initForm();
this.setDefaultValues();
this.initFormValidations();
this.activeTabIndexes = this.appUtilBaseService.generateDynamicTabViewProps(this.detailFormStructureConfig);
this.showWorkflowSimulator = Boolean(this.isPrototype && this.workFlowEnabled && this.id);
this.getUserRoles();
this.calculateFormula();
    }
	
     onDestroy() {
		
    }
     onAfterViewInit() {
		
		this.scrolltoTop();
    }

}

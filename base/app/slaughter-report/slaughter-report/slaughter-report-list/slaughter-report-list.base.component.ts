import { SlaughterReportService } from '../slaughter-report.service';
import { SlaughterReportBase} from '../slaughter-report.base.model';
import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component'
import { CompaniesApiConstants } from '@baseapp/master-data-tables/farmer-data/companies/companies.api-constants';
import { CountriesApiConstants } from '@baseapp/master-data-tables/background-lists/countries/countries.api-constants';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ElementRef, Renderer2, ViewChild } from '@angular/core';
import { AppConstants } from '@app/app-constants';
import { BaseService } from '@baseapp/base.service';
import { fromEvent, Subscription, map } from 'rxjs';
import { allowedValuesValidator } from '@baseapp/widgets/validators/allowedValuesValidator';
import { environment } from '@env/environment';
import { Filter } from '@baseapp/vs-models/filter.model';
import { BaseAppConstants } from '@baseapp/app-constants.base';
import { AppGlobalService } from '@baseapp/app-global.service';

@Directive(
{
	providers:[MessageService, ConfirmationService, DialogService]
}
)
export class SlaughterReportListBaseComponent{
	
	
	quickFilter: any;
hiddenFields:any = {};
quickFilterFieldConfig:any={}
	bsModalRef?: BsModalRef;
	isSearchFocused:boolean = false;
showBreadcrumb = AppConstants.showBreadcrumb;
	
showAdvancedSearch: boolean = false;

tableSearchFieldConfig:any = {};
@ViewChild('toggleButton')
  toggleButton!: ElementRef;
  @ViewChild('menu')
  menu!: ElementRef;

	
filteredItemstableSearchfarmer:any = [];
isAutoSuggestCallFiredtableSearchfarmer: boolean = false;
	
filteredItemstableSearchtoCountry:any = [];
isAutoSuggestCallFiredtableSearchtoCountry: boolean = false;
	selectedValues!: [];
  filter: Filter = {
    globalSearch: '',
    advancedSearch: {},
    sortField: null,
    sortOrder: null,
    quickFilter: {}
  };
params: any;
isMobile: boolean = AppConstants.isMobile;

  gridData: SlaughterReportBase[] = [];
  totalRecords: number = 0;
  subscriptions: Subscription[] = [];
 multiSortMeta:any =[];
 selectedColumns:any =[];
subHeader: any;
  autoSuggest: any;
  query: any;

rightFreezeColums:any;
total:number =0;
inValidFields:any = {};
selectedItems:any ={};
scrollTop:number =0;
isRowSelected: boolean = false;
isPrototype = environment.prototype;
  workFlowEnabled = false;
isList = true;
isPageLoading:boolean = false;
autoSuggestPageNo:number = 0;
complexAutoSuggestPageNo:number = 0
localStorageStateKey = "slaughter-report-list";
showMenu: boolean = false;
conditionalActions:any ={
  disableActions:[],
  hideActions:[]
}
actionBarConfig:any =[];
first: number =0;
rows: number = 0;
updatedRecords:SlaughterReportBase[] = [];
showPaginationOnTop = AppConstants.showPaginationonTop;
 showPaginationOnBottom = AppConstants.showPaginationonBottom;
 tableFieldConfig:any ={};
dateFormat: string = AppConstants.calDateFormat;
selectedRowId: any = '';
 showWorkflowSimulator:boolean = false;
	isChildPage:boolean = false;
	
filteredItemstableSearchfromCountry:any = [];
isAutoSuggestCallFiredtableSearchfromCountry: boolean = false;

	
	leftActionBarConfig : any = {
  "children" : [ {
    "outline" : "true",
    "buttonType" : "icon_on_left",
    "visibility" : "show",
    "showOn" : "both",
    "buttonStyle" : "curved",
    "action" : "new",
    "buttonEnabled" : "yes",
    "label" : "NEW",
    "type" : "button"
  }, {
    "outline" : "true",
    "buttonType" : "icon_only",
    "visibility" : "show",
    "showOn" : "both",
    "buttonStyle" : "curved",
    "icon" : {
      "type" : "icon",
      "icon" : {
        "label" : "fas fa-trash-alt",
        "value" : "fas fa-trash-alt"
      },
      "iconColor" : "#000000",
      "iconSize" : "13px"
    },
    "action" : "delete",
    "buttonEnabled" : "yes",
    "label" : "DELETE",
    "type" : "button"
  }, {
    "outline" : "true",
    "buttonType" : "icon_only",
    "visibility" : "show",
    "showOn" : "both",
    "buttonStyle" : "curved",
    "icon" : {
      "type" : "icon",
      "icon" : {
        "label" : "fas fa-sync",
        "value" : "fas fa-sync"
      },
      "iconColor" : "#000000",
      "iconSize" : "13px"
    },
    "action" : "refresh",
    "buttonEnabled" : "yes",
    "label" : "REFRESH",
    "type" : "button"
  } ]
}
	rightActionBarConfig : any = { }
	tableSearchConfig : any = {
  "children" : [ {
    "label" : "REPORT_ID",
    "data" : "",
    "field" : "reportId",
    "type" : "searchField",
    "fieldType" : "number",
    "fieldId" : "reportId",
    "timeOnly" : false,
    "uiType" : "number",
    "name" : "reportId",
    "fieldName" : "reportId"
  }, {
    "label" : "SEQUENCE_NR",
    "data" : "",
    "field" : "sequenceNr",
    "type" : "searchField",
    "fieldType" : "number",
    "fieldId" : "sequenceNr",
    "timeOnly" : false,
    "uiType" : "number",
    "name" : "sequenceNr",
    "fieldName" : "sequenceNr"
  }, {
    "label" : "STATUS",
    "data" : "",
    "field" : "status",
    "type" : "searchField",
    "fieldType" : "string",
    "multipleValues" : false,
    "uiType" : "select",
    "allowedValues" : {
      "values" : [ {
        "label" : "SUPPLY_AND_FLOCK_DATA",
        "value" : "SUPPLY_AND_FLOCK_DATA"
      }, {
        "label" : "SLAUGHTER_DETAILS",
        "value" : "SLAUGHTER_DETAILS"
      }, {
        "label" : "PURCHASE_PRICING",
        "value" : "PURCHASE_PRICING"
      }, {
        "label" : "TO_BE_APPROVED",
        "value" : "TO_BE_APPROVED"
      }, {
        "label" : "FINALIZED",
        "value" : "FINALIZED"
      }, {
        "label" : "CANCELLED",
        "value" : "CANCELLED"
      } ],
      "conditions" : {
        "conditionType" : "None",
        "conditions" : [ ]
      }
    },
    "fieldId" : "status",
    "defaultVal" : "",
    "placeHolder" : "",
    "timeOnly" : false,
    "name" : "status",
    "fieldName" : "status"
  }, {
    "label" : "SLAUGHTER_DATE",
    "data" : "",
    "field" : "slaughterDate",
    "type" : "searchField",
    "fieldType" : "Date",
    "fieldId" : "slaughterDate",
    "timeOnly" : false,
    "uiType" : "date",
    "name" : "slaughterDate",
    "fieldName" : "slaughterDate"
  }, {
    "label" : "SLAUGHTER_BATCH_ID",
    "data" : "",
    "field" : "slaughterBatchId",
    "type" : "searchField",
    "fieldType" : "number",
    "fieldId" : "slaughterBatchId",
    "timeOnly" : false,
    "uiType" : "number",
    "name" : "slaughterBatchId",
    "fieldName" : "slaughterBatchId"
  }, {
    "label" : "FARMER",
    "data" : "",
    "field" : "farmer",
    "type" : "searchField",
    "fieldType" : "any",
    "multipleValues" : false,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "farmerName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "farmer",
    "timeOnly" : false,
    "uiType" : "autosuggest",
    "name" : "farmer",
    "fieldName" : "farmer"
  }, {
    "label" : "FARMER_NAME",
    "data" : "",
    "field" : "farmerName",
    "type" : "searchField",
    "fieldType" : "string",
    "multipleValues" : false,
    "fieldId" : "farmerName",
    "timeOnly" : false,
    "uiType" : "text",
    "name" : "farmerName",
    "fieldName" : "farmerName"
  }, {
    "label" : "INVOICE_DATE",
    "data" : "",
    "field" : "invoiceDate",
    "type" : "searchField",
    "fieldType" : "Date",
    "fieldId" : "invoiceDate",
    "timeOnly" : false,
    "uiType" : "date",
    "name" : "invoiceDate",
    "fieldName" : "invoiceDate"
  }, {
    "label" : "FROM_COUNTRY",
    "data" : "",
    "field" : "fromCountry",
    "type" : "searchField",
    "fieldType" : "any",
    "multipleValues" : false,
    "lookupFields" : [ "countryName", "countryCode" ],
    "functionalPrimaryKey" : [ "countryCode" ],
    "displayField" : "countryCode",
    "mandatoryFilters" : [ "countryCode" ],
    "lookupUrl" : "countries/autosuggest",
    "fieldId" : "fromCountry",
    "timeOnly" : false,
    "uiType" : "autosuggest",
    "name" : "fromCountry",
    "fieldName" : "fromCountry"
  }, {
    "label" : "TO_COUNTRY",
    "data" : "",
    "field" : "toCountry",
    "type" : "searchField",
    "fieldType" : "any",
    "multipleValues" : false,
    "lookupFields" : [ "countryName", "countryCode" ],
    "functionalPrimaryKey" : [ "countryCode" ],
    "displayField" : "countryCode",
    "mandatoryFilters" : [ "countryCode" ],
    "lookupUrl" : "countries/autosuggest",
    "fieldId" : "toCountry",
    "timeOnly" : false,
    "uiType" : "autosuggest",
    "name" : "toCountry",
    "fieldName" : "toCountry"
  } ],
  "columns" : "2",
  "type" : "tableSearch",
  "showAdvancedSearch" : true
}
	quickFilterConfig : any = { }
	tableConfig : any = {
  "rightFreezeFromColumn" : "0",
  "currentNode" : "table",
  "columnReorder" : false,
  "type" : "grid",
  "showDetailPageAs" : "navigate_to_new_page",
  "rowGroup" : "",
  "storageName" : "slaughter-report-list",
  "alignColumns" : {
    "css" : ".container{ display:grid; grid-template-columns: repeat(4, 1fr); grid-template-rows: repeat(3, 1fr); grid-gap: 0px;} #1e62ddaa-a72e-446b-9b99-4cd9e1636a7f{ grid-row:1/2; grid-column:1/2;}#06c3eeaa-f177-42d2-9d44-ffd4d28e18fd{ grid-row:2/3; grid-column:4/5;}#eeae8111-12fa-454d-9cb2-b9804a68cc81{ grid-row:2/3; grid-column:1/2;}#8c862084-10ef-40a7-bd9c-e84c1b9104ea{ grid-row:1/2; grid-column:4/5;}#e0ac9d0c-41b6-4d8f-bd6f-b1d1ef988390{ grid-row:2/3; grid-column:2/3;}#affbd293-501b-4a23-b8c2-7d2fbebfffff{ grid-row:1/2; grid-column:2/3;}#860a7e8b-196e-468e-9175-0d2677fc947c{ grid-row:1/2; grid-column:3/4;}#091a005c-8a68-416b-9140-fdab068e3dbe{ grid-row:2/3; grid-column:3/4;}#8e0ef8b9-1703-487a-907e-c6eec781c64d{ grid-row:3/4; grid-column:1/2;}#d14e4d67-e663-497d-a574-96366b9e31ad{ grid-row:3/4; grid-column:2/3;}",
    "raw" : [ {
      "cols" : 1,
      "rows" : 1,
      "y" : 0,
      "x" : 0,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "1e62ddaa-a72e-446b-9b99-4cd9e1636a7f",
      "label" : "Report ID"
    }, {
      "cols" : 1,
      "rows" : 1,
      "y" : 1,
      "x" : 3,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "06c3eeaa-f177-42d2-9d44-ffd4d28e18fd",
      "label" : "Sequence nr"
    }, {
      "cols" : 1,
      "rows" : 1,
      "y" : 1,
      "x" : 0,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "eeae8111-12fa-454d-9cb2-b9804a68cc81",
      "label" : "Status"
    }, {
      "cols" : 1,
      "rows" : 1,
      "y" : 0,
      "x" : 3,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "8c862084-10ef-40a7-bd9c-e84c1b9104ea",
      "label" : "Slaughter Date"
    }, {
      "cols" : 1,
      "rows" : 1,
      "y" : 1,
      "x" : 1,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "e0ac9d0c-41b6-4d8f-bd6f-b1d1ef988390",
      "label" : "Slaughter batch ID"
    }, {
      "cols" : 1,
      "rows" : 1,
      "y" : 0,
      "x" : 1,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "affbd293-501b-4a23-b8c2-7d2fbebfffff",
      "label" : "Farmer"
    }, {
      "cols" : 1,
      "rows" : 1,
      "y" : 0,
      "x" : 2,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "860a7e8b-196e-468e-9175-0d2677fc947c",
      "label" : "Farmer name"
    }, {
      "cols" : 1,
      "rows" : 1,
      "y" : 1,
      "x" : 2,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "091a005c-8a68-416b-9140-fdab068e3dbe",
      "label" : "Invoice date"
    }, {
      "cols" : 1,
      "rows" : 1,
      "y" : 2,
      "x" : 0,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "8e0ef8b9-1703-487a-907e-c6eec781c64d",
      "label" : "From country"
    }, {
      "cols" : 1,
      "rows" : 1,
      "y" : 2,
      "x" : 1,
      "minItemRows" : 1,
      "minItemCols" : 1,
      "id" : "d14e4d67-e663-497d-a574-96366b9e31ad",
      "label" : "To country"
    } ]
  },
  "HEADER_TAG" : "<th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"reportId\"\n        (click)=\"sort($event,'reportId')\"\n     *ngIf=\"columns[0]\">\n    <span [innerHTML]=\"columns[0].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'reportId'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"sequenceNr\"\n        (click)=\"sort($event,'sequenceNr')\"\n     *ngIf=\"columns[1]\">\n    <span [innerHTML]=\"columns[1].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'sequenceNr'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"status\"\n        (click)=\"sort($event,'status')\"\n     *ngIf=\"columns[2]\">\n    <span [innerHTML]=\"columns[2].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'status'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"slaughterDate\"\n        (click)=\"sort($event,'slaughterDate')\"\n     *ngIf=\"columns[3]\">\n    <span [innerHTML]=\"columns[3].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'slaughterDate'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"slaughterBatchId\"\n        (click)=\"sort($event,'slaughterBatchId')\"\n     *ngIf=\"columns[4]\">\n    <span [innerHTML]=\"columns[4].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'slaughterBatchId'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"farmer\"\n        (click)=\"sort($event,'farmer')\"\n     *ngIf=\"columns[5]\">\n    <span [innerHTML]=\"columns[5].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'farmer'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"farmerName\"\n        (click)=\"sort($event,'farmerName')\"\n     *ngIf=\"columns[6]\">\n    <span [innerHTML]=\"columns[6].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'farmerName'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"invoiceDate\"\n        (click)=\"sort($event,'invoiceDate')\"\n     *ngIf=\"columns[7]\">\n    <span [innerHTML]=\"columns[7].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'invoiceDate'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"fromCountry\"\n        (click)=\"sort($event,'fromCountry')\"\n     *ngIf=\"columns[8]\">\n    <span [innerHTML]=\"columns[8].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'fromCountry'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"toCountry\"\n        (click)=\"sort($event,'toCountry')\"\n     *ngIf=\"columns[9]\">\n    <span [innerHTML]=\"columns[9].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'toCountry'\"></p-sortIcon>\n</th>",
  "children" : [ {
    "label" : "REPORT_ID",
    "data" : "Report ID",
    "width" : "120px",
    "showOnMobile" : "true",
    "showLabel" : false,
    "field" : "reportId",
    "type" : "gridColumn",
    "currentNode" : "1e62ddaa-a72e-446b-9b99-4cd9e1636a7f",
    "valueChange" : true,
    "fieldType" : "number",
    "fieldId" : "reportId",
    "timeOnly" : false,
    "uiType" : "number",
    "name" : "reportId",
    "fieldName" : "reportId"
  }, {
    "label" : "SEQUENCE_NR",
    "data" : "",
    "field" : "sequenceNr",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "number",
    "fieldId" : "sequenceNr",
    "timeOnly" : false,
    "uiType" : "number",
    "name" : "sequenceNr",
    "fieldName" : "sequenceNr"
  }, {
    "label" : "STATUS",
    "data" : "",
    "field" : "status",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "string",
    "multipleValues" : false,
    "uiType" : "select",
    "allowedValues" : {
      "values" : [ {
        "label" : "SUPPLY_AND_FLOCK_DATA",
        "value" : "SUPPLY_AND_FLOCK_DATA"
      }, {
        "label" : "SLAUGHTER_DETAILS",
        "value" : "SLAUGHTER_DETAILS"
      }, {
        "label" : "PURCHASE_PRICING",
        "value" : "PURCHASE_PRICING"
      }, {
        "label" : "TO_BE_APPROVED",
        "value" : "TO_BE_APPROVED"
      }, {
        "label" : "FINALIZED",
        "value" : "FINALIZED"
      }, {
        "label" : "CANCELLED",
        "value" : "CANCELLED"
      } ],
      "conditions" : {
        "conditionType" : "None",
        "conditions" : [ ]
      }
    },
    "fieldId" : "status",
    "timeOnly" : false,
    "name" : "status",
    "fieldName" : "status"
  }, {
    "label" : "SLAUGHTER_DATE",
    "data" : "",
    "field" : "slaughterDate",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "Date",
    "fieldId" : "slaughterDate",
    "timeOnly" : false,
    "uiType" : "date",
    "name" : "slaughterDate",
    "fieldName" : "slaughterDate"
  }, {
    "label" : "SLAUGHTER_BATCH_ID",
    "data" : "",
    "field" : "slaughterBatchId",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "number",
    "fieldId" : "slaughterBatchId",
    "timeOnly" : false,
    "uiType" : "number",
    "name" : "slaughterBatchId",
    "fieldName" : "slaughterBatchId"
  }, {
    "label" : "FARMER",
    "data" : "",
    "field" : "farmer",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "any",
    "multipleValues" : false,
    "lookupFields" : [ "relationType", "administration", "address", "companyCode", "postalCode", "standardPurchaseNotation", "name", "city" ],
    "functionalPrimaryKey" : [ "companyCode" ],
    "displayField" : "companyCode",
    "mandatoryFilters" : [ "relationType_value_relationTypeCode" ],
    "optionalFilters" : [ ],
    "filterOutputMapping" : [ {
      "lookupField" : "relationType",
      "tableField" : ""
    }, {
      "lookupField" : "administration",
      "tableField" : ""
    }, {
      "lookupField" : "address",
      "tableField" : ""
    }, {
      "lookupField" : "companyCode",
      "tableField" : ""
    }, {
      "lookupField" : "postalCode",
      "tableField" : ""
    }, {
      "lookupField" : "standardPurchaseNotation",
      "tableField" : ""
    }, {
      "lookupField" : "name",
      "tableField" : "farmerName"
    }, {
      "lookupField" : "city",
      "tableField" : ""
    } ],
    "lookupUrl" : "companies/autosuggest",
    "fieldId" : "farmer",
    "timeOnly" : false,
    "uiType" : "autosuggest",
    "name" : "farmer",
    "fieldName" : "farmer"
  }, {
    "label" : "FARMER_NAME",
    "data" : "",
    "field" : "farmerName",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "string",
    "multipleValues" : false,
    "fieldId" : "farmerName",
    "timeOnly" : false,
    "uiType" : "text",
    "name" : "farmerName",
    "fieldName" : "farmerName"
  }, {
    "label" : "INVOICE_DATE",
    "data" : "",
    "field" : "invoiceDate",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "Date",
    "fieldId" : "invoiceDate",
    "timeOnly" : false,
    "uiType" : "date",
    "name" : "invoiceDate",
    "fieldName" : "invoiceDate"
  }, {
    "label" : "FROM_COUNTRY",
    "data" : "",
    "field" : "fromCountry",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "any",
    "multipleValues" : false,
    "lookupFields" : [ "countryName", "countryCode" ],
    "functionalPrimaryKey" : [ "countryCode" ],
    "displayField" : "countryCode",
    "mandatoryFilters" : [ "countryCode" ],
    "lookupUrl" : "countries/autosuggest",
    "fieldId" : "fromCountry",
    "timeOnly" : false,
    "uiType" : "autosuggest",
    "name" : "fromCountry",
    "fieldName" : "fromCountry"
  }, {
    "label" : "TO_COUNTRY",
    "data" : "",
    "field" : "toCountry",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "any",
    "multipleValues" : false,
    "lookupFields" : [ "countryName", "countryCode" ],
    "functionalPrimaryKey" : [ "countryCode" ],
    "displayField" : "countryCode",
    "mandatoryFilters" : [ "countryCode" ],
    "lookupUrl" : "countries/autosuggest",
    "fieldId" : "toCountry",
    "timeOnly" : false,
    "uiType" : "autosuggest",
    "name" : "toCountry",
    "fieldName" : "toCountry"
  } ],
  "valueChange" : true,
  "toggleColumns" : false,
  "sorting" : "single_column",
  "sortField" : "slaughterDate",
  "resizeMode" : "",
  "rowSpacing" : "medium",
  "rowHeight" : "small",
  "recordSelection" : "multiple_records",
  "striped" : true,
  "infiniteScroll" : false,
  "viewAs" : "list",
  "hoverStyle" : "box",
  "tableStyle" : "style_2",
  "BODY_TAG" : "<td   class=\"reportId  overflow-hidden\" tooltipActive  *ngIf=\"columns[0]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n        <span [innerHTML]=\"data[columns[0].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n    </td><td   class=\"sequenceNr  overflow-hidden\" tooltipActive  *ngIf=\"columns[1]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n        <span [innerHTML]=\"data[columns[1].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n    </td>    <td  class=\"status  overflow-hidden \" tooltipActive\n       *ngIf=\"columns[2]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n >\n<span [innerHTML]=\"data[columns[2].fieldName] | translate\" class=\"ellipsis white-space-nowrap mr-1\"></span>\n    \n    </td><td class=\"slaughterDate  overflow-hidden\" tooltipActive *ngIf=\"columns[3]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n        <span [innerHTML]=\"data[columns[3].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n    </td><td   class=\"slaughterBatchId  overflow-hidden\" tooltipActive  *ngIf=\"columns[4]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n        <span [innerHTML]=\"data[columns[4].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n    </td><td    class=\"farmer  overflow-hidden\" tooltipActive *ngIf=\"columns[5]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n        <span [innerHTML]=\"data[columns[5].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n    </td><td  class=\"farmerName  overflow-hidden\" tooltipActive *ngIf=\"columns[6]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n\n        <span [innerHTML]=\"data[columns[6].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n</td><td class=\"invoiceDate  overflow-hidden\" tooltipActive *ngIf=\"columns[7]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n        <span [innerHTML]=\"data[columns[7].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n    </td><td    class=\"fromCountry  overflow-hidden\" tooltipActive *ngIf=\"columns[8]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n        <span [innerHTML]=\"data[columns[8].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n    </td><td    class=\"toCountry  overflow-hidden\" tooltipActive *ngIf=\"columns[9]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n        <span [innerHTML]=\"data[columns[9].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n    </td>",
  "leftFreezeUptoColumn" : "0",
  "pageLimit" : "50",
  "rememberLastTableSettings" : false,
  "columnResize" : false,
  "sortOrder" : "desc",
  "showGridlines" : true,
  "groupOnColumn" : {
    "name" : "farmer",
    "sid" : "affbd293-501b-4a23-b8c2-7d2fbebfffff",
    "type" : "datamodelField"
  },
  "detailPage" : {
    "name" : "Slaughter report Detail",
    "sid" : "76401131-38b6-4f6c-b5f9-a00b7f7e7fa8",
    "url" : "/slaughterreport/slaughterreportdetail"
  },
  "detailPageNavigation" : "click_of_the_row"
}
	pageViewTitle: string = 'SLAUGHTER_REPORT_LIST_TEST_TEST_TEST';
	
		tableSearchControls : FormGroup = new FormGroup({
	slaughterDate: new FormControl('',[]),
	slaughterBatchId: new FormGroup({ min: new FormControl(null, []), max: new FormControl(null, []) }),
	fromCountry: new FormControl('',[]),
	reportId: new FormGroup({ min: new FormControl(null, []), max: new FormControl(null, []) }),
	sequenceNr: new FormGroup({ min: new FormControl(null, []), max: new FormControl(null, []) }),
	status: new FormControl('',[]),
	invoiceDate: new FormControl('',[]),
	farmerName: new FormControl('',[]),
	toCountry: new FormControl('',[]),
	farmer: new FormControl('',[]),
});

		quickFilterControls : FormGroup = new FormGroup({
});


	constructor(public slaughterReportService: SlaughterReportService, public appUtilBaseService: AppUtilBaseService, public translateService: TranslateService, public messageService: MessageService, public confirmationService: ConfirmationService, public dialogService: DialogService, public domSanitizer: DomSanitizer, public bsModalService: BsModalService, public activatedRoute: ActivatedRoute, public renderer2: Renderer2, public router: Router, public appGlobalService: AppGlobalService, public baseService: BaseService, ...args: any) {
    
 	 }

	
	clearFilterValues() {
  this.tableSearchControls.reset();
  this.filter.advancedSearch = {};
  this.onRefresh();
}
	conditionalFormatting(config: any, data: any) {
   if(config?.hasOwnProperty([data])){
    const query = config[data].query
    const initialCondition = true;
    const finalCondition = query.condition === 'and' ? '&&' : '||';
    const conditions: any[] = [];
    query.rules?.forEach((rule: any) => {
      conditions.push(this.appUtilBaseService.evaluate(data, rule.value, rule.operator));
    })
    let finalResult = conditions?.reduce((previousValue: any, currentValue: any) =>
      this.appUtilBaseService.evaluate(previousValue, currentValue, finalCondition), initialCondition);
    return finalResult;
   }
   else{
     return false;
   }
}
	deattachScroll() {
  }
	getSubHeader() {
this.subHeader = this.tableConfig.groupOnColumn?.name?.split('.');
}
	onUpdate(id: any,event?:any) {
	if (this.tableConfig.detailPage?.url) {
      const value: any = "parentId";
       let property: Exclude<keyof SlaughterReportListBaseComponent, ''> = value;
       const methodName: any = "onUpdateChild";
       let action: Exclude<keyof SlaughterReportListBaseComponent, ''> = methodName;
       if (this.isChildPage && this[property]) {
	       if (typeof this[action] === "function") {
	        	this[action](id);
	         }
       }
       else {
       	this.router.navigateByUrl(this.tableConfig.detailPage.url + '?id=' + id)
       }
    }
}
	unSelect(event:any,field:string,multiple:boolean,rowIndex?:number){
    field = field.replace('?.','_');
if(rowIndex!= undefined && rowIndex>=0)
    field = `${field}_${rowIndex}`;
 if(multiple){
    this.selectedItems[field]?.forEach((item:any,index:number)=>{
        if(item.id === event.sid){
            this.selectedItems[field].splice(index,1);
        }
    })
}else{
 this.selectedItems[field] =[];
}
  }
	filterSearch() {
    this.quickFilterControls.valueChanges.subscribe((value) => {
      let dateRangeNotChoosen: boolean = false;
      for (let control of this.quickFilterConfig.children) {
        if (control.fieldType === 'Date') {
          if (value[control.field][0] && !value[control.field][1]) {
            dateRangeNotChoosen = true;
            break;
          }
        }
      }
      if (!dateRangeNotChoosen) {
        this.filter.quickFilter = value;
       this.onRefresh();
      }
    });
  }
	formatAutoComplete(item:any,displayField:string,formControlName:string){
     return ((item && item[displayField]) ? item[displayField] : '');
  }
	advancedSearch() {
    this.filter.advancedSearch = this.tableSearchControls.value;
    let hasDates = this.tableSearchConfig.children.filter((e: any) => e.fieldType.toLowerCase() == "date" || e.fieldType.toLowerCase() == "datetime");
    if (hasDates.length > 0) {
      hasDates.forEach((f: any) => {
        let field = f.name;
        let value = this.filter.advancedSearch[field];
        if (value && Array.isArray(value)) {
          let val = { lLimit: value[0].getTime(), uLimit: value[1] ? value[1].getTime() : value[1], type: "Date" }
          this.filter.advancedSearch[field] = val;
          if (value[0] == null && value[1] == null) {
            delete this.filter.advancedSearch[field];
          }
        }
      });
    }
    let hasNumbers = this.tableSearchConfig.children.filter((e: any) => e.fieldType.toLowerCase() == "number" || e.fieldType.toLowerCase() == "double");
    if (hasNumbers.length > 0) {
      hasNumbers.forEach((f: any) => {
        let field = f.name;
        let value = this.filter.advancedSearch[field];
        if (value && !Array.isArray(value)) {
          this.filter.advancedSearch[field] = {
            lLimit: value.min, uLimit: value.max, type: "Number"
          }
          if (value.min == null && value.max == null) {
            delete this.filter.advancedSearch[field];
          }
        }
      });
    }
    this.onRefresh();
    this.toggleAdvancedSearch();
  }
	attachInfiniteScrollForAutoCompletetableSearchfromCountry(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFiredtableSearchfromCountry = false;
          if(this.filteredItemstableSearchfromCountry.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchtableSearchfromCountry`
        let action: Exclude<keyof SlaughterReportListBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	// closeAdvancedSearchPopup() {
  //   this.renderer2.listen('window', 'click', (e: Event) => {
  //     let clickedInside = this.menu?.nativeElement.contains(e.target);
  //     if(e.target !== this.toggleButton?.nativeElement&& !clickedInside &&this.showAdvancedSearch){
  //       this.showAdvancedSearch = false;
  //     }
  //   );
  // }
clearFilters(){
  this.filter.globalSearch = '';
  this.isSearchFocused = false;
}

focus(){
  this.isSearchFocused = !this.isSearchFocused;
}
	next() {
     
        this.first = this.first + this.rows;
        this.params.start = this.first;
        this.loadGridData();
      
      
    }
	showToastMessage(config: object) {
this.messageService.add(config);
}
	onSelect(event: any, field: string, config: any,index?:number) { 
    field = field.replace('?.','_');
    if(index != undefined && index >=0)
    field = `${field}_${index}`
    let lookupFields: any[] = config?.lookupFields || [];
    let model = {
      id: event.sid,
      value: {}
    }
    if (lookupFields.length > 0) {
      model.value = lookupFields?.reduce((o: any, key: any) => ({ ...o, [key]: event[key] }), {});
    }
    else {
      model.value = event;
    }
    if (!this.selectedItems?.hasOwnProperty(field)) {
      this.selectedItems[field] = [];
    }
    if (config?.multiple === true) {
      this.selectedItems[field]?.push(model);
    }
    else {
      this.selectedItems[field][0] = model;
    }
  }
	openSettings() {
    let matColumnOrder: any = this.getValueFromLocalStorage(this.localStorageStateKey) || {};
    let alreadySelectedCols = matColumnOrder['columnOrder'] || [];
    if (alreadySelectedCols.length > 0) {
      this.tableConfig.children.map((e: any) => e.checked = false);
      this.tableConfig.children.forEach((e: any) => {
        if (alreadySelectedCols.includes(e.field)) {
          e.checked = true;
        }
      });
    } else {
      this.tableConfig.children.map((e: any) => e.checked = true);
    }
    this.showMenu = true;
  }
	formatFilteredData(data:any,fieldName:string){
    return data;
  }
	setHeight() {
    setTimeout(() => {
      const el = (<HTMLInputElement>document.getElementById("table-container")).getBoundingClientRect();
      let paginatorHeight: number = 0;
      if (this.showPaginationOnBottom) {
        const paginator = (<HTMLInputElement>document.getElementById("paginator-bottom")).getBoundingClientRect();
        paginatorHeight = paginator.height;
      }
      const top = (el.top + paginatorHeight) + 'px';
      (<HTMLInputElement>document.getElementById('table-container')).style.setProperty('height', 'calc(100vh - ' + top + ')');
    }, 100);

}
	isFirstPage(): boolean {
      return this.gridData ? this.first === 0 : true;
    }
	saveResizeColumns(event: any) {
    setTimeout(() => {
      this.selectedColumns.forEach((e: any) => {
        if (e.data == event.element.innerText) {
          e.width = event.element.offsetWidth + 'px';
          localStorage.setItem(this.localStorageStateKey, JSON.stringify(this.selectedColumns));
        }
      });
    }, 10);
  }
	onRowSelect(event?: any) {
    if (event?.originalEvent) {
      event?.originalEvent.stopPropagation();
    }
    if (this.selectedValues.length > 0) {
      this.isRowSelected = true;
    }
    else if (this.selectedValues.length <= 0) {
      this.isRowSelected = false;
    }
  }
	updateActions(){
      this.actionBarConfig = this.appUtilBaseService.getActionsConfig(this.leftActionBarConfig.children);
      this.actionBarConfig.forEach((actionConfig:any)=>{
        if(actionConfig.visibility === 'conditional' && actionConfig.conditionForButtonVisiblity){
          const conResult = this.appUtilBaseService.evaluvateCondition(actionConfig.conditionForButtonVisiblity?.query?.rules, actionConfig.conditionForButtonVisiblity?.query?.condition);
          this.validateActions(actionConfig.action,conResult,'view');
        }
        if(actionConfig.buttonEnabled === 'conditional' && actionConfig.conditionForButtonEnable){
          const conResult = this.appUtilBaseService.evaluvateCondition(actionConfig.conditionForButtonEnable?.query?.rules, actionConfig.conditionForButtonEnable?.query?.condition);
          this.validateActions(actionConfig.action,conResult,'edit');
        }
      })
    }
	validateActions(label: string, result: boolean, action: string) {
      if (action === 'view') {
        if (result && this.conditionalActions.hideActions.includes(label))
          this.conditionalActions.hideActions?.splice(this.conditionalActions.hideActions?.indexOf(label), 1)
        else if (!result && !this.conditionalActions.hideActions.includes(label))
          this.conditionalActions.hideActions.push(label);
      }
      else if (action === 'edit') {
        if (result && this.conditionalActions.disableActions.includes(label))
          this.conditionalActions.disableActions.splice(this.conditionalActions.disableActions?.indexOf(label), 1);
        else if (!result && !this.conditionalActions.disableActions.includes(label))
          this.conditionalActions.disableActions.push(label);
      }
    }
	initFilterForm(){
    this.quickFilterFieldConfig= this.appUtilBaseService.getControlsFromFormConfig(this.quickFilterConfig);
    this.filterSearch();
}
	loadGridData() {
 this.isPageLoading = true;
  let gridSubscription: any;
  if (environment.prototype) {
  	gridSubscription = this.slaughterReportService.getProtoTypingData().subscribe((data: any) => {
  		this.tableConfig.children.map((o: any) => {
        data.map((d: any) => {
          if (['date', 'datetime', 'curreny'].includes(o.uiType)) {
            d[o.name] = this.onFormatColumns(o, d); 
          }
        })
      })
      this.gridData = [...this.gridData, ...data];
  	  this.isPageLoading = false;
  	});
  } else {
	  const params = this.assignTableParams();
	  	const value:any = "parentId";
	  	let property: Exclude<keyof SlaughterReportListBaseComponent, ''> = value;
	  	const method:any = "getChildTableData";
	  	let action: Exclude<keyof SlaughterReportListBaseComponent, ''> = method;
	  	if(this.isChildPage && typeof this[action] === "function"){
        if (this[property]) {
          params.pid = this[property];
          
          this[action](params);
        }
        else {
          this.isPageLoading = false;
        }
	  	}
	  	else{
	  	
		  	this.getTableData(params);
	}
	}
}

  getTableData(params: any) {
    	const gridSubscription =  this.slaughterReportService.getDatatableData(params).subscribe((data: any) => {
      if (this.first >= this.total || this.first === 0) {
        this.tableConfig.children.map((o: any) => {
          data?.results.map((d: any) => {
            if (['date', 'datetime', 'curreny', 'autosuggest'].includes(o.uiType)) {
              d[o.name] = this.onFormatColumns(o, d); 
            }
          })
        })
        let updateRecords: SlaughterReportBase[] = [...this.updatedRecords, ...data?.results];
        const ids = updateRecords.map(o => o.sid);
        this.updatedRecords = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
      }
      this.gridData = this.updatedRecords.slice(this.first, (this.first +this.rows));
      this.denormalize(this.gridData);
      this.total = data?.filtered ? data?.filtered : 0;
      this.isPageLoading = false;
    }, (err: any) => {
      this.isPageLoading = false;
    });
    this.subscriptions.push(gridSubscription);
  }
	onFormatMultipleValues(col: any, data: any): any {
    const arr: any = []
        if(col.uiType === 'link') {
      if(data) {
        for(var i=0;i<data.length;i++) {
          const url = "<a href="+data[i]+">Link"+(i+1)+"</a>"
          arr.push(url);
        }
        return arr.join(', ')
      }
    }
    const displayField = col.displayField ? col.displayField : '';
  if (col.uiType == 'autosuggest' && Array.isArray(data)) {
      data?.forEach((k: any) => {
        arr.push(k.value[displayField]);
      })
    }
    else if (Array.isArray(data)) {
      data.forEach(function (e: any) {
        if (displayField)
          arr.push(e[displayField])
        else
          arr.push(e);
      })
    }
    else if (typeof data === 'object') {
      if (displayField) {
        arr.push(data[displayField]);
      }
    }
    else {
      arr.push(data);
    }
    return (arr.toString());
  }
	disablechildAction() {
    const value: any = "parentId";
    let property: Exclude<keyof SlaughterReportListBaseComponent, ' '> = value;
    this.leftActionBarConfig?.children?.map((ele:any)=>{
      if(ele.type === 'buttonGroup'){
        ele?.children.map((childEle:any,index:number)=>{
          if (childEle?.action === 'new' && !this[property] && this.isChildPage && childEle.buttonEnabled !='conditional') {
              childEle.buttonEnabled ='no';
            }
          else if(childEle.action === 'new' && this[property] && this.isChildPage && childEle.buttonEnabled !='conditional'){
            childEle.buttonEnabled ='yes';
          }
        })
      }
    })
  }
	autoSuggestSearchtableSearchfarmer(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFiredtableSearchfarmer){
      this.isAutoSuggestCallFiredtableSearchfarmer = true;
    let apiObj = Object.assign({}, CompaniesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.tableSearchControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFiredtableSearchfarmer = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemstableSearchfarmer, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemstableSearchfarmer = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemstableSearchfarmer = this.formatFilteredData(this.filteredItemstableSearchfarmer, 'farmer');
    }, (err: any) => {
      this.isAutoSuggestCallFiredtableSearchfarmer = false;
    })
this.subscriptions.push(sub);}
 }
	clearGlobalSearch(){
  this.filter.globalSearch = '';
  this.onRefresh();
}
	onRefresh(){
this.first = 0;
this.gridData = [];
this.updatedRecords =[];
this.params.start =0;
this.loadGridData();
this.updateActions();
this.selectedValues=[];
this.onRowSelect();
}
	getDisabled(formControl: FormGroup, ele: string) {
  const parent = ele.split('?.')[0];
  if (formControl.controls[parent] instanceof FormGroup){
    return formControl.get(ele)?.disabled
  }
  else
    return formControl.controls[parent].disabled;
}
	setAutoSuggestValue(data:any,col:any){
    const arr: any = [];
    const displayField = col.displayField ? col.displayField : '';
    if(data && Array.isArray(data)){
      data?.forEach((k: any) => {
        arr.push(k.value[displayField]);
      })
    }
    else if(data?.value){
      arr.push(data.value[displayField]);
    }
    else{
      arr.push(data);
    }
    return arr.join();
  }
	getSelectedObject(field:string,options:any){
      const selectedObj = (options.filter((item: { label: any}) => (item.label)?.toUpperCase() === field?.toUpperCase()));
      return selectedObj[0];
  }
	clearAllFilters() {
  this.filter.globalSearch = '';
  this.clearFilterValues();
}
	autoSuggestSearchtableSearchfromCountry(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFiredtableSearchfromCountry){
      this.isAutoSuggestCallFiredtableSearchfromCountry = true;
    let apiObj = Object.assign({}, CountriesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.tableSearchControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFiredtableSearchfromCountry = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemstableSearchfromCountry, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemstableSearchfromCountry = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemstableSearchfromCountry = this.formatFilteredData(this.filteredItemstableSearchfromCountry, 'fromCountry');
    }, (err: any) => {
      this.isAutoSuggestCallFiredtableSearchfromCountry = false;
    })
this.subscriptions.push(sub);}
 }
	saveColumns() {
    let columns = document.querySelectorAll('.tbl-ctx-menu input:checked')
    let columnsToShow: string[] = [];
    columns.forEach((e: any) => {
      columnsToShow.push(e.value);
    });
    let matColumnOrder: any = this.getValueFromLocalStorage(this.localStorageStateKey) || {};
    matColumnOrder['columnOrder'] = columnsToShow;
    localStorage.setItem(this.localStorageStateKey, JSON.stringify(matColumnOrder));
    let cols = this.tableConfig.children;
    cols = cols.filter((e: any) => columnsToShow.includes(e.field));
    if (matColumnOrder.columnOrder) {
      let sortingArr = matColumnOrder.columnOrder || [];
      cols.sort(function (a: any, b: any) {
        return sortingArr.indexOf(a) - sortingArr.indexOf(b);
      });
    }
    this.selectedColumns = cols;
    this.showMenu = false;
  }
	attachInfiniteScroll() {
const tracker = (<HTMLInputElement>document.getElementsByClassName('p-datatable-wrapper')[0])
let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
  return Math.round(tracker.scrollTop);
}));

const scrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
  if(this.scrollTop != scrollPos){
        this.scrollTop = scrollPos;
    if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
      this.params.start = this.total;
     this.loadGridData();
   }
  }
});
this.subscriptions.push(scrollSubscription);
}
	clearColumnReorder() {
    setTimeout(() => {
      this.selectedColumns = this.tableConfig.children;
      localStorage.removeItem(this.localStorageStateKey);
      this.showMenu = false;
    }, 10);
  }
	getValue(formControl: FormGroup, ele: string) {
    const parent = ele.split('?.')[0];
    if (formControl.controls[parent] instanceof FormGroup){
      const child = ele.split('?.')[1];
      return formControl.controls[parent].value[child];
    }
    else
      return formControl.controls[parent].value;
  }
	prev() {
      if(!this.isFirstPage()){
        this.first = this.first - this.rows;
        this.params.start = this.first;
        if(this.first === 0){
          this.gridData = [];
          this.updatedRecords =[];
        }
        this.loadGridData();
      }
    }
	initSearchForm(){
  this.tableSearchFieldConfig= this.appUtilBaseService.getControlsFromFormConfig(this.tableSearchConfig)
}
	checkLocalStorage() {
    if (localStorage.getItem(this.localStorageStateKey)) {
      try {
        let columnsToShow = JSON.parse(localStorage.getItem(this.localStorageStateKey) || '{}');
        let cols = this.tableConfig.children;
        if (columnsToShow.columnOrder) {
          cols = cols.filter((e: any) => columnsToShow.columnOrder.includes(e.field));
          let sortingArr = columnsToShow.columnOrder;
          cols.sort(function (a: any, b: any) {
            return sortingArr.indexOf(a) - sortingArr.indexOf(b);
          });
        }
        this.selectedColumns = cols;
      }
      catch (e) {
        this.selectedColumns = this.tableConfig.children;
      }
    } else {
      this.selectedColumns = this.tableConfig.children;
    }
  }
	getSearchData(searchFields: any, config: any) {
    let searchData: any = {}
    for (const key in searchFields) {
      if (searchFields.hasOwnProperty(key) && searchFields[key]?.toString().length) {
       if (this.selectedItems.hasOwnProperty(key)) {
          let lookupObj: any = [];
          if (config[key].multiple) {
            this.selectedItems[key].map((o: any) => lookupObj.push(o.id));
          }
          searchData[`${key}__id`] = config[key].multiple ? lookupObj : this.selectedItems[key][0].id;
        }
        else {
          searchData[key] = searchFields[key];
        }
      }
    }
    return searchData;
  }
	setLocalStorageKey(){
  const currentUserData = this.appGlobalService.get('currentUser') && this.appGlobalService.get('currentUser')[0];
  const userId = (environment.prototype)? '':currentUserData?.sid;
  this.localStorageStateKey = `${this.localStorageStateKey}${userId}`
}
	actionBarAction(btn: any) {
    const methodName: any = (`on` + btn.action.charAt(0).toUpperCase() + btn.action.slice(1));
    let action: Exclude<keyof SlaughterReportListBaseComponent, ' '> = methodName;
    if (btn.action === 'navigate_to_page' && btn.pageName?.url) {
      this.router.navigateByUrl(btn.pageName.url);
    }
    else if (typeof this[action] === "function") {
      this[action]();
    }
  }
	saveReorderedColumns(event: any) {
    let columnOrder = event.columns.map((e: any) => e.field);
    let matColumnOrder: any = this.getValueFromLocalStorage(this.localStorageStateKey) || {};
    matColumnOrder['columnOrder'] = columnOrder;
    localStorage.setItem(this.localStorageStateKey, JSON.stringify(matColumnOrder));
    this.selectedColumns = JSON.parse(JSON.stringify(event.columns));
  }
	onKeydown(event: any) {
  if (event.which === 13 || event.keyCode === 13) {
    // this.filter.globalSearch = this.globalSearch
   this.onRefresh();
  }
}
	sort(e: any, field: string) {
this.filter.sortField = field;
this.filter.sortOrder = (e.currentTarget.childNodes[1].childNodes[0].classList.contains('pi-sort-amount-up-alt')) ? 'desc' : 'asc';
this.onRefresh();
}
	onNew() {
	const value: any = "parentId";
	let property: Exclude<keyof SlaughterReportListBaseComponent, ''> = value;
	if (this.isChildPage && this[property]) {
		const methodName: any = "onNewChild";
		let action: Exclude<keyof SlaughterReportListBaseComponent, ''> = methodName;
		if (typeof this[action] == "function") {
			this[action]();
		}
	}
	else {
		this.router.navigate(['../slaughterreportdetail'], { relativeTo: this.activatedRoute});
	}
}
	onFormatColumns(col: any, datum: any) {
    const type = col.uiType;
    let data = datum[col.name];
    let formattedValue: any;
    switch (type) {
      case 'date':
        formattedValue = this.appUtilBaseService.formatDate(data, col.format ? col.format : null);
        break;

      case 'datetime':
        formattedValue = this.appUtilBaseService.formatDateTime(data, col.format ? col.format : null)
        break;

      case 'currency':
       const ccode = col.currencySymbol ? col.currencySymbol : null;
        const cDigits = col.currencyDigits ? col.currencyDigits : null;
        formattedValue = this.appUtilBaseService.formatCurrency(data, ccode, cDigits);
        break;  
      case 'autosuggest':
        formattedValue = this.setAutoSuggestValue(data,col);
        break;
      default:
        formattedValue = data;
    }
    return (formattedValue);
  }
	toggleAdvancedSearch() {
  this.showAdvancedSearch = !this.showAdvancedSearch;
}
	getClass(){
    const styleClass = (this.isMobile && this.tableConfig.viewAs ==='list') ? 'table-body-md':'table-body';
    return styleClass;
  }
	denormalize(gridData: any) {
}
	cancelColumnOptions() {
    this.showMenu = false;
  }
	attachInfiniteScrollForAutoCompletetableSearchtoCountry(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFiredtableSearchtoCountry = false;
          if(this.filteredItemstableSearchtoCountry.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchtableSearchtoCountry`
        let action: Exclude<keyof SlaughterReportListBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	attachInfiniteScrollForAutoCompletetableSearchfarmer(fieldName:string) {
    const tracker = (<HTMLInputElement>document.getElementsByClassName('p-autocomplete-panel')[0])
    let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
      return Math.round(tracker.scrollTop);
    }));

    const autoSuggestScrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
      if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
        this.isAutoSuggestCallFiredtableSearchfarmer = false;
          if(this.filteredItemstableSearchfarmer.length  >= this.autoSuggestPageNo * AppConstants.defaultPageSize){
            this.autoSuggestPageNo = this.autoSuggestPageNo + 1;
          }
         const methodName: any = `autoSuggestSearchtableSearchfarmer`
        let action: Exclude<keyof SlaughterReportListBaseComponent, ' '> = methodName;
        this[action]();
      }
    });
    this.subscriptions.push(autoSuggestScrollSubscription);
  }
	autoSuggestSearchtableSearchtoCountry(event?: any, col?: any,url?:any) {
if(!this.isAutoSuggestCallFiredtableSearchtoCountry){
      this.isAutoSuggestCallFiredtableSearchtoCountry = true;
    let apiObj = Object.assign({}, CountriesApiConstants.autoSuggestService);
     const urlObj = {
        url: (url || apiObj.url),
        searchText: event ? event.query: '' ,
        colConfig: col,
        value: this.tableSearchControls.getRawValue(),
        pageNo:this.autoSuggestPageNo
      }
    apiObj.url = this.appUtilBaseService.generateDynamicQueryParams(urlObj);
   const sub =  this.baseService.get(apiObj).subscribe((res: any) => {
      this.isAutoSuggestCallFiredtableSearchtoCountry = false;
          let updateRecords = [];
          if(event && event.query) {
                updateRecords =  [...res];
          } else {
                updateRecords =  [...this.filteredItemstableSearchtoCountry, ...res];
          }
      const ids = updateRecords.map(o => o.sid)
      this.filteredItemstableSearchtoCountry = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
          this.filteredItemstableSearchtoCountry = this.formatFilteredData(this.filteredItemstableSearchtoCountry, 'toCountry');
    }, (err: any) => {
      this.isAutoSuggestCallFiredtableSearchtoCountry = false;
    })
this.subscriptions.push(sub);}
 }
	onDelete() {
  if (this.selectedValues.length > 0) {
    let values: any = [];
	this.selectedValues.forEach((field: any) => {
		values.push(field.sid)
	});
	let requestedParams:any = {ids:values.toString()}
      this.confirmationService.confirm({
        message: this.translateService.instant('DELETE_CONFIRMATION_MESSAGE'),
        header: 'Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
          const deleteSubscription = this.slaughterReportService.delete(requestedParams).subscribe((res: any) => {
            this.showToastMessage({severity:'success', summary:'', detail:this.translateService.instant('RECORDS_DELETED_SUCCESSFULLY')});
            requestedParams = {};
            this.selectedValues = [];
            values?.map((m:any)=>{
              const index = this.updatedRecords?.findIndex(o=>o.sid === m);
              if(index > -1){
              this.updatedRecords?.splice(index,1);
              }
            })
            this.loadGridData();
            
          });
           this.subscriptions.push(deleteSubscription);
        },
        reject: () => {
          //rejected
        },
      });
    }

  }
	calculateFormula(){
	
}
	assignTableParams(){
  const params = this.params;
  this.filter.sortField = this.tableConfig.groupOnColumn ? this.tableConfig.groupOnColumn?.name : this.filter.sortField;
  const searchData = { ...this.getSearchData(this.filter.advancedSearch, this.tableSearchFieldConfig), ...this.getSearchData(this.filter.quickFilter, this.quickFilterFieldConfig) }
  if (this.filter.globalSearch)
    searchData['_global'] = this.filter.globalSearch;

  if (this.filter.sortField && this.filter.sortOrder) {
    let isFieldTypeAutoSuggest: boolean = false;
    this.tableConfig.children.map((ele: any) => {
      if (ele.uiType === "autosuggest" && this.filter.sortField === ele.name) {
        isFieldTypeAutoSuggest = true;
      }
      const columnName = isFieldTypeAutoSuggest ? (ele.name + "__value__" + ele.displayField) : this.filter.sortField;
      params.order = [{
        column: columnName,
        dir: this.filter.sortOrder
      }]
    })
  }
  else {
    params.order = null;
  }
  params.search = searchData;

  return params;
}
	getValueFromLocalStorage(key: string) {
    let val = localStorage.getItem(key);
    if (val != null) {
      return JSON.parse(val);
    } else {
      return null;
    }
  }

    onInit() {
		
		this.initSearchForm();

		this.initFilterForm();
		this.setLocalStorageKey()
this.tableConfig.children = this.appUtilBaseService.formatTableConfig(this.tableConfig.children);
 this.tableFieldConfig = this.appUtilBaseService.formatTableFieldConfig(this.tableConfig.children)
this.params = this.appUtilBaseService.getTableRequestParams(this.tableConfig);
this.rows = this.params.length;
this.loadGridData();
this. checkLocalStorage();
this.getSubHeader();
this.rightFreezeColums = (this.tableConfig.children.length - this.tableConfig.rightFreezeFromColumn);
 this.disablechildAction();
this.updateActions();
    }
	
     onDestroy() {
		
		    this.subscriptions.forEach((subs: { unsubscribe: () => void; }) => subs.unsubscribe());
  
    }
     onAfterViewInit() {
		
		
 this.setHeight();
//  setTimeout(() => {
  //     this.attachInfiniteScroll();
  //   }, 2000);
 
  

    }

}

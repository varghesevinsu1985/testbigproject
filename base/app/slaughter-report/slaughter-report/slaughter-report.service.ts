import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BaseService } from '@baseapp/base.service';
import { SlaughterReportBase} from './slaughter-report.base.model';
import { SlaughterReportApiConstants } from './slaughter-report.api-constants';


@Injectable({
  providedIn: 'root'
})
export class SlaughterReportService {

  constructor(
      public baseService:BaseService
  ) { }
  
	  getProtoTypingData(): Observable<any> {
	      const subject:Observable<SlaughterReportBase> = new Observable(observer => {
	        const data =  require('base/assets/sample-data/slaughter-report.json');
	        observer.next(data as SlaughterReportBase);
	      });
	      return subject;
	  }
	 
	 getProtoTypingDataById(...args:any): Observable<any> {
		 const params= args[0];
		 const subject:Observable<SlaughterReportBase> = new Observable(observer => {
			 const response = require('base/assets/sample-data/slaughter-report.json');
			 const data = response.find((x: { sid: string; }) => x.sid === params.sid);
			 observer.next(data as SlaughterReportBase);
		 });
		 return subject;
	}

    slaughterReportsWorkflowTest(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowTest;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    slaughterReportsWorkflowApprove(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowApprove;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    slaughterReportsWorkflowPricingDone(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowPricingDone;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    getById(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.getById;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.get(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    slaughterReportsWorkflowToSlaughterDetails(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowToSlaughterDetails;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    slaughterReportsWorkflowCancelBatch(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowCancelBatch;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    update(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.update;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    getDatatableData(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.getDatatableData;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.post(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    autoSuggestService(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.autoSuggestService;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.get(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    slaughterReportsWorkflowNoaction(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowNoaction;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    create(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.create;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.post(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    slaughterReportsWorkflowBackToSlaughterDetails(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowBackToSlaughterDetails;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    slaughterReportsWorkflowNotApproved(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowNotApproved;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    delete(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.delete;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.delete(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    slaughterReportsWorkflowBackToSupplyAndFlockData(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowBackToSupplyAndFlockData;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
    slaughterReportsWorkflowToPurchasePricing(...args: any):Observable<any>{
        const serviceOpts = SlaughterReportApiConstants.slaughterReportsWorkflowToPurchasePricing;
        const params= args[0];
        
        const subject = new Observable(observer => {
          this.baseService.put(serviceOpts,params).subscribe((response: any) => {
            observer.next(response);
          },
          (err:any) => {
            observer.error(err);
          });
        });
    
        return subject;
    }
}

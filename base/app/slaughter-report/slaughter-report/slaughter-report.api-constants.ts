import { SlaughterReportBase} from '@baseapp/slaughter-report/slaughter-report/slaughter-report.base.model';

export class SlaughterReportApiConstants {
    public static readonly slaughterReportsWorkflowTest: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/test/{id}',
        method: 'PUT',
        showloading: false
    };
    public static readonly slaughterReportsWorkflowApprove: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/approve/{id}',
        method: 'PUT',
        showloading: false
    };
    public static readonly slaughterReportsWorkflowPricingDone: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/pricingdone/{id}',
        method: 'PUT',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/slaughterreports/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly slaughterReportsWorkflowToSlaughterDetails: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/toslaughterdetails/{id}',
        method: 'PUT',
        showloading: false
    };
    public static readonly slaughterReportsWorkflowCancelBatch: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/cancelbatch/{id}',
        method: 'PUT',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/slaughterreports/',
        method: 'PUT',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/slaughterreports/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/slaughterreports/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly slaughterReportsWorkflowNoaction: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/noaction/{id}',
        method: 'PUT',
        showloading: false
    };
    public static readonly create: any = {
        url: '/rest/slaughterreports/',
        method: 'POST',
        showloading: false
    };
    public static readonly slaughterReportsWorkflowBackToSlaughterDetails: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/backtoslaughterdetails/{id}',
        method: 'PUT',
        showloading: false
    };
    public static readonly slaughterReportsWorkflowNotApproved: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/notapproved/{id}',
        method: 'PUT',
        showloading: false
    };
    public static readonly delete: any = {
        url: '/rest/slaughterreports/{ids}',
        method: 'DELETE',
        showloading: false
    };
    public static readonly slaughterReportsWorkflowBackToSupplyAndFlockData: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/backtosupplyandflockdata/{id}',
        method: 'PUT',
        showloading: false
    };
    public static readonly slaughterReportsWorkflowToPurchasePricing: any = {
        url: '/rest/slaughterreports/slaughterreportsworkflow/topurchasepricing/{id}',
        method: 'PUT',
        showloading: false
    };
}
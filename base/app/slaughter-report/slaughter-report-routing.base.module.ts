import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from '@baseapp/auth.can-deactivate-guard.service';

import { SlaughterReportDetailComponent } from '@app/slaughter-report/slaughter-report/slaughter-report-detail/slaughter-report-detail.component';
import { SlaughterReportListComponent } from '@app/slaughter-report/slaughter-report/slaughter-report-list/slaughter-report-list.component';
import { DashboardComponent } from '@app/slaughter-report/slaughter-report/dashboard/dashboard.component';

export const routes: Routes = [

{
     path: 'slaughterreportdetail',
     component: SlaughterReportDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "SLAUGHTER_REPORT_DETAIL",
        breadcrumb: "SLAUGHTER_REPORT_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'slaughterreportlist',
     component: SlaughterReportListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "SLAUGHTER_REPORT_LIST",
        breadcrumb: "SLAUGHTER_REPORT_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'dashboard',
     component: DashboardComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "DASHBOARD",
        breadcrumb: "DASHBOARD",
        roles : [
        			"all"
				]
     }
}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class SlaughterReportBaseRoutingModule
{
}

import { FarmerCertificatesBase} from '@baseapp/master-data-tables/farmer-data/farmer-certificates/farmer-certificates.base.model';

export class FarmerCertificatesApiConstants {
    public static readonly getById: any = {
        url: '/rest/farmercertificates/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly getFarmerCertificatesByPId: any = {
        url: '/rest/farmercertificates/bypid/{pid}',
        method: 'GET',
        showloading: false
    };
    public static readonly delete: any = {
        url: '/rest/farmercertificates/{ids}',
        method: 'DELETE',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/farmercertificates/',
        method: 'PUT',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/farmercertificates/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly create: any = {
        url: '/rest/farmercertificates/',
        method: 'POST',
        showloading: false
    };
    public static readonly getDatatableDataByPId: any = {
        url: '/rest/farmercertificates/datatable/{pid}',
        method: 'POST',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/farmercertificates/datatable',
        method: 'POST',
        showloading: false
    };
}
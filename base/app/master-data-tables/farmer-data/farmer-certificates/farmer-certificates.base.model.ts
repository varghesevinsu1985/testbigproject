export interface FarmerCertificatesBase {
	createdDate: Date;
	sid: string;
	certificateName: string;
	modifiedBy: string;
	relatedStable: any;
	createdBy: string;
	validFrom: Date;
	relatedCompany: any;
	certificateId: any;
	validTo: Date;
	modifiedDate: Date;
	relatedCompanyName: string;
}
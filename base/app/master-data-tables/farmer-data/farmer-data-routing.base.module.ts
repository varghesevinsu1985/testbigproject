import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from '@baseapp/auth.can-deactivate-guard.service';

import { FarmerCertificatesDetailComponent } from '@app/master-data-tables/farmer-data/farmer-certificates/farmer-certificates-detail/farmer-certificates-detail.component';
import { FarmerCertificatesListComponent } from '@app/master-data-tables/farmer-data/farmer-certificates/farmer-certificates-list/farmer-certificates-list.component';
import { StablesDetailComponent } from '@app/master-data-tables/farmer-data/stables/stables-detail/stables-detail.component';
import { StablesListComponent } from '@app/master-data-tables/farmer-data/stables/stables-list/stables-list.component';
import { RelationsListComponent } from '@app/master-data-tables/farmer-data/relations/relations-list/relations-list.component';
import { RelationsDetailComponent } from '@app/master-data-tables/farmer-data/relations/relations-detail/relations-detail.component';
import { CompaniesListComponent } from '@app/master-data-tables/farmer-data/companies/companies-list/companies-list.component';
import { CompaniesDetailComponent } from '@app/master-data-tables/farmer-data/companies/companies-detail/companies-detail.component';

export const routes: Routes = [

{
     path: 'farmercertificatesdetail',
     component: FarmerCertificatesDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "FARMER_CERTIFICATES_DETAIL",
        breadcrumb: "FARMER_CERTIFICATES_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'farmercertificateslist',
     component: FarmerCertificatesListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "FARMER_CERTIFICATES_LIST",
        breadcrumb: "FARMER_CERTIFICATES_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'stablesdetail',
     component: StablesDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "STABLES_DETAIL",
        breadcrumb: "STABLES_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'stableslist',
     component: StablesListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "STABLES_LIST",
        breadcrumb: "STABLES_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'relationslist',
     component: RelationsListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "RELATIONS_LIST",
        breadcrumb: "RELATIONS_LIST",
        roles : [
        			"Development Administrator"
				]
     }
},
{
     path: 'relationsdetail',
     component: RelationsDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "RELATIONS_DETAIL",
        breadcrumb: "RELATIONS_DETAIL",
        roles : [
        			"Development Administrator"
				]
     }
},
{
     path: 'companieslist',
     component: CompaniesListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "COMPANIES_LIST",
        breadcrumb: "COMPANIES_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'companiesdetail',
     component: CompaniesDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "COMPANIES_DETAIL",
        breadcrumb: "COMPANIES_DETAIL",
        roles : [
        			"all"
				]
     }
}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class FarmerDataBaseRoutingModule
{
}

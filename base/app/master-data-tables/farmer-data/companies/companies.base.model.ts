export interface CompaniesBase {
	relationType: any;
	city: string;
	modifiedBy: string;
	sid: string;
	name: string;
	administration: any;
	createdBy: string;
	locidValue: string;
	address: string;
	companyCode: string;
	locid: string;
	modifiedDate: Date;
	createdDate: Date;
	postalCode: string;
	standardPurchaseNotation: any;
}
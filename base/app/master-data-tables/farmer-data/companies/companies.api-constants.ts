import { CompaniesBase} from '@baseapp/master-data-tables/farmer-data/companies/companies.base.model';

export class CompaniesApiConstants {
    public static readonly create: any = {
        url: '/rest/companies/',
        method: 'POST',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/companies/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly getByCompanyCode: any = {
        url: '/rest/companies/bycompanycode/{companycode}',
        method: 'GET',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/companies/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/companies/',
        method: 'PUT',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/companies/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly delete: any = {
        url: '/rest/companies/{ids}',
        method: 'DELETE',
        showloading: false
    };
}
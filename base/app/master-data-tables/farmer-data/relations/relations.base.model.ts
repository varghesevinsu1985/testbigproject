export interface RelationsBase {
	relatedCompanyFromId: any;
	sid: string;
	modifiedBy: string;
	relationType: any;
	stable: any;
	relatedCompanyFrom: string;
	createdDate: Date;
	relatedCompanyToId: any;
	modifiedDate: Date;
	createdBy: string;
	relatedCompanyTo: string;
}
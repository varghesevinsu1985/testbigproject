import { RelationsBase} from '@baseapp/master-data-tables/farmer-data/relations/relations.base.model';

export class RelationsApiConstants {
    public static readonly update: any = {
        url: '/rest/relations/',
        method: 'PUT',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/relations/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/relations/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly delete: any = {
        url: '/rest/relations/{ids}',
        method: 'DELETE',
        showloading: false
    };
    public static readonly getDatatableDataByPId: any = {
        url: '/rest/relations/datatable/{pid}',
        method: 'POST',
        showloading: false
    };
    public static readonly create: any = {
        url: '/rest/relations/',
        method: 'POST',
        showloading: false
    };
    public static readonly getRelationsByPId: any = {
        url: '/rest/relations/bypid/{pid}',
        method: 'GET',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/relations/{sid}',
        method: 'GET',
        showloading: false
    };
}
import { StablesBase} from '@baseapp/master-data-tables/farmer-data/stables/stables.base.model';

export class StablesApiConstants {
    public static readonly delete: any = {
        url: '/rest/stables/{ids}',
        method: 'DELETE',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/stables/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly getByStableId: any = {
        url: '/rest/stables/bystableid/{stableid}',
        method: 'GET',
        showloading: false
    };
    public static readonly getDatatableDataByPId: any = {
        url: '/rest/stables/datatable/{pid}',
        method: 'POST',
        showloading: false
    };
    public static readonly create: any = {
        url: '/rest/stables/',
        method: 'POST',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/stables/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly getStablesByPId: any = {
        url: '/rest/stables/bypid/{pid}',
        method: 'GET',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/stables/',
        method: 'PUT',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/stables/datatable',
        method: 'POST',
        showloading: false
    };
}
export interface StablesBase {
	sid: string;
	modifiedDate: Date;
	stableM2: number;
	createdDate: Date;
	relatedCompanyId: any;
	createdBy: string;
	relatedCompanyName: string;
	stableId: string;
	modifiedBy: string;
}
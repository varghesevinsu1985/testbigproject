import { FarmerCertificatesListComponent } from '@app/master-data-tables/farmer-data/farmer-certificates/farmer-certificates-list/farmer-certificates-list.component';
import { BackgroundListsModule } from '@app/master-data-tables/background-lists/background-lists.module';
import { RelationsListComponent } from '@app/master-data-tables/farmer-data/relations/relations-list/relations-list.component';
import { WidgetsBaseModule } from '@baseapp/widgets/widgets.base.module';
import { CompaniesDetailComponent } from '@app/master-data-tables/farmer-data/companies/companies-detail/companies-detail.component';
import { RelationsPipe } from '@app/master-data-tables/farmer-data/relations/relations.pipe.component';
import { StablesPipe } from '@app/master-data-tables/farmer-data/stables/stables.pipe.component';
import { CompaniesListComponent } from '@app/master-data-tables/farmer-data/companies/companies-list/companies-list.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { CompaniesPipe } from '@app/master-data-tables/farmer-data/companies/companies.pipe.component';
import { StablesListComponent } from '@app/master-data-tables/farmer-data/stables/stables-list/stables-list.component';
import { FarmerCertificatesDetailComponent } from '@app/master-data-tables/farmer-data/farmer-certificates/farmer-certificates-detail/farmer-certificates-detail.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { FarmerCertificatesPipe } from '@app/master-data-tables/farmer-data/farmer-certificates/farmer-certificates.pipe.component';
import { CanDeactivateGuard } from '@baseapp/auth.can-deactivate-guard.service';
import { RelationsDetailComponent } from '@app/master-data-tables/farmer-data/relations/relations-detail/relations-detail.component';
import { StablesDetailComponent } from '@app/master-data-tables/farmer-data/stables/stables-detail/stables-detail.component';

@NgModule({
  declarations: [
FarmerCertificatesDetailComponent,
FarmerCertificatesPipe,
FarmerCertificatesListComponent,
StablesDetailComponent,
StablesPipe,
StablesListComponent,
RelationsListComponent,
RelationsPipe,
RelationsDetailComponent,
CompaniesListComponent,
CompaniesPipe,
CompaniesDetailComponent
],
imports: [
SharedModule,
WidgetsBaseModule,
BackgroundListsModule
],

exports: [
SharedModule,
WidgetsBaseModule,
FarmerCertificatesDetailComponent,
FarmerCertificatesListComponent,
StablesDetailComponent,
StablesListComponent,
RelationsListComponent,
RelationsDetailComponent,
CompaniesListComponent,
CompaniesDetailComponent,
BackgroundListsModule
],

providers: [
BsModalService,
CanDeactivateGuard
],
  
})
export class FarmerDataBaseModule { }
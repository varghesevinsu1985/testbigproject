export interface RelationTypeBase {
	createdDate: Date;
	relationTypeCode: string;
	modifiedBy: string;
	relationTypeName: string;
	modifiedDate: Date;
	sid: string;
	createdBy: string;
}
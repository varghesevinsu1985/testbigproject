import { RelationTypeBase} from '@baseapp/master-data-tables/background-lists/relation-type/relation-type.base.model';

export class RelationTypeApiConstants {
    public static readonly create: any = {
        url: '/rest/relationtypes/',
        method: 'POST',
        showloading: false
    };
    public static readonly getByRelationTypeCode: any = {
        url: '/rest/relationtypes/byrelationtypecode/{relationtypecode}',
        method: 'GET',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/relationtypes/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/relationtypes/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly delete: any = {
        url: '/rest/relationtypes/{ids}',
        method: 'DELETE',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/relationtypes/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/relationtypes/',
        method: 'PUT',
        showloading: false
    };
}
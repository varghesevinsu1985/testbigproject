import { BreedService } from '../breed.service';
import { BreedBase} from '../breed.base.model';
import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppUtilBaseService } from '@baseapp/app-util.base.service';
import { TranslateService } from '@ngx-translate/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ChangeLogsComponent } from '@baseapp/widgets/change-logs/change-logs.component'
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ElementRef, Renderer2, ViewChild } from '@angular/core';
import { allowedValuesValidator } from '@baseapp/widgets/validators/allowedValuesValidator';
import { fromEvent, Subscription, map } from 'rxjs';
import { environment } from '@env/environment';
import { Filter } from '@baseapp/vs-models/filter.model';
import { AppConstants } from '@app/app-constants';
import { BaseAppConstants } from '@baseapp/app-constants.base';
import { AppGlobalService } from '@baseapp/app-global.service';

@Directive(
{
	providers:[MessageService, ConfirmationService, DialogService]
}
)
export class BreedListBaseComponent{
	
	
	quickFilter: any;
hiddenFields:any = {};
quickFilterFieldConfig:any={}
	bsModalRef?: BsModalRef;
	isSearchFocused:boolean = false;
showBreadcrumb = AppConstants.showBreadcrumb;
	
showAdvancedSearch: boolean = false;

tableSearchFieldConfig:any = {};
@ViewChild('toggleButton')
  toggleButton!: ElementRef;
  @ViewChild('menu')
  menu!: ElementRef;

	selectedValues!: [];
  filter: Filter = {
    globalSearch: '',
    advancedSearch: {},
    sortField: null,
    sortOrder: null,
    quickFilter: {}
  };
params: any;
isMobile: boolean = AppConstants.isMobile;

  gridData: BreedBase[] = [];
  totalRecords: number = 0;
  subscriptions: Subscription[] = [];
 multiSortMeta:any =[];
 selectedColumns:any =[];
subHeader: any;
  autoSuggest: any;
  query: any;

rightFreezeColums:any;
total:number =0;
inValidFields:any = {};
selectedItems:any ={};
scrollTop:number =0;
isRowSelected: boolean = false;
isPrototype = environment.prototype;
  workFlowEnabled = false;
isList = true;
isPageLoading:boolean = false;
autoSuggestPageNo:number = 0;
complexAutoSuggestPageNo:number = 0
localStorageStateKey = "breed-list";
showMenu: boolean = false;
conditionalActions:any ={
  disableActions:[],
  hideActions:[]
}
actionBarConfig:any =[];
first: number =0;
rows: number = 0;
updatedRecords:BreedBase[] = [];
showPaginationOnTop = AppConstants.showPaginationonTop;
 showPaginationOnBottom = AppConstants.showPaginationonBottom;
 tableFieldConfig:any ={};
dateFormat: string = AppConstants.calDateFormat;
selectedRowId: any = '';
 showWorkflowSimulator:boolean = false;
	isChildPage:boolean = false;

	
	leftActionBarConfig : any = {
  "children" : [ {
    "outline" : "true",
    "buttonType" : "icon_on_left",
    "visibility" : "show",
    "showOn" : "both",
    "buttonStyle" : "curved",
    "action" : "new",
    "buttonEnabled" : "yes",
    "label" : "NEW",
    "type" : "button"
  }, {
    "outline" : "true",
    "buttonType" : "icon_only",
    "visibility" : "show",
    "showOn" : "both",
    "buttonStyle" : "curved",
    "icon" : {
      "type" : "icon",
      "icon" : {
        "label" : "fas fa-trash-alt",
        "value" : "fas fa-trash-alt"
      },
      "iconColor" : "#000000",
      "iconSize" : "13px"
    },
    "action" : "delete",
    "buttonEnabled" : "yes",
    "label" : "DELETE",
    "type" : "button"
  }, {
    "outline" : "true",
    "buttonType" : "icon_only",
    "visibility" : "show",
    "showOn" : "both",
    "buttonStyle" : "curved",
    "icon" : {
      "type" : "icon",
      "icon" : {
        "label" : "fas fa-sync",
        "value" : "fas fa-sync"
      },
      "iconColor" : "#000000",
      "iconSize" : "13px"
    },
    "action" : "refresh",
    "buttonEnabled" : "yes",
    "label" : "REFRESH",
    "type" : "button"
  } ]
}
	rightActionBarConfig : any = { }
	tableSearchConfig : any = {
  "children" : [ {
    "label" : "BREED_CODE",
    "data" : "",
    "field" : "breedCode",
    "type" : "searchField",
    "fieldType" : "string",
    "multipleValues" : false,
    "fieldId" : "breedCode",
    "timeOnly" : false,
    "uiType" : "text",
    "name" : "breedCode",
    "fieldName" : "breedCode"
  }, {
    "label" : "BREED_NAME",
    "data" : "",
    "field" : "breedName",
    "type" : "searchField",
    "fieldType" : "string",
    "multipleValues" : false,
    "fieldId" : "breedName",
    "timeOnly" : false,
    "uiType" : "text",
    "name" : "breedName",
    "fieldName" : "breedName"
  } ],
  "columns" : "2",
  "type" : "tableSearch",
  "showAdvancedSearch" : true
}
	quickFilterConfig : any = { }
	tableConfig : any = {
  "rightFreezeFromColumn" : "0",
  "currentNode" : "table",
  "columnReorder" : false,
  "type" : "grid",
  "showDetailPageAs" : "navigate_to_new_page",
  "rowGroup" : "",
  "storageName" : "breed-list",
  "HEADER_TAG" : "<th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"breedCode\"\n        (click)=\"sort($event,'breedCode')\"\n     *ngIf=\"columns[0]\">\n    <span [innerHTML]=\"columns[0].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'breedCode'\"></p-sortIcon>\n</th><th scope=\"col\" \n[style.min-width]=\"'120px'\"\n     \n     [style.width]=\"'120px'\"\n        style=\"overflow:hidden\"  \n        pSortableColumn=\"breedName\"\n        (click)=\"sort($event,'breedName')\"\n     *ngIf=\"columns[1]\">\n    <span [innerHTML]=\"columns[1].label| translate\" class=\"\"></span>\n        <p-sortIcon [field]=\"'breedName'\"></p-sortIcon>\n</th>",
  "children" : [ {
    "label" : "BREED_CODE",
    "data" : "",
    "field" : "breedCode",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "string",
    "multipleValues" : false,
    "fieldId" : "breedCode",
    "timeOnly" : false,
    "uiType" : "text",
    "name" : "breedCode",
    "fieldName" : "breedCode"
  }, {
    "label" : "BREED_NAME",
    "data" : "",
    "field" : "breedName",
    "type" : "gridColumn",
    "width" : "120px",
    "showOnMobile" : "true",
    "fieldType" : "string",
    "multipleValues" : false,
    "fieldId" : "breedName",
    "timeOnly" : false,
    "uiType" : "text",
    "name" : "breedName",
    "fieldName" : "breedName"
  } ],
  "valueChange" : true,
  "toggleColumns" : false,
  "sorting" : "single_column",
  "sortField" : "",
  "resizeMode" : "",
  "rowSpacing" : "medium",
  "rowHeight" : "medium",
  "recordSelection" : "multiple_records",
  "striped" : true,
  "infiniteScroll" : false,
  "viewAs" : "list",
  "hoverStyle" : "box",
  "tableStyle" : "style_2",
  "BODY_TAG" : "<td  class=\"breedCode  overflow-hidden\" tooltipActive *ngIf=\"columns[0]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n\n        <span [innerHTML]=\"data[columns[0].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n</td><td  class=\"breedName  overflow-hidden\" tooltipActive *ngIf=\"columns[1]&&((isMobile && tableConfig.showOnMobile) || !isMobile)\" \n[style.min-width]=\"'120px'\"\n[style.width]=\"'120px'\"\n>\n\n        <span [innerHTML]=\"data[columns[1].fieldName]\" class=\"ellipsis white-space-nowrap\"></span>\n\n</td>",
  "leftFreezeUptoColumn" : "0",
  "pageLimit" : "50",
  "rememberLastTableSettings" : false,
  "columnResize" : false,
  "sortOrder" : "asc",
  "showGridlines" : true,
  "detailPage" : {
    "name" : "Breed Detail",
    "sid" : "3b5ebe46-48c3-4799-b8d3-fe3fdf03921e",
    "url" : "/masterdatatables/backgroundlists/breeddetail"
  },
  "detailPageNavigation" : "click_of_the_row"
}
	pageViewTitle: string = 'BREED_LIST';
	
		tableSearchControls : FormGroup = new FormGroup({
	breedName: new FormControl('',[]),
	breedCode: new FormControl('',[]),
});

		quickFilterControls : FormGroup = new FormGroup({
});


	constructor(public breedService: BreedService, public appUtilBaseService: AppUtilBaseService, public translateService: TranslateService, public messageService: MessageService, public confirmationService: ConfirmationService, public dialogService: DialogService, public domSanitizer: DomSanitizer, public bsModalService: BsModalService, public activatedRoute: ActivatedRoute, public renderer2: Renderer2, public router: Router, public appGlobalService: AppGlobalService, ...args: any) {
    
 	 }

	
	clearFilterValues() {
  this.tableSearchControls.reset();
  this.filter.advancedSearch = {};
  this.onRefresh();
}
	conditionalFormatting(config: any, data: any) {
   if(config?.hasOwnProperty([data])){
    const query = config[data].query
    const initialCondition = true;
    const finalCondition = query.condition === 'and' ? '&&' : '||';
    const conditions: any[] = [];
    query.rules?.forEach((rule: any) => {
      conditions.push(this.appUtilBaseService.evaluate(data, rule.value, rule.operator));
    })
    let finalResult = conditions?.reduce((previousValue: any, currentValue: any) =>
      this.appUtilBaseService.evaluate(previousValue, currentValue, finalCondition), initialCondition);
    return finalResult;
   }
   else{
     return false;
   }
}
	getSubHeader() {
this.subHeader = this.tableConfig.groupOnColumn?.name?.split('.');
}
	filterSearch() {
    this.quickFilterControls.valueChanges.subscribe((value) => {
      let dateRangeNotChoosen: boolean = false;
      for (let control of this.quickFilterConfig.children) {
        if (control.fieldType === 'Date') {
          if (value[control.field][0] && !value[control.field][1]) {
            dateRangeNotChoosen = true;
            break;
          }
        }
      }
      if (!dateRangeNotChoosen) {
        this.filter.quickFilter = value;
       this.onRefresh();
      }
    });
  }
	advancedSearch() {
    this.filter.advancedSearch = this.tableSearchControls.value;
    let hasDates = this.tableSearchConfig.children.filter((e: any) => e.fieldType.toLowerCase() == "date" || e.fieldType.toLowerCase() == "datetime");
    if (hasDates.length > 0) {
      hasDates.forEach((f: any) => {
        let field = f.name;
        let value = this.filter.advancedSearch[field];
        if (value && Array.isArray(value)) {
          let val = { lLimit: value[0].getTime(), uLimit: value[1] ? value[1].getTime() : value[1], type: "Date" }
          this.filter.advancedSearch[field] = val;
          if (value[0] == null && value[1] == null) {
            delete this.filter.advancedSearch[field];
          }
        }
      });
    }
    let hasNumbers = this.tableSearchConfig.children.filter((e: any) => e.fieldType.toLowerCase() == "number" || e.fieldType.toLowerCase() == "double");
    if (hasNumbers.length > 0) {
      hasNumbers.forEach((f: any) => {
        let field = f.name;
        let value = this.filter.advancedSearch[field];
        if (value && !Array.isArray(value)) {
          this.filter.advancedSearch[field] = {
            lLimit: value.min, uLimit: value.max, type: "Number"
          }
          if (value.min == null && value.max == null) {
            delete this.filter.advancedSearch[field];
          }
        }
      });
    }
    this.onRefresh();
    this.toggleAdvancedSearch();
  }
	// closeAdvancedSearchPopup() {
  //   this.renderer2.listen('window', 'click', (e: Event) => {
  //     let clickedInside = this.menu?.nativeElement.contains(e.target);
  //     if(e.target !== this.toggleButton?.nativeElement&& !clickedInside &&this.showAdvancedSearch){
  //       this.showAdvancedSearch = false;
  //     }
  //   );
  // }
clearFilters(){
  this.filter.globalSearch = '';
  this.isSearchFocused = false;
}

focus(){
  this.isSearchFocused = !this.isSearchFocused;
}
	next() {
     
        this.first = this.first + this.rows;
        this.params.start = this.first;
        this.loadGridData();
      
      
    }
	showToastMessage(config: object) {
this.messageService.add(config);
}
	openSettings() {
    let matColumnOrder: any = this.getValueFromLocalStorage(this.localStorageStateKey) || {};
    let alreadySelectedCols = matColumnOrder['columnOrder'] || [];
    if (alreadySelectedCols.length > 0) {
      this.tableConfig.children.map((e: any) => e.checked = false);
      this.tableConfig.children.forEach((e: any) => {
        if (alreadySelectedCols.includes(e.field)) {
          e.checked = true;
        }
      });
    } else {
      this.tableConfig.children.map((e: any) => e.checked = true);
    }
    this.showMenu = true;
  }
	setHeight() {
    setTimeout(() => {
      const el = (<HTMLInputElement>document.getElementById("table-container")).getBoundingClientRect();
      let paginatorHeight: number = 0;
      if (this.showPaginationOnBottom) {
        const paginator = (<HTMLInputElement>document.getElementById("paginator-bottom")).getBoundingClientRect();
        paginatorHeight = paginator.height;
      }
      const top = (el.top + paginatorHeight) + 'px';
      (<HTMLInputElement>document.getElementById('table-container')).style.setProperty('height', 'calc(100vh - ' + top + ')');
    }, 100);

}
	isFirstPage(): boolean {
      return this.gridData ? this.first === 0 : true;
    }
	saveResizeColumns(event: any) {
    setTimeout(() => {
      this.selectedColumns.forEach((e: any) => {
        if (e.data == event.element.innerText) {
          e.width = event.element.offsetWidth + 'px';
          localStorage.setItem(this.localStorageStateKey, JSON.stringify(this.selectedColumns));
        }
      });
    }, 10);
  }
	onDelete() {
  if (this.selectedValues.length > 0) {
    let values: any = [];
	this.selectedValues.forEach((field: any) => {
		values.push(field.sid)
	});
	let requestedParams:any = {ids:values.toString()}
      this.confirmationService.confirm({
        message: this.translateService.instant('DELETE_CONFIRMATION_MESSAGE'),
        header: 'Confirmation',
        icon: 'pi pi-info-circle',
        accept: () => {
          const deleteSubscription = this.breedService.delete(requestedParams).subscribe((res: any) => {
            this.showToastMessage({severity:'success', summary:'', detail:this.translateService.instant('RECORDS_DELETED_SUCCESSFULLY')});
            requestedParams = {};
            this.selectedValues = [];
            values?.map((m:any)=>{
              const index = this.updatedRecords?.findIndex(o=>o.sid === m);
              if(index > -1){
              this.updatedRecords?.splice(index,1);
              }
            })
            this.loadGridData();
            
          });
           this.subscriptions.push(deleteSubscription);
        },
        reject: () => {
          //rejected
        },
      });
    }

  }
	onRowSelect(event?: any) {
    if (event?.originalEvent) {
      event?.originalEvent.stopPropagation();
    }
    if (this.selectedValues.length > 0) {
      this.isRowSelected = true;
    }
    else if (this.selectedValues.length <= 0) {
      this.isRowSelected = false;
    }
  }
	updateActions(){
      this.actionBarConfig = this.appUtilBaseService.getActionsConfig(this.leftActionBarConfig.children);
      this.actionBarConfig.forEach((actionConfig:any)=>{
        if(actionConfig.visibility === 'conditional' && actionConfig.conditionForButtonVisiblity){
          const conResult = this.appUtilBaseService.evaluvateCondition(actionConfig.conditionForButtonVisiblity?.query?.rules, actionConfig.conditionForButtonVisiblity?.query?.condition);
          this.validateActions(actionConfig.action,conResult,'view');
        }
        if(actionConfig.buttonEnabled === 'conditional' && actionConfig.conditionForButtonEnable){
          const conResult = this.appUtilBaseService.evaluvateCondition(actionConfig.conditionForButtonEnable?.query?.rules, actionConfig.conditionForButtonEnable?.query?.condition);
          this.validateActions(actionConfig.action,conResult,'edit');
        }
      })
    }
	validateActions(label: string, result: boolean, action: string) {
      if (action === 'view') {
        if (result && this.conditionalActions.hideActions.includes(label))
          this.conditionalActions.hideActions?.splice(this.conditionalActions.hideActions?.indexOf(label), 1)
        else if (!result && !this.conditionalActions.hideActions.includes(label))
          this.conditionalActions.hideActions.push(label);
      }
      else if (action === 'edit') {
        if (result && this.conditionalActions.disableActions.includes(label))
          this.conditionalActions.disableActions.splice(this.conditionalActions.disableActions?.indexOf(label), 1);
        else if (!result && !this.conditionalActions.disableActions.includes(label))
          this.conditionalActions.disableActions.push(label);
      }
    }
	initFilterForm(){
    this.quickFilterFieldConfig= this.appUtilBaseService.getControlsFromFormConfig(this.quickFilterConfig);
    this.filterSearch();
}
	onFormatMultipleValues(col: any, data: any): any {
    const arr: any = []
        if(col.uiType === 'link') {
      if(data) {
        for(var i=0;i<data.length;i++) {
          const url = "<a href="+data[i]+">Link"+(i+1)+"</a>"
          arr.push(url);
        }
        return arr.join(', ')
      }
    }
    const displayField = col.displayField ? col.displayField : '';
  if (col.uiType == 'autosuggest' && Array.isArray(data)) {
      data?.forEach((k: any) => {
        arr.push(k.value[displayField]);
      })
    }
    else if (Array.isArray(data)) {
      data.forEach(function (e: any) {
        if (displayField)
          arr.push(e[displayField])
        else
          arr.push(e);
      })
    }
    else if (typeof data === 'object') {
      if (displayField) {
        arr.push(data[displayField]);
      }
    }
    else {
      arr.push(data);
    }
    return (arr.toString());
  }
	onUpdate(id: any,event?:any) {
	if (this.tableConfig.detailPage?.url) {
      const value: any = "parentId";
       let property: Exclude<keyof BreedListBaseComponent, ''> = value;
       const methodName: any = "onUpdateChild";
       let action: Exclude<keyof BreedListBaseComponent, ''> = methodName;
       if (this.isChildPage && this[property]) {
	       if (typeof this[action] === "function") {
	        	this[action](id);
	         }
       }
       else {
       	this.router.navigateByUrl(this.tableConfig.detailPage.url + '?id=' + id)
       }
    }
}
	clearGlobalSearch(){
  this.filter.globalSearch = '';
  this.onRefresh();
}
	onRefresh(){
this.first = 0;
this.gridData = [];
this.updatedRecords =[];
this.params.start =0;
this.loadGridData();
this.updateActions();
this.selectedValues=[];
this.onRowSelect();
}
	getDisabled(formControl: FormGroup, ele: string) {
  const parent = ele.split('?.')[0];
  if (formControl.controls[parent] instanceof FormGroup){
    return formControl.get(ele)?.disabled
  }
  else
    return formControl.controls[parent].disabled;
}
	setAutoSuggestValue(data:any,col:any){
    const arr: any = [];
    const displayField = col.displayField ? col.displayField : '';
    if(data && Array.isArray(data)){
      data?.forEach((k: any) => {
        arr.push(k.value[displayField]);
      })
    }
    else if(data?.value){
      arr.push(data.value[displayField]);
    }
    else{
      arr.push(data);
    }
    return arr.join();
  }
	clearAllFilters() {
  this.filter.globalSearch = '';
  this.clearFilterValues();
}
	loadGridData() {
 this.isPageLoading = true;
  let gridSubscription: any;
  if (environment.prototype) {
  	gridSubscription = this.breedService.getProtoTypingData().subscribe((data: any) => {
  		this.tableConfig.children.map((o: any) => {
        data.map((d: any) => {
          if (['date', 'datetime', 'curreny'].includes(o.uiType)) {
            d[o.name] = this.onFormatColumns(o, d); 
          }
        })
      })
      this.gridData = [...this.gridData, ...data];
  	  this.isPageLoading = false;
  	});
  } else {
	  const params = this.assignTableParams();
	  	const value:any = "parentId";
	  	let property: Exclude<keyof BreedListBaseComponent, ''> = value;
	  	const method:any = "getChildTableData";
	  	let action: Exclude<keyof BreedListBaseComponent, ''> = method;
	  	if(this.isChildPage && typeof this[action] === "function"){
        if (this[property]) {
          params.pid = this[property];
          
          this[action](params);
        }
        else {
          this.isPageLoading = false;
        }
	  	}
	  	else{
	  	
		  	this.getTableData(params);
	}
	}
}

  getTableData(params: any) {
    	const gridSubscription =  this.breedService.getDatatableData(params).subscribe((data: any) => {
      if (this.first >= this.total || this.first === 0) {
        this.tableConfig.children.map((o: any) => {
          data?.results.map((d: any) => {
            if (['date', 'datetime', 'curreny', 'autosuggest'].includes(o.uiType)) {
              d[o.name] = this.onFormatColumns(o, d); 
            }
          })
        })
        let updateRecords: BreedBase[] = [...this.updatedRecords, ...data?.results];
        const ids = updateRecords.map(o => o.sid);
        this.updatedRecords = updateRecords.filter(({ sid }, index) => !ids.includes(sid, index + 1));
      }
      this.gridData = this.updatedRecords.slice(this.first, (this.first +this.rows));
      this.denormalize(this.gridData);
      this.total = data?.filtered ? data?.filtered : 0;
      this.isPageLoading = false;
    }, (err: any) => {
      this.isPageLoading = false;
    });
    this.subscriptions.push(gridSubscription);
  }
	onNew() {
	const value: any = "parentId";
	let property: Exclude<keyof BreedListBaseComponent, ''> = value;
	if (this.isChildPage && this[property]) {
		const methodName: any = "onNewChild";
		let action: Exclude<keyof BreedListBaseComponent, ''> = methodName;
		if (typeof this[action] == "function") {
			this[action]();
		}
	}
	else {
		this.router.navigate(['../breeddetail'], { relativeTo: this.activatedRoute});
	}
}
	saveColumns() {
    let columns = document.querySelectorAll('.tbl-ctx-menu input:checked')
    let columnsToShow: string[] = [];
    columns.forEach((e: any) => {
      columnsToShow.push(e.value);
    });
    let matColumnOrder: any = this.getValueFromLocalStorage(this.localStorageStateKey) || {};
    matColumnOrder['columnOrder'] = columnsToShow;
    localStorage.setItem(this.localStorageStateKey, JSON.stringify(matColumnOrder));
    let cols = this.tableConfig.children;
    cols = cols.filter((e: any) => columnsToShow.includes(e.field));
    if (matColumnOrder.columnOrder) {
      let sortingArr = matColumnOrder.columnOrder || [];
      cols.sort(function (a: any, b: any) {
        return sortingArr.indexOf(a) - sortingArr.indexOf(b);
      });
    }
    this.selectedColumns = cols;
    this.showMenu = false;
  }
	attachInfiniteScroll() {
const tracker = (<HTMLInputElement>document.getElementsByClassName('p-datatable-wrapper')[0])
let windowYOffsetObservable = fromEvent(tracker, 'scroll').pipe(map(() => {
  return Math.round(tracker.scrollTop);
}));

const scrollSubscription = windowYOffsetObservable.subscribe((scrollPos: number) => {
  if(this.scrollTop != scrollPos){
        this.scrollTop = scrollPos;
    if ((tracker.offsetHeight + scrollPos >= tracker.scrollHeight)) {
      this.params.start = this.total;
     this.loadGridData();
   }
  }
});
this.subscriptions.push(scrollSubscription);
}
	clearColumnReorder() {
    setTimeout(() => {
      this.selectedColumns = this.tableConfig.children;
      localStorage.removeItem(this.localStorageStateKey);
      this.showMenu = false;
    }, 10);
  }
	getValue(formControl: FormGroup, ele: string) {
    const parent = ele.split('?.')[0];
    if (formControl.controls[parent] instanceof FormGroup){
      const child = ele.split('?.')[1];
      return formControl.controls[parent].value[child];
    }
    else
      return formControl.controls[parent].value;
  }
	prev() {
      if(!this.isFirstPage()){
        this.first = this.first - this.rows;
        this.params.start = this.first;
        if(this.first === 0){
          this.gridData = [];
          this.updatedRecords =[];
        }
        this.loadGridData();
      }
    }
	initSearchForm(){
  this.tableSearchFieldConfig= this.appUtilBaseService.getControlsFromFormConfig(this.tableSearchConfig)
}
	checkLocalStorage() {
    if (localStorage.getItem(this.localStorageStateKey)) {
      try {
        let columnsToShow = JSON.parse(localStorage.getItem(this.localStorageStateKey) || '{}');
        let cols = this.tableConfig.children;
        if (columnsToShow.columnOrder) {
          cols = cols.filter((e: any) => columnsToShow.columnOrder.includes(e.field));
          let sortingArr = columnsToShow.columnOrder;
          cols.sort(function (a: any, b: any) {
            return sortingArr.indexOf(a) - sortingArr.indexOf(b);
          });
        }
        this.selectedColumns = cols;
      }
      catch (e) {
        this.selectedColumns = this.tableConfig.children;
      }
    } else {
      this.selectedColumns = this.tableConfig.children;
    }
  }
	getSearchData(searchFields: any, config: any) {
    let searchData: any = {}
    for (const key in searchFields) {
      if (searchFields.hasOwnProperty(key) && searchFields[key]?.toString().length) {
       if (this.selectedItems.hasOwnProperty(key)) {
          let lookupObj: any = [];
          if (config[key].multiple) {
            this.selectedItems[key].map((o: any) => lookupObj.push(o.id));
          }
          searchData[`${key}__id`] = config[key].multiple ? lookupObj : this.selectedItems[key][0].id;
        }
        else {
          searchData[key] = searchFields[key];
        }
      }
    }
    return searchData;
  }
	setLocalStorageKey(){
  const currentUserData = this.appGlobalService.get('currentUser') && this.appGlobalService.get('currentUser')[0];
  const userId = (environment.prototype)? '':currentUserData?.sid;
  this.localStorageStateKey = `${this.localStorageStateKey}${userId}`
}
	saveReorderedColumns(event: any) {
    let columnOrder = event.columns.map((e: any) => e.field);
    let matColumnOrder: any = this.getValueFromLocalStorage(this.localStorageStateKey) || {};
    matColumnOrder['columnOrder'] = columnOrder;
    localStorage.setItem(this.localStorageStateKey, JSON.stringify(matColumnOrder));
    this.selectedColumns = JSON.parse(JSON.stringify(event.columns));
  }
	onKeydown(event: any) {
  if (event.which === 13 || event.keyCode === 13) {
    // this.filter.globalSearch = this.globalSearch
   this.onRefresh();
  }
}
	sort(e: any, field: string) {
this.filter.sortField = field;
this.filter.sortOrder = (e.currentTarget.childNodes[1].childNodes[0].classList.contains('pi-sort-amount-up-alt')) ? 'desc' : 'asc';
this.onRefresh();
}
	onFormatColumns(col: any, datum: any) {
    const type = col.uiType;
    let data = datum[col.name];
    let formattedValue: any;
    switch (type) {
      case 'date':
        formattedValue = this.appUtilBaseService.formatDate(data, col.format ? col.format : null);
        break;

      case 'datetime':
        formattedValue = this.appUtilBaseService.formatDateTime(data, col.format ? col.format : null)
        break;

      case 'currency':
       const ccode = col.currencySymbol ? col.currencySymbol : null;
        const cDigits = col.currencyDigits ? col.currencyDigits : null;
        formattedValue = this.appUtilBaseService.formatCurrency(data, ccode, cDigits);
        break;  
      case 'autosuggest':
        formattedValue = this.setAutoSuggestValue(data,col);
        break;
      default:
        formattedValue = data;
    }
    return (formattedValue);
  }
	toggleAdvancedSearch() {
  this.showAdvancedSearch = !this.showAdvancedSearch;
}
	getClass(){
    const styleClass = (this.isMobile && this.tableConfig.viewAs ==='list') ? 'table-body-md':'table-body';
    return styleClass;
  }
	denormalize(gridData: any) {
}
	cancelColumnOptions() {
    this.showMenu = false;
  }
	actionBarAction(btn: any) {
    const methodName: any = (`on` + btn.action.charAt(0).toUpperCase() + btn.action.slice(1));
    let action: Exclude<keyof BreedListBaseComponent, ' '> = methodName;
    if (btn.action === 'navigate_to_page' && btn.pageName?.url) {
      this.router.navigateByUrl(btn.pageName.url);
    }
    else if (typeof this[action] === "function") {
      this[action]();
    }
  }
	calculateFormula(){
	
}
	disablechildAction() {
    const value: any = "parentId";
    let property: Exclude<keyof BreedListBaseComponent, ' '> = value;
    this.leftActionBarConfig?.children?.map((ele:any)=>{
      if(ele.type === 'buttonGroup'){
        ele?.children.map((childEle:any,index:number)=>{
          if (childEle?.action === 'new' && !this[property] && this.isChildPage && childEle.buttonEnabled !='conditional') {
              childEle.buttonEnabled ='no';
            }
          else if(childEle.action === 'new' && this[property] && this.isChildPage && childEle.buttonEnabled !='conditional'){
            childEle.buttonEnabled ='yes';
          }
        })
      }
    })
  }
	assignTableParams(){
  const params = this.params;
  this.filter.sortField = this.tableConfig.groupOnColumn ? this.tableConfig.groupOnColumn?.name : this.filter.sortField;
  const searchData = { ...this.getSearchData(this.filter.advancedSearch, this.tableSearchFieldConfig), ...this.getSearchData(this.filter.quickFilter, this.quickFilterFieldConfig) }
  if (this.filter.globalSearch)
    searchData['_global'] = this.filter.globalSearch;

  if (this.filter.sortField && this.filter.sortOrder) {
    let isFieldTypeAutoSuggest: boolean = false;
    this.tableConfig.children.map((ele: any) => {
      if (ele.uiType === "autosuggest" && this.filter.sortField === ele.name) {
        isFieldTypeAutoSuggest = true;
      }
      const columnName = isFieldTypeAutoSuggest ? (ele.name + "__value__" + ele.displayField) : this.filter.sortField;
      params.order = [{
        column: columnName,
        dir: this.filter.sortOrder
      }]
    })
  }
  else {
    params.order = null;
  }
  params.search = searchData;

  return params;
}
	getValueFromLocalStorage(key: string) {
    let val = localStorage.getItem(key);
    if (val != null) {
      return JSON.parse(val);
    } else {
      return null;
    }
  }

    onInit() {
		
		this.initSearchForm();

		this.initFilterForm();
		this.setLocalStorageKey()
this.tableConfig.children = this.appUtilBaseService.formatTableConfig(this.tableConfig.children);
 this.tableFieldConfig = this.appUtilBaseService.formatTableFieldConfig(this.tableConfig.children)
this.params = this.appUtilBaseService.getTableRequestParams(this.tableConfig);
this.rows = this.params.length;
this.loadGridData();
this. checkLocalStorage();
this.getSubHeader();
this.rightFreezeColums = (this.tableConfig.children.length - this.tableConfig.rightFreezeFromColumn);
 this.disablechildAction();
this.updateActions();
    }
	
     onDestroy() {
		
		    this.subscriptions.forEach((subs: { unsubscribe: () => void; }) => subs.unsubscribe());
  
    }
     onAfterViewInit() {
		
		
 this.setHeight();
//  setTimeout(() => {
  //     this.attachInfiniteScroll();
  //   }, 2000);
 
  

    }

}

import { BreedBase} from '@baseapp/master-data-tables/background-lists/breed/breed.base.model';

export class BreedApiConstants {
    public static readonly autoSuggestService: any = {
        url: '/rest/breeds/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly getByBreedCode: any = {
        url: '/rest/breeds/bybreedcode/{breedcode}',
        method: 'GET',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/breeds/',
        method: 'PUT',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/breeds/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly create: any = {
        url: '/rest/breeds/',
        method: 'POST',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/breeds/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly delete: any = {
        url: '/rest/breeds/{ids}',
        method: 'DELETE',
        showloading: false
    };
}
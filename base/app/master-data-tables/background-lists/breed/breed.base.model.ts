export interface BreedBase {
	modifiedBy: string;
	sid: string;
	createdBy: string;
	createdDate: Date;
	breedCode: string;
	modifiedDate: Date;
	breedName: string;
}
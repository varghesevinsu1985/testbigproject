export interface CertificatesListBase {
	createdBy: string;
	certificateName: string;
	modifiedBy: string;
	stable: string;
	certificateId: string;
	createdDate: Date;
	validFrom: Date;
	modifiedDate: Date;
	validForCountry: any;
	validTo: Date;
	sid: string;
}
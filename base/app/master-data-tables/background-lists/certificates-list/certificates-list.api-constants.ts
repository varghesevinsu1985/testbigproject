import { CertificatesListBase} from '@baseapp/master-data-tables/background-lists/certificates-list/certificates-list.base.model';

export class CertificatesListApiConstants {
    public static readonly create: any = {
        url: '/rest/certificateslists/',
        method: 'POST',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/certificateslists/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly getCertificatesListByPId: any = {
        url: '/rest/certificateslists/bypid/{pid}',
        method: 'GET',
        showloading: false
    };
    public static readonly getDatatableDataByPId: any = {
        url: '/rest/certificateslists/datatable/{pid}',
        method: 'POST',
        showloading: false
    };
    public static readonly getByCertificateId: any = {
        url: 'bycertificateid/{certificateid}',
        method: 'GET',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/certificateslists/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/certificateslists/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/certificateslists/',
        method: 'PUT',
        showloading: false
    };
    public static readonly delete: any = {
        url: '/rest/certificateslists/{ids}',
        method: 'DELETE',
        showloading: false
    };
}
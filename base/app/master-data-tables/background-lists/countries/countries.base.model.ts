export interface CountriesBase {
	modifiedDate: Date;
	sid: string;
	createdDate: Date;
	createdBy: string;
	countryName: string;
	modifiedBy: string;
	countryCode: string;
}
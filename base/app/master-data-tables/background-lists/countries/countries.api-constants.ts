import { CountriesBase} from '@baseapp/master-data-tables/background-lists/countries/countries.base.model';

export class CountriesApiConstants {
    public static readonly getByCountryCode: any = {
        url: 'bycountrycode/{countrycode}',
        method: 'GET',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/countries/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/countries/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/countries/',
        method: 'PUT',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/countries/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly delete: any = {
        url: '/rest/countries/{ids}',
        method: 'DELETE',
        showloading: false
    };
    public static readonly create: any = {
        url: '/rest/countries/',
        method: 'POST',
        showloading: false
    };
}
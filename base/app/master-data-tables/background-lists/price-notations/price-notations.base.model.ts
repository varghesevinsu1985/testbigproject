export interface PriceNotationsBase {
	concept: string;
	priceNotationKey: string;
	sid: string;
	createdBy: string;
	createdDate: Date;
	priceNotationName: string;
	modifiedDate: Date;
	country: string;
	modifiedBy: string;
}
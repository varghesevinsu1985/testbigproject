import { PriceNotationsBase} from '@baseapp/master-data-tables/background-lists/price-notations/price-notations.base.model';

export class PriceNotationsApiConstants {
    public static readonly delete: any = {
        url: '/rest/pricenotations/{ids}',
        method: 'DELETE',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/pricenotations/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly create: any = {
        url: '/rest/pricenotations/',
        method: 'POST',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/pricenotations/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/pricenotations/autosuggest',
        method: 'GET',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/pricenotations/',
        method: 'PUT',
        showloading: false
    };
    public static readonly getByPriceNotationKey: any = {
        url: '/rest/pricenotations/bypricenotationkey/{pricenotationkey}',
        method: 'GET',
        showloading: false
    };
}
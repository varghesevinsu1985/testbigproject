import { BreedListComponent } from '@app/master-data-tables/background-lists/breed/breed-list/breed-list.component';
import { AdministrationListComponent } from '@app/master-data-tables/background-lists/administration/administration-list/administration-list.component';
import { CertificatesListPipe } from '@app/master-data-tables/background-lists/certificates-list/certificates-list.pipe.component';
import { WidgetsBaseModule } from '@baseapp/widgets/widgets.base.module';
import { BreedPipe } from '@app/master-data-tables/background-lists/breed/breed.pipe.component';
import { BreedDetailComponent } from '@app/master-data-tables/background-lists/breed/breed-detail/breed-detail.component';
import { NgModule } from '@angular/core';
import { AdministrationPipe } from '@app/master-data-tables/background-lists/administration/administration.pipe.component';
import { PriceNotationsPipe } from '@app/master-data-tables/background-lists/price-notations/price-notations.pipe.component';
import { CountriesPipe } from '@app/master-data-tables/background-lists/countries/countries.pipe.component';
import { StatusPipe } from '@app/master-data-tables/background-lists/status/status.pipe.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CanDeactivateGuard } from '@baseapp/auth.can-deactivate-guard.service';
import { RelationTypePipe } from '@app/master-data-tables/background-lists/relation-type/relation-type.pipe.component';
import { CountriesDetailComponent } from '@app/master-data-tables/background-lists/countries/countries-detail/countries-detail.component';
import { RelationTypeDetailComponent } from '@app/master-data-tables/background-lists/relation-type/relation-type-detail/relation-type-detail.component';
import { PriceNotationsDetailComponent } from '@app/master-data-tables/background-lists/price-notations/price-notations-detail/price-notations-detail.component';
import { PriceNotationsListComponent } from '@app/master-data-tables/background-lists/price-notations/price-notations-list/price-notations-list.component';
import { AdministrationDetailComponent } from '@app/master-data-tables/background-lists/administration/administration-detail/administration-detail.component';
import { StatusListComponent } from '@app/master-data-tables/background-lists/status/status-list/status-list.component';
import { CertificatesListDetailComponent } from '@app/master-data-tables/background-lists/certificates-list/certificates-list-detail/certificates-list-detail.component';
import { RelationTypeListComponent } from '@app/master-data-tables/background-lists/relation-type/relation-type-list/relation-type-list.component';
import { StatusDetailComponent } from '@app/master-data-tables/background-lists/status/status-detail/status-detail.component';
import { SharedModule } from '@app/shared/shared.module';
import { CountriesListComponent } from '@app/master-data-tables/background-lists/countries/countries-list/countries-list.component';
import { CertificatesListListComponent } from '@app/master-data-tables/background-lists/certificates-list/certificates-list-list/certificates-list-list.component';

@NgModule({
  declarations: [
CertificatesListDetailComponent,
CertificatesListPipe,
CertificatesListListComponent,
AdministrationListComponent,
AdministrationPipe,
AdministrationDetailComponent,
RelationTypeDetailComponent,
RelationTypePipe,
RelationTypeListComponent,
PriceNotationsDetailComponent,
PriceNotationsPipe,
PriceNotationsListComponent,
CountriesListComponent,
CountriesPipe,
CountriesDetailComponent,
BreedDetailComponent,
BreedPipe,
BreedListComponent,
StatusDetailComponent,
StatusPipe,
StatusListComponent
],
imports: [
SharedModule,
WidgetsBaseModule
],

exports: [
SharedModule,
WidgetsBaseModule,
CertificatesListDetailComponent,
CertificatesListListComponent,
AdministrationListComponent,
AdministrationDetailComponent,
RelationTypeDetailComponent,
RelationTypeListComponent,
PriceNotationsDetailComponent,
PriceNotationsListComponent,
CountriesListComponent,
CountriesDetailComponent,
BreedDetailComponent,
BreedListComponent,
StatusDetailComponent,
StatusListComponent
],

providers: [
BsModalService,
CanDeactivateGuard
],
  
})
export class BackgroundListsBaseModule { }
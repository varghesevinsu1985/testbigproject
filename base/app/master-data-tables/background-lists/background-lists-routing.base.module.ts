import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from '@baseapp/auth.can-deactivate-guard.service';

import { CertificatesListDetailComponent } from '@app/master-data-tables/background-lists/certificates-list/certificates-list-detail/certificates-list-detail.component';
import { CertificatesListListComponent } from '@app/master-data-tables/background-lists/certificates-list/certificates-list-list/certificates-list-list.component';
import { AdministrationListComponent } from '@app/master-data-tables/background-lists/administration/administration-list/administration-list.component';
import { AdministrationDetailComponent } from '@app/master-data-tables/background-lists/administration/administration-detail/administration-detail.component';
import { RelationTypeDetailComponent } from '@app/master-data-tables/background-lists/relation-type/relation-type-detail/relation-type-detail.component';
import { RelationTypeListComponent } from '@app/master-data-tables/background-lists/relation-type/relation-type-list/relation-type-list.component';
import { PriceNotationsDetailComponent } from '@app/master-data-tables/background-lists/price-notations/price-notations-detail/price-notations-detail.component';
import { PriceNotationsListComponent } from '@app/master-data-tables/background-lists/price-notations/price-notations-list/price-notations-list.component';
import { CountriesListComponent } from '@app/master-data-tables/background-lists/countries/countries-list/countries-list.component';
import { CountriesDetailComponent } from '@app/master-data-tables/background-lists/countries/countries-detail/countries-detail.component';
import { BreedDetailComponent } from '@app/master-data-tables/background-lists/breed/breed-detail/breed-detail.component';
import { BreedListComponent } from '@app/master-data-tables/background-lists/breed/breed-list/breed-list.component';
import { StatusDetailComponent } from '@app/master-data-tables/background-lists/status/status-detail/status-detail.component';
import { StatusListComponent } from '@app/master-data-tables/background-lists/status/status-list/status-list.component';

export const routes: Routes = [

{
     path: 'certificateslistdetail',
     component: CertificatesListDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "CERTIFICATES_LIST_DETAIL",
        breadcrumb: "CERTIFICATES_LIST_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'certificateslistlist',
     component: CertificatesListListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "CERTIFICATES_LIST_LIST",
        breadcrumb: "CERTIFICATES_LIST_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'administrationlist',
     component: AdministrationListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "ADMINISTRATION_LIST",
        breadcrumb: "ADMINISTRATION_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'administrationdetail',
     component: AdministrationDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "ADMINISTRATION_DETAIL",
        breadcrumb: "ADMINISTRATION_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'relationtypedetail',
     component: RelationTypeDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "RELATION_TYPE_DETAIL",
        breadcrumb: "RELATION_TYPE_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'relationtypelist',
     component: RelationTypeListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "RELATION_TYPE_LIST",
        breadcrumb: "RELATION_TYPE_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'pricenotationsdetail',
     component: PriceNotationsDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "PRICE_NOTATIONS_DETAIL",
        breadcrumb: "PRICE_NOTATIONS_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'pricenotationslist',
     component: PriceNotationsListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "PRICE_NOTATIONS_LIST",
        breadcrumb: "PRICE_NOTATIONS_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'countrieslist',
     component: CountriesListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "COUNTRIES_LIST",
        breadcrumb: "COUNTRIES_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'countriesdetail',
     component: CountriesDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "COUNTRIES_DETAIL",
        breadcrumb: "COUNTRIES_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'breeddetail',
     component: BreedDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "BREED_DETAIL",
        breadcrumb: "BREED_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'breedlist',
     component: BreedListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "BREED_LIST",
        breadcrumb: "BREED_LIST",
        roles : [
        			"all"
				]
     }
},
{
     path: 'statusdetail',
     component: StatusDetailComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "STATUS_DETAIL",
        breadcrumb: "STATUS_DETAIL",
        roles : [
        			"all"
				]
     }
},
{
     path: 'statuslist',
     component: StatusListComponent,
     canDeactivate: [ CanDeactivateGuard ],
     data: {
     	label: "STATUS_LIST",
        breadcrumb: "STATUS_LIST",
        roles : [
        			"all"
				]
     }
}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class BackgroundListsBaseRoutingModule
{
}

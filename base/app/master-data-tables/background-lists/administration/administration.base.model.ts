export interface AdministrationBase {
	createdBy: string;
	sid: string;
	createdDate: Date;
	modifiedBy: string;
	administrationKey: string;
	administrationName: string;
	modifiedDate: Date;
}
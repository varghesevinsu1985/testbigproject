import { AdministrationBase} from '@baseapp/master-data-tables/background-lists/administration/administration.base.model';

export class AdministrationApiConstants {
    public static readonly update: any = {
        url: '/rest/administrations/',
        method: 'PUT',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/administrations/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly getByAdministrationKey: any = {
        url: '/rest/administrations/byadministrationkey/{administrationkey}',
        method: 'GET',
        showloading: false
    };
    public static readonly create: any = {
        url: '/rest/administrations/',
        method: 'POST',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/administrations/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly delete: any = {
        url: '/rest/administrations/{ids}',
        method: 'DELETE',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/administrations/autosuggest',
        method: 'GET',
        showloading: false
    };
}
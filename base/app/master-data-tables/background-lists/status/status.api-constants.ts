import { StatusBase} from '@baseapp/master-data-tables/background-lists/status/status.base.model';

export class StatusApiConstants {
    public static readonly delete: any = {
        url: '/rest/statuses/{ids}',
        method: 'DELETE',
        showloading: false
    };
    public static readonly create: any = {
        url: '/rest/statuses/',
        method: 'POST',
        showloading: false
    };
    public static readonly getById: any = {
        url: '/rest/statuses/{sid}',
        method: 'GET',
        showloading: false
    };
    public static readonly update: any = {
        url: '/rest/statuses/',
        method: 'PUT',
        showloading: false
    };
    public static readonly getDatatableData: any = {
        url: '/rest/statuses/datatable',
        method: 'POST',
        showloading: false
    };
    public static readonly autoSuggestService: any = {
        url: '/rest/statuses/autosuggest',
        method: 'GET',
        showloading: false
    };
}
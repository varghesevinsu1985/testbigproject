export interface StatusBase {
	modifiedBy: string;
	createdBy: string;
	createdDate: Date;
	statusHierarchy: number;
	modifiedDate: Date;
	sid: string;
	status: string;
}